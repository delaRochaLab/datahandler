import traceback
import logging
import configparser
import pkg_resources
from datahandler import arg, Timing
from datahandler.aplication_gui import ApplicationGui
from datahandler.cluster import Cluster
from tasks.selected_task import Selection
import numpy as np
import pandas as pd
import os
import re
import warnings
from tasks.cellex4A_opto_to_matlab import cellex4A_opto_to_matlab

import platform
if platform.system() == "Darwin":
    import matplotlib
    matplotlib.use('Agg')
    import matplotlib.pyplot as plt


# LOGGING AND WARNINGS
logger = logging.getLogger(__name__)
warnings.simplefilter(action='ignore', category=UserWarning)
warnings.simplefilter(action='ignore', category=RuntimeWarning)
warnings.simplefilter(action='ignore', category=FutureWarning)


timing = Timing()


# DATAFILE
# individual raw or clean files
class DataFile:
    def __init__(self, session_name, subject_name, task, date, default_task, first=True, last=True,
                 raw_path=None, params_path=None, trials_path=None, daily_report_path=None,
                 clean_subject_directory=None, report_subject_directory=None):

        self.session_name = session_name
        self.subject_name = subject_name
        self.task = task
        self.date = date
        self.day = date[0:4] + '-' + date[4:6] + '-' + date[6:8]
        self.time = date[9:11] + ':' + date[11:13] + ':' + date[13:15]
        self.raw_path = raw_path
        self.params_path = params_path
        self.trials_path = trials_path
        self.daily_report_path = daily_report_path
        self.clean_subject_directory = clean_subject_directory
        self.report_subject_directory = report_subject_directory
        self.clean = False
        self.use = False
        self.df = None
        self.path = None
        self.first = first
        self.last = last
        self.done = False
        self.selection = Selection(task, default_task)

    def __eq__(self, other):
        if not isinstance(other, DataFile):
            return NotImplemented
        return self.subject_name == other.subject_name and self.task == other.task and self.date == other.date

    def __repr__(self):
        values = [self.session_name, self.subject_name, self.task, str(self.clean)]
        return ','.join(values)


# DATAFILES
# group of files per subject
class DataFiles:
    def __init__(self, subject_name, files, params, trials, make_intersession, make_matlab):
        self.subject_name = subject_name
        self.files = files
        self.params = params
        self.trials = trials
        self.df = None
        self.global_df = None
        self.total_df = None
        self.make_intersession = make_intersession
        self.make_matlab = make_matlab
        self.intersession_df_path = None
        self.intersession_report_path = None
        self.filename = None
        self.filenamemat = None
        self.numbers = []



# DIRECTORIES, OPTIONS...
class Values:
    def __init__(self, settings):

        self.intersession = True

        if not arg.local and not arg.cluster:
            self.cluster = settings['OPTIONS']['CLUSTER'] == 'True'
            if settings['OPTIONS']['OVERWRITE'] == 'reports':
                self.overwrite_clean = False
                self.overwrite_reports = True
            elif settings['OPTIONS']['OVERWRITE'] == 'reports and clean data':
                self.overwrite_clean = True
                self.overwrite_reports = True
            else:
                self.overwrite_clean = False
                self.overwrite_reports = False
            self.globalfiles = settings['OPTIONS']['GLOBALFILES'] == 'True'
            self.filelog = settings['OPTIONS']['FILELOG'] == 'True'
            self.file = ''
        else:
            self.overwrite_reports = False
            self.overwrite_clean = False
            self.globalfiles = False
            self.filelog = False
            self.cluster = False
            self.file = ''
            if arg.overwrite:
                self.overwrite_reports = True
            if arg.overwriteall:
                self.overwrite_reports = True
                self.overwrite_clean = True
            if arg.globalfiles:
                self.globalfiles = True
            if arg.filelog:
                self.filelog = True
            if arg.cluster:
                self.cluster = True
            if arg.file:
                self.file = arg.file[0]
                self.overwrite_clean = True
                self.overwrite_reports = True
                self.intersession = False
        if self.cluster:
            self.raw_directory = None
            self.clean_directory = settings['CLUSTER']['CLEAN_DIRECTORY']
            self.report_directory = settings['CLUSTER']['REPORT_DIRECTORY']
        else:
            self.raw_directory = settings['LOCAL']['RAW_DIRECTORY']
            self.clean_directory = settings['LOCAL']['CLEAN_DIRECTORY']
            self.report_directory = settings['LOCAL']['REPORT_DIRECTORY']

        self.global_directory = settings['LOCAL']['GLOBAL_DIRECTORY']
        self.user = settings['CLUSTER']['USER']
        self.password = settings['CLUSTER']['PASSWORD']
        self.subject_names = settings['SELECTION']['SUBJECT_NAMES']
        self.task_names = settings['SELECTION']['TASK_NAMES']
        self.date_range = settings['SELECTION']['DATE_RANGE']
        self.exclude = settings['SELECTION']['EXCLUDE']
        self.filtering = settings['SELECTION']['FILTERING']
        self.default_task = settings['OPTIONS']['DEFAULT_TASK']

        self.cluster = Cluster(self.user, self.password) if self.cluster else None

# MAIN
def main():
    settings_file = get_settings_file()
    settings = configparser.ConfigParser()
    settings.read(settings_file)

    if not arg.local and not arg.cluster:
        stop = start_gui(settings, settings_file)
        if stop:
            return

    print('')
    v = Values(settings)
    set_logger(v.raw_directory, v.clean_directory, v.cluster, v.filelog)

    if v.raw_directory == v.clean_directory:
        logger.critical('Select different directories for raw and clean data.')
        return

    if v.raw_directory == v.report_directory:
        logger.critical('Select different directories for raw and reports.')
        return

    if v.raw_directory == v.global_directory:
        logger.critical('Select different directories for raw and global.')
        return

    if not v.cluster:
        raw_files = find_raw_files(v.raw_directory, v.clean_directory, v.report_directory, v.default_task, v.file)
        logger.info(str(len(raw_files)) + ' files found.' + timing.time())
    else:
        raw_files = []

    if v.cluster:
        animals = v.subject_names
    else:
        animals = None

    clean_files = find_clean_files(raw_files, v.clean_directory, v.report_directory, v.cluster, v.default_task, animals)

    if len(raw_files) > 0:
        calculate_clean_and_dailys(clean_files, v.clean_directory, v.report_directory,
                                   v.overwrite_clean, v.overwrite_reports, raw_files)

    if len(clean_files) == 0:
        logger.info('No clean data found and imposible to create new clean data')
        return

    if len(raw_files) > 0:
        allsubjects = create_subjects(clean_files, 'all', 'all', 'all', 'null', 'null',
                                      v.clean_directory, v.report_directory, v.cluster, True)
        if not v.globalfiles and v.intersession:
            calculate_intersessions(allsubjects, v.cluster, True)

    if v.globalfiles:
        subjects = create_subjects(clean_files, v.subject_names, v.task_names, v.date_range, v.exclude,
                                   v.filtering, v.global_directory, v.global_directory, v.cluster, False)
        if subjects:
            calculate_intersessions(subjects, v.cluster, False)
            calculate_globals(subjects, v.global_directory, v.cluster)
        else:
            logger.info('No subjects matching the criteria to create global files')


    if v.cluster:
        v.cluster.close()

    logger.info('')
    logger.info('-' * 30)
    print('')


def get_settings_file():
    distribution_directory = pkg_resources.get_distribution('datahandler').location
    settings_file = os.path.join(distribution_directory, 'datahandler/settings.ini')
    if not os.path.exists(settings_file):
        default_settings_file = os.path.join(distribution_directory, 'datahandler/default_settings.ini')
        with open(default_settings_file, "r") as inputfile:
            data = inputfile.read().splitlines(True)
        with open(settings_file, "w") as outputfile:
            outputfile.writelines(data[2:])
    return settings_file


def set_logger(raw_directory, clean_directory, cluster, filelog):
    if filelog:
        if cluster:
            try:
                cluster.sftp.chdir(clean_directory)
            except IOError:
                cluster.sftp.mkdir(clean_directory)
        else:
            if not os.path.exists(clean_directory):
                os.makedirs(clean_directory)
        filename = os.path.join(clean_directory, 'logs.log')
        logging.basicConfig(filename=filename, level=logging.ERROR)
        logger.setLevel(logging.INFO)
    else:
        logging.basicConfig(level=logging.ERROR)
        logger.setLevel(logging.INFO)
    logger.info('')
    if raw_directory is None:
        logger.info('Starting process')
    else:
        logger.info('Starting process, searching for raw data in: ' + raw_directory)


def path_generator(path, pattern, cluster, animals=None):
    paths = []
    if cluster:
        if animals == 'all' or animals == 'All':
            animals = [path]
        else:
            animals = re.split(',', animals)
        for animal in animals:
            path_to_walk = os.path.join(path, animal)
            try:
                for root, _, file in cluster.sftp_walk(path_to_walk):
                    for f in file:
                        if f.endswith(pattern):
                            paths.append(os.path.join(root, f))
            except:
                logger.critical('Invalid clean directory: ' + path)
    else:
        for root, _, file in os.walk(path):
            for f in file:
                if f.endswith(pattern):
                    paths.append(os.path.join(root, f))
    return sorted(paths)

def create_directories_for_raw(file, clean_directory, report_directory):

    clean_subject_directory = os.path.join(clean_directory, file.subject_name)
    report_subject_directory = os.path.join(report_directory, file.subject_name)

    params_path = os.path.join(clean_subject_directory, file.session_name + '_params.csv')
    trials_path = os.path.join(clean_subject_directory, file.session_name + '_trials.csv')
    daily_report_path = os.path.join(report_subject_directory, file.session_name + '.pdf')

    if not os.path.exists(clean_subject_directory):
        os.makedirs(clean_subject_directory)
    if not os.path.exists(report_subject_directory):
        os.makedirs(report_subject_directory)

    file.params_path = params_path
    file.trials_path = trials_path
    file.daily_report_path = daily_report_path
    file.clean_subject_directory = clean_subject_directory
    file.report_subject_directory = report_subject_directory
    return file


def find_raw_files(raw_directory, clean_directory, report_directory, default_task, file=''):
    data = []

    raw_paths = path_generator(raw_directory, '.csv', cluster=False)

    for raw_path in raw_paths:
        try:
            session_name = re.findall('\w+-\w+', raw_path)[-1]
            if file in session_name:
                values = re.split("_", session_name)
                subject_name = values[0]
                task = [values[i] for i in range(1, len(values) - 1)]
                task = '_'.join(task)
                date = values[len(values) - 1]
                first = True
                last = True
                for f in data:
                    if f.date[0:8] == date[0:8] and f.subject_name == subject_name and f.task == task:
                        first = False
                        f.last = False
                        date = f.date
                        session_name = f.session_name
                newfile = DataFile(session_name, subject_name, task, date, default_task, first, last, raw_path=raw_path)
                newfile = create_directories_for_raw(newfile, clean_directory, report_directory)
                data.append(newfile)
        except:
            pass
    return data


def find_clean_files(raw_files, clean_directory, report_directory, cluster, default_task, animals):
    data = []

    paths = path_generator(clean_directory, '.csv', cluster, animals)

    for path in paths:
        try:
            filename = re.findall('\w+-\w+', path)[-1]
            values = re.split("_", filename)
            if values[len(values) - 1] == 'params':
                subject_name = values[0]
                task = [values[i] for i in range(1, len(values) - 2)]
                task = '_'.join(task)
                date = values[len(values) - 2]
                session_name = '_'.join([subject_name, task, date])

                clean_subject_directory = os.path.join(clean_directory, subject_name)
                report_subject_directory = os.path.join(report_directory, subject_name)

                trials_path = os.path.join(clean_subject_directory, session_name + '_trials.csv')
                daily_report_path = os.path.join(report_subject_directory, session_name + '.pdf')

                if cluster:
                    if cluster.path_exists(trials_path) and cluster.path_exists(daily_report_path):
                        newfile = DataFile(session_name, subject_name, task, date, default_task, params_path=path,
                                           trials_path=trials_path, daily_report_path=daily_report_path,
                                           clean_subject_directory=clean_subject_directory,
                                           report_subject_directory=report_subject_directory)
                        data.append(newfile)
                else:
                    if os.path.exists(trials_path) and os.path.exists(daily_report_path):
                        newfile = DataFile(session_name, subject_name, task, date, default_task, params_path=path,
                                           trials_path=trials_path, daily_report_path=daily_report_path,
                                           clean_subject_directory=clean_subject_directory,
                                           report_subject_directory=report_subject_directory)
                        data.append(newfile)
                        for raw_file in raw_files:
                            if raw_file.session_name[:-6] == session_name[:-6]:
                                raw_file.clean = True
        except:
            pass
    return data


def calculate_clean_and_dailys(clean_files, clean_directory, report_directory,
                               overwrite_clean, overwrite_reports, raw_files):
    for file in raw_files:
        filenametoshow = re.findall('\w+-\w+', file.raw_path)[-1]

        if not file.clean or overwrite_clean or overwrite_reports:
            if not file.clean or overwrite_clean:
                try:
                    if file.selection.parse is None:
                        continue
                    parse_raw_file(file, clean_directory, report_directory)
                    logger.info('Data parse finished succesfully for file: ' + filenametoshow + timing.time())
                except Exception as error:
                    logger.critical('Data parse finished with error for file: ' + filenametoshow + timing.time())
                    logger.critical(error)
                    logger.critical(traceback.format_exc())
                    continue
            if file.last:
                file.done = True
                if not file.first:
                    for f in raw_files:
                        if f.session_name == file.session_name:
                            f.done = True
                make_daily(clean_files, file, filenametoshow)

    for file in raw_files:
        filenametoshow = re.findall('\w+-\w+', file.raw_path)[-1]
        if not file.clean or overwrite_clean or overwrite_reports:
            if not file.done:
                make_daily(clean_files, file, filenametoshow)


def make_daily(clean_files, file, filenametoshow):
    try:
        if file.selection.make_daily is None:
            logger.info('Daily report function not found for file: ' + filenametoshow + timing.time())
            return
        create_daily_report(clean_files, file,)
        logger.info('Daily report finished successfully for file: ' + filenametoshow + timing.time())
    except Exception as error:
        try:
            os.remove(file.daily_report_path)
        except OSError:
            pass
        logger.critical('Daily report finished with error for file: ' + filenametoshow + timing.time())
        logger.critical(error)
        logger.critical(traceback.format_exc())


def create_daily_report(clean_files, file):
    params = file.selection.make_daily(file.trials_path, file.params_path, file.daily_report_path)
    if params is not None:
        params = convert_everything_to_lists(params)
        params_df = pd.DataFrame([params])
        create_csv(params_df, file.params_path)
    if not file.clean and file.last:
        clean_files.append(file)

def calculate_intersessions(subjects, cluster, intersession_for_all):
    for subject_name, subject in subjects.items():
        if subject.make_intersession is None:
            logger.info('Intersession report function not found for animal: ' + subject_name + timing.time())
            continue
        try:
            create_intersession(subject, cluster, intersession_for_all)
            if intersession_for_all:
                logger.info('Intersession finished successfully for animal: ' + subject_name + timing.time())
        except Exception as error:
            logger.critical('Intersession finished with error for animal: ' + subject_name + timing.time())
            logger.critical(error)
            logger.critical(traceback.format_exc())


def create_intersession(subject, cluster, intersession_for_all):

    if cluster and intersession_for_all:
        create_csv(subject.df, subject.intersession_df_path)
        intersession_df_path = cluster.sftp.open(subject.intersession_df_path)
        intersession_report_path = cluster.sftp.open(subject.intersession_report_path, 'w', 10000)
        subject.make_intersession(intersession_df_path, intersession_report_path)
        intersession_df_path.close()
        intersession_report_path.close()
    else:
        create_csv(subject.df, subject.intersession_df_path)
        subject.make_intersession(subject.intersession_df_path, subject.intersession_report_path)


def parse_raw_file(file, clean_directory, report_directory):
    file.path = file.raw_path
    params, trials = file.selection.parse(file)

    if trials is None or params is None:  #empty file
        return

    params = convert_everything_to_lists(params)
    trials = convert_everything_to_lists(trials)

    if file.first:
        params_new = params
        trials_new = trials
        old_failure = True
    else:
        try:
            params_old = pd.read_csv(file.params_path, sep=';')

            check, params_sum, trials_plus_total_duration, trials_plus_total_trials = file.selection.glue_sessions()

            for variable in check:
                if params[variable] != params_old[variable].iloc[0]:
                    file.first = True
                    break

            if file.first:
                file.session_name = re.findall('\w+-\w+', file.raw_path)[-1]
                values = re.split("_", file.session_name)
                file.date = values[len(values) - 1]
                create_directories_for_raw(file, clean_directory, report_directory)
                params_new = params
                trials_new = trials
                old_failure = True

            else:
                try:
                    total_trials = params_old.total_trials.iloc[0]
                except:
                    total_trials = 0
                try:
                    total_duration = params_old.total_duration.iloc[0]
                except: total_duration = 0

                params_new = {}
                for element in params:
                    if element in params_sum:
                        params_new[element] = params[element] + params_old[element].iloc[0]
                    else:
                        params_new[element] = params[element]

                trials_new = {}
                for element in trials:
                    if element in trials_plus_total_trials:
                        try:
                            trials_new[element] = trials[element] + total_trials
                        except:
                            trials_new[element] = [x + total_trials for x in trials[element]]
                    elif element in trials_plus_total_duration:
                        try:
                            trials_new[element] = trials[element] + total_duration
                        except:
                            trials_new[element] = [list(x + total_duration) for x in trials[element]]
                    else:
                        trials_new[element] = trials[element]
                old_failure = False
        except:
            old_failure = True
            params_new = params
            trials_new = trials

    trials_new = convert_everything_to_lists(trials_new)

    params = pd.DataFrame([params_new])

    for key, elem in trials_new.items():
        try:
            if len(elem) == 0:
                elem = np.nan
                trials_new[key] = elem
        except:
            pass

    trials = pd.DataFrame(trials_new)

    params = convert_lists_to_strings(params)
    trials = convert_lists_to_strings(trials)

    if file.first or old_failure:
        params.to_csv(file.params_path, sep=';', na_rep='nan', index=False)
        trials.to_csv(file.trials_path, sep=';', na_rep='nan', index=False)
    else:
        params.to_csv(file.params_path, sep=';', na_rep='nan', index=False)
        with open(file.trials_path, 'a') as f:
            trials.to_csv(f, header=False, sep=';', na_rep='nan', index=False)


def calculate_globals(subjects, global_directory, cluster):
    for subject_name, subject in subjects.items():

        global_params_path = subject.intersession_df_path
        global_trials_path = os.path.join(global_directory, subject_name, subject.filename + '_global_trials.csv')
        # global_total_path = os.path.join(global_directory, subject_name, subject.filename + '_global.csv')
        matlab_path = os.path.join(global_directory, subject_name, subject.filenamemat)

        all_trials = []
        j = 0
        for i in range(len(subject.files)):
            if i in subject.numbers:
                j += 1
                trials = read_one_csv(subject.trials[i], cluster)
                trials['date'] = subject.files[i].date
                trials['session'] = j
                trials['subject_name'] = subject_name
                all_trials.append(trials)

        global_df = pd.concat([df for df in all_trials])
        subject.global_df = global_df

        # total_df = pd.merge(global_df, subject.df, on='session')
        # subject.total_df = total_df
        # create_csv(total_df, global_total_path)

        logger.info('Global data finished successfully for animal: ' + subject_name + timing.time())

        if subject.make_matlab is None:
            continue

        create_csv(global_df, global_trials_path)

        if 'p4_opto_ttl' in subject.df.task.values or 'p4_opto_ramp' in subject.df.task.values:
            subject.make_matlab = cellex4A_opto_to_matlab

        subject.make_matlab(global_params_path, global_trials_path, subject_name, matlab_path)
        logger.info('Matlab conversion finished successfully for animal: ' + subject_name + timing.time())

    _trials_path = os.path.join(global_directory, 'global_trials.csv')
    _params_path = os.path.join(global_directory, 'global_params.csv')
    _trials = pd.concat([subject.global_df for subject in subjects.values()])
    _params = pd.concat([subject.df for subject in subjects.values()])
    create_csv(_trials, _trials_path)
    create_csv(_params, _params_path)
    logger.info('Global data finished successfully')


def create_csv(df, path):
    df = convert_lists_to_strings(df)
    df.to_csv(path, sep=';', na_rep='nan', index=False)


def create_subjects(clean_files, subject_names, task_names, date_range, exclude, filtering, directory_csv,
                    directory_pdf, cluster, intersession_for_all):

    if subject_names == 'all' or subject_names == 'All':
        all_subjects = True
    else:
        all_subjects = False
        subject_names = re.split(',', subject_names)

    if task_names == 'all' or task_names == 'All':
        all_tasks = True
    else:
        all_tasks = False
        task_names = re.split(',', task_names)

    date_range = get_date_range(date_range)

    excludes = []
    if exclude != 'null':
        excludes_string = re.split(',', exclude)
        for exclude_range_string in excludes_string:
            exclude_range = get_date_range(exclude_range_string)
            excludes.append(exclude_range)

    if filtering == 'null' or filtering == '':
        filtering = []
    else:
        filtering = re.split(',', filtering)

    animals = {}
    clean_sorted_files = sorted(clean_files, key=lambda file: file.date)
    for file in clean_sorted_files:
        date_int = int(file.date[0:8])
        if file.subject_name in animals:
            if date_range[0] <= date_int <= date_range[1]:
                include = True
                for exclude in excludes:
                    if exclude[0] <= date_int <= exclude[1]:
                        include = False
                if include:
                    if file.task in task_names or all_tasks:
                        animals[file.subject_name].files.append(file)
                        animals[file.subject_name].params.append(file.params_path)
                        animals[file.subject_name].trials.append(file.trials_path)
        elif file.subject_name in subject_names or all_subjects:
            if file.task in task_names or all_tasks:
                if date_range[0] <= date_int <= date_range[1]:
                    include = True
                    for exclude in excludes:
                        if exclude[0] <= date_int <= exclude[1]:
                            include = False
                    if include:
                        animals[file.subject_name] = DataFiles(subject_name=file.subject_name, files=[file],
                                                               params=[file.params_path], trials=[file.trials_path],
                                                               make_intersession=file.selection.make_intersession,
                                                               make_matlab=file.selection.make_matlab)

    for animal_name, file in animals.items():
        if cluster:
            dfs = []
            for path in file.params:
                with cluster.sftp.open(path) as f:
                    dfs.append(pd.read_csv(f, sep=';'))
        else:
            dfs = [pd.read_csv(path, sep=';') for path in file.params]

        dfs2 = []
        numbers = []

        for index, df in enumerate(dfs):
            add_element = True
            for filt in filtering:
                if filt in df.values:
                    add_element = False
            if df.task.iloc[0] not in task_names and not all_tasks:
                add_element = False
            if add_element:
                dfs2.append(df)
                numbers.append(index)

        file.numbers = numbers

        df = pd.concat(dfs2)

        session = list(range(1, df.shape[0] + 1))
        df['session'] = session
        file.df = df

        mindate = min(df.date)
        maxdate = max(df.date)

        if intersession_for_all:
            file.filename = file.subject_name + '_intersession'
            file.filenamemat = None
            extra =''
        else:
            directory = os.path.join(directory_csv, file.subject_name)
            extra ='_global_params'
            if not os.path.exists(directory):
                os.makedirs(directory)

            if mindate == maxdate:
                file.filename = file.subject_name + '_' + mindate
                file.filenamemat = file.subject_name + 'PreProcessData' + mindate + '.mat'
            else:
                file.filename = file.subject_name + '_' + mindate[0:8] + '-' + maxdate[0:8]
                file.filenamemat = file.subject_name + 'PreProcessData' + mindate[0:8] + '-' + maxdate[0:8] + '.mat'

        file.intersession_df_path = os.path.join(directory_csv, file.subject_name, file.filename + extra + '.csv')
        file.intersession_report_path = os.path.join(directory_pdf, file.subject_name, file.filename + '.pdf')

    return animals


def read_csvs(params_path, trials_path, cluster):
    if cluster:
        params_path = cluster.sftp.open(params_path)
        trials_path = cluster.sftp.open(trials_path)
        params = pd.read_csv(params_path, sep=';')
        trials = pd.read_csv(trials_path, sep=';')
        trials_path.close()
        params_path.close()
    else:
        params = pd.read_csv(params_path, sep=';')
        trials = pd.read_csv(trials_path, sep=';')

    return params, trials

def read_one_csv(path, cluster):
    if cluster:
        path = cluster.sftp.open(path)
        df = pd.read_csv(path, sep=';')
        path.close()
    else:
        df = pd.read_csv(path, sep=';')

    return df


def convert_everything_to_lists(my_dict):
    for key in my_dict.keys():
        elem = my_dict[key]
        if isinstance(elem, (np.ndarray, pd.Series)):
            elem = elem.tolist()
            my_dict[key] = elem
        elif isinstance(elem, list):
            elem2 = []
            for item in elem:
                try:
                    item2 = item.tolist()
                    elem2.append(item2)
                except:
                    elem2.append(item)
            my_dict[key] = elem2
    return my_dict


def convert_lists_to_strings(df):
    def list_to_string(mylist):
        if len(mylist) > 0:
            return ','.join(str(x) for x in mylist)
        else:
            return 'nan'

    for column in df:
        try:
            first = df[column].iloc[0]
            if isinstance(first, list):
                df[column] = df[column].map(list_to_string)
        except:
            pass
    return df


def get_date_range(date_range):

    if date_range == 'all' or date_range == 'All':
        initial_date = 0
        last_date = 99999999
    else:
        try:
            date_range = re.split('-', date_range)
            initial_date = date_range[0]
            last_date = date_range[1]
            if initial_date == 'start':
                initial_date = 0
            else:
                initial_date = int(initial_date)
            if last_date == 'end':
                last_date = 100000000
            else:
                last_date = int(last_date)
        except:
            initial_date = 0
            last_date = 100000000

    return [initial_date, last_date]


def start_gui(settings, settings_file):
    app = ApplicationGui(settings, settings_file)
    app.window.call('wm', 'attributes', '.', '-topmost', '1')
    app.mainloop()
    if not app.stop:
        print('')
        input('    press a key to proceed  >>>>')
        print('')
        print('Generating data and reports...')
    return app.stop


# MAIN
if __name__ == "__main__":
    main()
