import paramiko
import os
from stat import S_ISDIR


class Cluster:

    def __init__(self, username, password):

        hostname = 'neurocomp.fcrb.es'
        port = 4022

        self.client = paramiko.SSHClient()
        self.client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
        self.client.connect(hostname=hostname, port=port, username=username, password=password)
        self.sftp = self.client.open_sftp()

    def sftp_walk(self, remotepath):
        """
        Kindof a stripped down version of os.walk, implemented for sftp.
        """
        path = remotepath
        files = []
        folders = []
        for f in self.sftp.listdir_attr(remotepath):
            if S_ISDIR(f.st_mode):
                folders.append(f.filename)
            else:
                files.append(f.filename)
        yield path, folders, files
        for folder in folders:
            new_path = os.path.join(remotepath, folder)
            for x in self.sftp_walk(new_path):
                yield x

    def path_exists(self, path):
        try:
            self.sftp.stat(path)
            return True
        except FileNotFoundError:
            return False

    def close(self):
        self.sftp.close()
        self.client.close()
