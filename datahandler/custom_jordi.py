import warnings
warnings.filterwarnings('ignore')
import os
import matplotlib as mpl
mpl.use('Agg')
import matplotlib.pyplot as plt
import pandas as pd
import numpy as np
import seaborn as sns
from ast import literal_eval
#import matplotlib as mpl
from matplotlib.backends.backend_pdf import PdfPages
# from matplotlib.lines import Line2D
from datahandler import Utils
import time
from statsmodels.stats.proportion import proportion_confint
#if os.environ.get('DISPLAY', '') == '':
#    mpl.use('Agg')


def rolling_window(a, window):
    shape = a.shape[:-1] + (a.shape[-1] - window + 1, window)
    strides = a.strides + (a.strides[-1],)
    c = np.lib.stride_tricks.as_strided(a, shape=shape, strides=strides)
    return c

# replace function name and adapt it to feedback_sessionsthis name
# even better, detect it automatically, without passing arguments
def report_noenv(csvpath, framestamppath, pdfpath=None):
    '''args: paths
    if pdfpath is None it will be saved at tmp dir
    [creates pdf]
    returns pdfpath so it can be easily sent through slackbot'''
    targ = csvpath
    npypath=framestamppath
    if npypath is not None:
        tvec=np.load(npypath, allow_pickle=True)


    df1 = pd.read_csv(targ,  sep = ';', skiprows = 6, error_bad_lines = False)

    # set flag if envelopes
    if len(df1[df1.MSG=='left_envelope']):
        envelope = True
    else:
        envelope = False
    df1['PC-TIME'] = pd.to_datetime(df1['PC-TIME'])
    df1['trial_index']=np.nan
    df1.loc[(df1.TYPE=='VAL')&(df1.MSG=='coherence01'), 'trial_index'] = np.arange(1,1+len(df1.loc[(df1.TYPE=='VAL')&(df1['MSG']=='coherence01')]))
    df1['trial_index'].fillna(method='ffill', inplace=True)
    states = df1[df1.TYPE=='STATE'].sort_values(['trial_index', 'BPOD-INITIAL-TIME']).reset_index(drop=True) # new sort_values but should work
    states['+INFO'] = states['+INFO'].astype(float)
    fix = states[states.MSG=='Fixation'] ##
    id_ss = states[states['MSG']=='StartSound'].index.values # startsound indexes
    id_wcp1 = states.drop_duplicates(subset='trial_index').index.values # first waitcpoke per trial
    tmp = states.drop(np.concatenate([id_ss-1, id_ss-2, id_wcp1])) # drop states unrelated to fb
    tmptrial_index = tmp.loc[tmp.MSG=='WaitCPoke', 'trial_index'].values
    try: # this was giving issues, i guess that rare ocasions where sessions ends during fixation
        fb_length = tmp.loc[tmp.MSG=='WaitCPoke', 'BPOD-INITIAL-TIME'].values - tmp.loc[tmp.MSG=='Fixation_fb', 'BPOD-INITIAL-TIME'].values
    except:
        fb_length = tmp.loc[tmp.MSG=='WaitCPoke', 'BPOD-INITIAL-TIME'].values - tmp.loc[tmp.MSG=='Fixation_fb', 'BPOD-INITIAL-TIME'].values[:-1]
    fbdf = pd.DataFrame({'len':fb_length, 'trial_index': tmptrial_index})
    missingidxmask = np.isin(np.arange(id_ss.size).astype(float)+1, tmptrial_index, invert=True)
    missingidx = (np.arange(id_ss.size).astype(float)+1)[missingidxmask]
    fbdf = fbdf.append(pd.DataFrame({'len': np.repeat(np.nan, missingidxmask.sum()), 'trial_index': missingidx}), ignore_index=True)
    fb = fbdf.groupby('trial_index')['len'].apply(list).apply(lambda x: x if (~np.isnan(x)).sum() else []).values
    coh = df1[df1.MSG=='coherence01']['+INFO'].astype(float).values
    trialidx = np.arange(1, len(coh)+1, step=1)
    rewside = np.array(literal_eval(df1.loc[df1.loc[df1.MSG=='REWARD_SIDE', '+INFO'].index.values[-1], '+INFO']))[:len(coh)]
    hithistory = np.where(states[states.MSG=='Reward']['BPOD-FINAL-TIME'].astype(float)>0, 1, np.nan)
    hithistory[np.where(states[states.MSG=='Punish']['BPOD-FINAL-TIME'].astype(float)>0)[0]] = 0
    hithistory[np.where(states[states.MSG=='Invalid']['BPOD-FINAL-TIME'].astype(float)>0)[0]] = -1
    if envelope:
        hithistory[np.where(states[states.MSG=='invReward']['BPOD-FINAL-TIME'].astype(float)>0)[0]] = 1
        #hithistory[np.where(states[states.MSG=='invPunish']['BPOD-FINAL-TIME'].astype(float)>0)[0]] = 0
        # switch those reward_sides where it was inverted due to misleading stim:
        sc_switched_responses = df1.loc[df1.MSG=='fair_sc_switch_rewside', '+INFO'].values.astype(int)
        rewside[sc_switched_responses] = (rewside[sc_switched_responses]-1)**2
            
    if len(df1.loc[df1.MSG=='enhance_com_switch']): # likely altered from original reward_side
        if float(literal_eval(df1.loc[df1.MSG=='TASK','+INFO'].values[0])[1])<0.3:
            # task earlier than v0.3 were sub-optimal
            # active ports:
            dict_events = dict(zip([x for x in range(68, 85, 2)], [
                                    f'Port{x+1}' for x in range(8)]))
            active_ports = sorted([dict_events[x] for x in dict_events.keys(
                ) if x in df1.loc[df1.TYPE == 'EVENT', 'MSG'].astype(int).unique()])        
            # whether they are correct is fine, just need to make sure side is fine!
            comswitchtrials = df1.loc[df1.MSG=='enhance_com_switch', 'trial_index'].values # perhaps not all of them are switch
            eventdfwobnc = df1.loc[df1.trial_index.isin(comswitchtrials)&(df1.TYPE=='EVENT')& ~(df1['+INFO'].str.startswith('BNC', na=False))]

            pat=np.asarray(['Tup', 'Tup', active_ports[1]+'Out'])
            N = pat.size
            arr = eventdfwobnc['+INFO'].values
            b = np.all(rolling_window(arr, N) == pat, axis=1)
            c = np.mgrid[0:len(b)][b]

            d = [i  for x in c for i in range(x, x+N)]
            eventdfwobnc['onset'] = np.in1d(np.arange(len(arr)), d)
            # iterate to avoid weird patterns/coincidences
            #switched_responses = [] # no need to keep it
            for i, tr in enumerate(comswitchtrials):
                try:
                    indx = eventdfwobnc.loc[(eventdfwobnc.trial_index==tr)&eventdfwobnc.onset].index.max()
                    c_resp = eventdfwobnc.loc[(eventdfwobnc.index>indx+1) &(eventdfwobnc.trial_index==tr)&~(eventdfwobnc['+INFO'].str.startswith(active_ports[1])) & (eventdfwobnc.MSG!='104'), '+INFO'].values[0][:5]
                    if active_ports[0]==c_resp: #rat response was left
                        if hithistory[int(tr)-1]==1: # it was a hit
                            rewside[int(tr)-1]= 0
                        elif hithistory[int(tr)-1]==0: # miss # we do not know with invalids
                            rewside[int(tr)-1]= 1
                    elif active_ports[1]==c_resp: # rat response was right
                        if hithistory[int(tr)-1]==1: # it was a hit
                            rewside[int(tr)-1]= 1
                        elif hithistory[int(tr)-1]==0: # miss # we do not know with invalids
                            rewside[int(tr)-1]= 0
                except:
                    print(f'coud not elucidate which was the reward side in trial {tr}')
            # get responses # tup tup PortXout
            # this will work too for CoM pipe
            # https://stackoverflow.com/questions/48710783/pandas-find-and-index-rows-that-match-row-sequence-pattern
            
            # get choices and then get rew-side using hithistory
        else:
            # issue solved in vers 0.3!
            # just invert rewside in those trials
            comswitchtrials = df1.loc[df1.MSG=='enhance_com_switch', '+INFO'].values.astype(int)
            rewside[comswitchtrials] = (rewside[comswitchtrials]-1)**2

    RResponse = np.zeros(len(hithistory))
    RResponse[np.where(np.logical_and(rewside==1, hithistory==1)==True)[0]] = 1 # right and correct
    RResponse[np.where(np.logical_and(rewside==0, hithistory==0)==True)[0]] = 1 # left and incorrect
    RResponse[~(hithistory>=0)] = np.nan # else, invalids will be considered L_responses

    sr_play = df1.loc[(df1.MSG.str.startswith('SoundR: Play.'))&(df1.TYPE=='stdout'), 'trial_index'].values
    sr_stop = df1.loc[(df1.MSG.str.startswith('SoundR: Stop.'))&(df1.TYPE=='stdout'), 'trial_index'].values
    soundrfail = sr_stop[np.isin(sr_stop, sr_play, assume_unique=True, invert=True)].astype(int) # 1 based trial index
    soundrok = sr_stop[np.isin(sr_stop, sr_play, assume_unique=True, invert=False)].astype(int) # inverse
    startpctime = df1.loc[df1.MSG.str.startswith('SoundR: P') & df1.trial_index.isin(soundrok), 'PC-TIME'].values
    stoppctime = df1.loc[df1.MSG.str.startswith('SoundR: S') & df1.trial_index.isin(soundrok), 'PC-TIME'].values
    soundr_len = (stoppctime - startpctime).astype(float)/1000000 

    onset1 = df1.loc[(df1.TYPE=='EVENT')&(df1['MSG']=='60')].groupby(['trial_index','MSG'])['BPOD-INITIAL-TIME'].agg('first').values
    offset1 = df1.loc[(df1.TYPE=='EVENT')&(df1['MSG']=='61')].groupby(['trial_index','MSG'])['BPOD-INITIAL-TIME'].agg('last').values
    onset2 = df1.loc[(df1.TYPE=='EVENT')&(df1['MSG']=='62')].groupby(['trial_index','MSG'])['BPOD-INITIAL-TIME'].agg('first').values
    offset2 = df1.loc[(df1.TYPE=='EVENT')&(df1['MSG']=='63')].groupby(['trial_index','MSG'])['BPOD-INITIAL-TIME'].agg('last').values

    idxes = [np.empty(0), np.empty(0)]
    BNCok =  [True, True]
    msgs = ['60','62']
    for i in [0,1]:
        try:
            idxes[i] = df1.loc[(df1.TYPE=='EVENT')&(df1['MSG']==msgs[i])].groupby(['trial_index','MSG'])['BPOD-INITIAL-TIME'].apply(list).apply(lambda x: x[0]).index.droplevel(1)
        except:
            print(f'BNC{i+1} not OK')
            BNCok[i] = False
        
    #idxes2 = df1.loc[(df1.TYPE=='EVENT')&(df1['MSG']=='62')].groupby(['trial_index','MSG'])['BPOD-INITIAL-TIME'].apply(list).apply(lambda x: x[0]).index.droplevel(1)

    res = pd.DataFrame(data=np.array([trialidx, coh, rewside, RResponse, hithistory]).T, columns=['origidx', 'coh', 'rewside', 'R_response','hithistory'])
    res['onset1'],res['offset1'], res['onset2'], res['offset2']=np.nan, np.nan,np.nan ,np.nan

    if BNCok[0]: # if BNC was not ok, they'll remain as np.nans
        res.loc[(res.origidx.isin(idxes[0])), 'onset1']=onset1 # empty if 
        res.loc[res.origidx.isin(idxes[0]), 'offset1']=offset1
    if BNCok[1]:
        res.loc[res.origidx.isin(idxes[1]), 'onset2']=onset2
        res.loc[res.origidx.isin(idxes[1]), 'offset2']=offset2

    res['albert_fail'] = False
    res.loc[res[['onset1','onset2']].isna().values.sum(axis=1)==2,'albert_fail'] = True


    res.loc[res.onset1.isna(), 'onset1'] = np.inf
    res.loc[res.onset2.isna(), 'onset2'] = np.inf
    # get first and last etc.

    res['bpod_timer'] = df1.loc[(df1.TYPE=='STATE')&(df1.MSG=='StartSound'), 'BPOD-INITIAL-TIME'].values
    res['aftercorrect']=res.hithistory.shift(1)
    origidx_ac = res.loc[res.aftercorrect==1, 'origidx'].values
    origidx_ae = res.loc[res.aftercorrect==0, 'origidx'].values
    toplot = df1.loc[df1.MSG=='TRIAL-BPOD-TIME', ['trial_index','BPOD-INITIAL-TIME', '+INFO']].astype(float).values

    res['rep_resp'] = 0
    res.loc[res.R_response.diff(1).abs()==0,'rep_resp']=1
    res.loc[res.origidx==1, 'rep_resp'] = np.nan
    res['responsesigned'] = res.R_response * 2 -1
    res['ev_repeat'] = (res.responsesigned.shift(1) * (res.coh*2-1))
    res['albert_onset'] = np.nan
    res.loc[res.origidx.isin(soundrok),'albert_onset'] = res.loc[res.origidx.isin(soundrok),['onset1', 'onset2']].values.min(axis=1)
    res.loc[:,'albert_onset'] -= res.bpod_timer
    res.loc[:,'albert_onset'] *= 1000 # ms
    res.loc[res.albert_onset==np.inf,'albert_onset']=np.nan
    res['prob_repeat'] = df1.loc[df1.MSG=='prob_repeat', '+INFO'].astype(float).values

    delay_on_purpose = df1.loc[df1.MSG=='expected_delay', '+INFO'].values.astype(float)
    delay_trials = df1.loc[df1.MSG=='delayed_trial', '+INFO'].values.astype(int)
    res['expected_delay'] = 0
    res.loc[delay_trials, 'expected_delay'] = delay_on_purpose * 1000 # in ms
    res.loc[:,'albert_onset'] = res['albert_onset'] - res['expected_delay']
    a = df1.loc[
        ~(df1.trial_index.isin(delay_trials+1))&(df1.TYPE=='TRANSITION') & df1.trial_index.isin(soundrok) & (df1.MSG=='StartSound'), 
        'PC-TIME'
    ].values
    
    aa = df1.loc[
        (df1.trial_index.isin(delay_trials+1))&(df1.TYPE=='TRANSITION') & df1.trial_index.isin(soundrok) & (df1.MSG=='StartSound'), 
        'PC-TIME'
    ].values
    b = df1.loc[
        ~(df1.trial_index.isin(delay_trials+1))&(df1.trial_index.isin(soundrok)) & (df1.TYPE=='stdout') & (df1.MSG=='SoundR: Play.'), 
        'PC-TIME'
        ].values
    bb = df1.loc[
        (df1.trial_index.isin(delay_trials+1))&(df1.trial_index.isin(soundrok)) & (df1.TYPE=='stdout') & (df1.MSG=='SoundR: Play.'), 
        'PC-TIME'
        ].values
    # expected delays for those trials
    cc = df1.loc[(df1.trial_index.isin(soundrok))&(df1.MSG=='expected_delay'), '+INFO'].values.astype(float)
    if float(literal_eval(df1.loc[df1.MSG=='TASK','+INFO'].values[0])[1])<0.4: # handle old bug
        if cc.size!=aa.size:
            print(f'attempting to correct delay_trials shape missmatch ({aa.shape} vs {cc.shape})')
            cc = df1.loc[(df1.trial_index.isin(soundrok-1))&(df1.MSG=='expected_delay'), '+INFO'].values.astype(float)

    # copy entire scheme:
    mpl.rcParams['lines.linewidth'] = 0.7
    mpl.rcParams['lines.markersize'] = 4
    mpl.rcParams['font.size'] = 7
    mpl.rcParams['axes.spines.right'] = False
    mpl.rcParams['axes.spines.top'] = False
    mpl.rcParams['errorbar.capsize'] = 3
    label_kwargs = {'fontsize': 9, 'fontweight': 'roman'}




    # df.loc[df.MSG=='delayed_trial'] # gets 0-based index in +INFO, 1-based in trial_index 
    # df.loc[df.MSG=='expected_delay'] # +info: time length; "
    # df.loc[df.MSG=='enhance_com_switch']# gets 0-based index in +INFO, 1-based in trial_index 

    if pdfpath is None:
        pdfpath = f'/tmp/{os.path.split(targ)[1]}.pdf'

    with PdfPages(pdfpath) as pdf:
        plt.figure(figsize=(11.7, 8.3))
        #plt.figure(figsize=(17.55, 12.45))
        # 1st: rolling av perf
        ax = plt.subplot2grid((100, 100), (0, 0), rowspan=25, colspan=85)
        for i in [0.25,0.5,0.75]:
            ax.axhline(i, ls=':', color='gray', alpha=0.3)

        kw = dict(marker='o', markersize='2')
        ax.plot(res.hithistory.rolling(20, min_periods=1).mean(), c='k', **kw, label='all')
        ax.plot(res.loc[res.rewside==0,'hithistory'].rolling(20, min_periods=1).mean(),c='cyan', **kw, label='left accu')
        ax.plot(res.loc[res.rewside==1,'hithistory'].rolling(20, min_periods=1).mean(), c='magenta', **kw, label='right accu')
        # 20210414 new jaime request to replace Zt
        ax.plot(1-res.loc[res.rep_resp==1,'hithistory'].rolling(20, min_periods=1).mean(),c='r', **kw, label='rep errors')
        ax.plot(1-res.loc[res.rep_resp==0,'hithistory'].rolling(20, min_periods=1).mean(),c='b', **kw, label='alt errors')
        ax.set_ylabel('fraction', **label_kwargs)
        # if len(df1.loc[df1.MSG=='zt_repalt', '+INFO']): # only if Zt is tracked
        #     # convert to 0.25 to 0.5 space to place it in the same fig
        #     ax.axhline(y=0.25, c='tab:orange', ls=':')
        #     ax.plot(
        #         df1.loc[df1.MSG=='zt_repalt', '+INFO'].values.astype(float)/4+0.25,
        #         color='tab:orange', label='Zt LR'
        #     )

        ax.set_xlim([0,res.shape[0]])
        handles, labels = ax.get_legend_handles_labels()
        #ax.legend(loc="lower left", ncol=1, prop={'size': 8})
        ax.spines['bottom'].set_visible(False)

        fatherylim = ax.get_ylim()



        #1st marginal
        ax = plt.subplot2grid((100, 100), (0, 85), rowspan=25, colspan=15)
        tmp = res.groupby('rewside')['hithistory'].agg(['count','sum','mean'])
        tmp.loc[-1, :] = [res.shape[0], res.hithistory.dropna().sum(),res.hithistory.dropna().mean()]
        tmp.sort_index(inplace=True)
        tmp['ci_l'], tmp['ci_u'] = proportion_confint(tmp['sum'], tmp['count'], method='beta')
        tmp['ci_l'], tmp['ci_u'] = tmp['mean']-tmp['ci_l'], tmp['ci_u']-tmp['mean']
        toplotdic = {
            'rowi': [-1.0,0.,1.0],
            'lab': ['all', 'L', 'R'],
            'c': ['k', 'cyan', 'magenta']
        }
        for i in [0.25,0.5,0.75]:
            ax.axhline(i, ls=':', color='gray', alpha=0.3)
        for i in range(3):
            ax.errorbar(toplotdic['rowi'][i], tmp.loc[toplotdic['rowi'][i],'mean'], 
                yerr=([tmp.loc[toplotdic['rowi'][i], 'ci_l']], [tmp.loc[toplotdic['rowi'][i], 'ci_u']]),
                marker='o', ls='', markersize=2, color=toplotdic['c'][i])

        ax.set_xticks([])
        #ax.set_xticklabels(toplotdic['lab'])
        for loc in ['top', 'right', 'bottom', 'left']:
            ax.spines[loc].set_visible(False)
        ax.set_yticks([])
        ax.set_ylim(fatherylim)
        ax.set_xlim([-3, 3])
        ax.axvline(-2, c='k')
        ax.axvline(-2.25, c='k')
        ax.legend(handles, labels, loc='lower right')


        ## TEXT
        t = os.path.split(targ)[1].split('_')[-1].split('.')[0]
        t = time.strptime(t,'%Y%m%d-%H%M%S')
        subj = literal_eval(df1.loc[df1.MSG=='SUBJECT-NAME', '+INFO'].values[0])[0]
        s1=subj+' session: '+time.strftime('%d of %b, %Y at %H:%M', t) + ' '+df1.loc[df1.MSG=='BOARD-NAME', '+INFO'].values[0]
        tot_water = res.hithistory.sum() * 0.024

        if tot_water<10:
            pcolor = 'r'
        else:
            pcolor = 'k'
        if res.loc[res.expected_delay==0,'albert_fail'].sum()<5:
            afcolor='k'
        else: # dismiss late_Stim trials!
            afcolor='r'
        plt.text(0.1,0.97,s1, fontsize=8, transform=plt.gcf().transFigure)
        plt.text(0.1,0.94,f'Drank a total of {round(tot_water,2)} ml in {int(round(toplot[-1,1]/60, 0))} minutes', 
            color=pcolor, fontsize=8, transform=plt.gcf().transFigure)
        plt.text(0.1,0.91,f"with {res.loc[res.expected_delay==0,'albert_fail'].sum()} unnoticed stim (according to Albert's board)", 
            color=afcolor, fontsize=8, transform=plt.gcf().transFigure)

        # 2nd, correc/incorrect 
        axes = plt.subplot2grid((100, 100), (29, 0), rowspan=8, colspan=85)
        axes.axhline(0, ls=':', color='gray')
        axes.scatter(res.origidx.astype(int)-1, res.coh*2-1, color=res.hithistory.fillna(-1).map({0:'red', 1:'green', -1:'grey'}), edgecolors='white')
        axes.set_xlim([1, res.origidx.max()])
        axes.set_ylim(-1.15,1.15)
        axes.set_yticks([-1.05, 1.05])
        axes.set_yticklabels(['Left', 'Right'])
        axes.invert_yaxis()
        #axes.set_xlabel('Trials')
        axes.set_ylabel('')
        axes.xaxis.set_ticks_position('top')
        axes.set_xticklabels([])
        axes.spines['bottom'].set_visible(False)
        fatherylim = ax.get_ylim()


        ## marginal summary
        ax = plt.subplot2grid((100, 100), (29, 85), rowspan=8, colspan=15)
        for i in np.linspace(0,1,5):
            ax.axvline(i, ls=':', color='gray', alpha=0.3)
        tmp = res.groupby('coh')['hithistory'].agg(['count','sum','mean'])
        tmp['ci_l'], tmp['ci_u'] = proportion_confint(tmp['sum'], tmp['count'], method='beta')
        tmp['ci_l'], tmp['ci_u'] = tmp['mean']-tmp['ci_l'], tmp['ci_u']-tmp['mean']
        ax.errorbar(
            tmp['mean'].values, 
            (tmp.index.values*2-1)[::-1], 
            xerr=[tmp.ci_l.values,tmp.ci_u.values], 
            marker='o', ls='', markersize=2, c='blue', capsize=0)
        # ax.invert_yaxis()
        ax.set_xlim([0,1.05])
        ax.set_ylim([-1.15,1.15])
        for loc in ['top', 'right', 'bottom', 'left']:
            ax.spines[loc].set_visible(False)
        ax.set_yticks([])
        ax.xaxis.set_ticks_position('top')
        ax.set_xticks([.25,.5,.75,1])
        ax.set_xticklabels((np.array([.25,.5,.75,1])*100).astype(int))

        # cpokes
        ax = plt.subplot2grid((100, 100), (41, 0), rowspan=29, colspan=29)
        kws = dict(kde=False, bins=np.linspace(0,0.8, 50),ax=ax)
        ax.axvline(0.3, ls=':', c='k')

        all_cpokes = np.concatenate(
                [np.concatenate(fb),
                df1.loc[(df1.TYPE=='STATE')&(df1.MSG=='StartSound'), '+INFO'].astype(float).dropna().values + 0.3])
        ac_only = np.concatenate([fbdf.loc[fbdf.trial_index.isin(origidx_ac), 'len'].dropna().values,
                df1.loc[(df1.TYPE=='STATE')&(df1.MSG=='StartSound')&(df1.trial_index.isin(origidx_ac)), '+INFO'].astype(float).dropna().values + 0.3
        ])
        ae_only = np.concatenate([fbdf.loc[fbdf.trial_index.isin(origidx_ae), 'len'].dropna().values,
                df1.loc[(df1.TYPE=='STATE')&(df1.MSG=='StartSound')&(df1.trial_index.isin(origidx_ae)), '+INFO'].astype(float).dropna().values + 0.3
        ])
        sns.distplot(all_cpokes, **kws) # keep fixed bins
        sns.distplot(ac_only, **kws, hist_kws={'histtype':'step', 'lw':3}, color='g') # =False, hist_kws={'histtype':'step'},
        sns.distplot(ae_only, **kws, hist_kws={'histtype':'step', 'lw':3},color='r')
        ax.set_title('c-pokes')
        ax.set_xticks([])
        ax.set_ylabel('counts', **label_kwargs)





        # cpokes normalized:
        ax = plt.subplot2grid((100, 100), (71, 0), rowspan=29, colspan=29)
        kws = dict(kde=False, norm_hist=True,bins=np.linspace(0,0.8, 50),ax=ax)
        ax.axvline(0.3, ls=':', c='k')
        sns.distplot(all_cpokes,  label='all', **kws) # keep fixed bins
        sns.distplot(ac_only, label='ac', **kws, hist_kws={'histtype':'step', 'lw':3}, color='g') # =False, hist_kws={'histtype':'step'},
        sns.distplot(ae_only, label='ae', **kws, hist_kws={'histtype':'step', 'lw':3},color='r')
        ax.legend()
        ax.set_xlabel('length(s)')
        ax.set_ylabel('norm counts', **label_kwargs)



        # delay plot1
        ax = plt.subplot2grid((100, 100), (74, 36), rowspan=26, colspan=29) # or 29
        dist1 = res.albert_onset.dropna()
        dist2 = np.concatenate((
            (b-a).astype(int)/1e6,
            (bb-aa).astype(int)/1e6 - cc*1000 # same but substracting expected delay (which could result in <0)
        ))
        kwargs= dict(bins=np.linspace(0,25,50), kde=False, norm_hist=True, ax=ax)
        sns.distplot(dist1, **kwargs, label='sound:pcb')
        sns.distplot(dist2, **kwargs, label='sound:pc_timer')
        ax.set_xlabel('delay (ms)')
        ax.set_ylabel('norm counts')
        ax.axvline(dist1.mean(), ls=':', c='tab:blue')
        ax.annotate( 
            str(round(dist1.mean(), 2)), 
            xy=(dist1.mean(), ax.get_yticks()[-2]),
            fontsize=7, color='tab:blue')
        ax.annotate( 
            str(round(dist2.mean(), 2)), 
            xy=(dist2.mean(), ax.get_yticks()[-2]),
            fontsize=7, color='tab:orange')

        ax.axvline((b-a).astype(int).mean()/1e6, ls=':', c='tab:orange')
        ax.legend(loc='center right')
        for bncnum, BNC in enumerate(BNCok):
            if not BNC:
                ax.annotate(
                    f'BNC{int(bncnum+1)} not detected!',
                    xy=(15, bncnum*0.2),
                    fontsize=7, color='red'
                )
        
        ax = plt.subplot2grid((100, 100), (74, 69), rowspan=26, colspan=31)
        if npypath is not None:
            dist3 = pd.Series(tvec).diff(1).values[1:].astype(int)/1e6
        
            kwargs= dict(bins=np.linspace(0,50,100), kde=False, norm_hist=True, ax=ax)
            ax.axvline(dist3.mean(), ls=':', c='tab:green')
            sns.distplot(dist3, label='video frames', color='tab:green', **kwargs)
            ax.annotate( 
                str(round(dist3.mean(), 2)), 
                xy=(dist3.mean(), ax.get_yticks()[-2]),
                fontsize=7, color='tab:green')
            ax.set_xlim([0,50])
            ax.set_xlabel('delay (ms)')
            ax.legend(loc='center right')
        else:
            ax.text(0,0, 'framestamp vector is None')


        axes = plt.subplot2grid((100, 100), (41, 75), rowspan=29, colspan=29, aspect=2) # aspect2 because spans={ y:0to1, x:-1 to 1}
        coherences = res.loc[res.R_response.notna(), 'coh'].round(3) # rounding because due to precission (task baed numpy vs native python floats) unique() was not OK
        response = res.loc[res.R_response.notna(), 'R_response']

        # we need coherences from -1 to 1, response from 0 to 1
        coherences = coherences * 2 - 1

        psych_curve = Utils.compute_psych_curve(coherences, response)

        axes.axhline(0.5, color='gray', ls=':')
        axes.axvline(0., color='gray', ls=':')

        axes.errorbar(psych_curve.xdata, psych_curve.ydata,
                        yerr=psych_curve.fit_error, color='k', elinewidth=1,marker='o', markersize=2, linestyle='none')
        axes.plot(np.linspace(-1, 1, 30), psych_curve.fit, color='k', linewidth=1, label='L-R')

        axes.set_ylabel('Prob')
        axes.set_xlabel('Evidence')

        axes.set_xlim([-1.05, 1.05])
        axes.set_ylim([-0.025, 1.025])
        axes.annotate(str(round(psych_curve.ydata[0], 2)), xy=(psych_curve.xdata[0], psych_curve.ydata[0]),
                        xytext=(psych_curve.xdata[0] - 0.03, psych_curve.ydata[0] - 0.05), fontsize=7, color='k', alpha=0.7)
        axes.annotate(str(round(psych_curve.ydata[-1], 2)), xy=(psych_curve.xdata[-1], psych_curve.ydata[-1]),
                        xytext=(psych_curve.xdata[-1] - 0.1, psych_curve.ydata[-1] + 0.05), fontsize=7, color='k', alpha=0.7)

        psych_curve2 = Utils.compute_psych_curve(res.loc[(res.aftercorrect==1)&(res.prob_repeat>0.5),'ev_repeat'].dropna().round(2), res.loc[(res.aftercorrect==1)&(res.prob_repeat>0.5),'rep_resp'].dropna())
        # print('unique ev repeat', res.ev_repeat.unique())
        # print('xdata',psych_curve2.xdata)
        # print('ydata',psych_curve2.ydata)
        # print('error',psych_curve2.fit_error)
        axes.errorbar(psych_curve2.xdata, psych_curve2.ydata,
                        yerr=psych_curve2.fit_error, color='crimson',  marker ='o', markersize=2, elinewidth=1,linestyle='none')# 
        axes.plot(np.linspace(-1, 1, 30), psych_curve2.fit, color='crimson', linewidth=1, label='rep')
        # axes.annotate(str(round(psych_curve2.ydata[0], 2)), xy=(psych_curve2.xdata[0], psych_curve2.ydata[0]),
        #                 xytext=(psych_curve2.xdata[0] - 0.03, psych_curve2.ydata[0] + 0.05), fontsize=7, color='crimson', alpha=0.7)
        # axes.annotate(str(round(psych_curve2.ydata[-1], 2)), xy=(psych_curve2.xdata[-1], psych_curve2.ydata[-1]),
        #                 xytext=(psych_curve2.xdata[-1] - 0.1, psych_curve2.ydata[-1] - 0.09), fontsize=7, color='crimson', alpha=0.7)

        psych_curve2 = Utils.compute_psych_curve(res.loc[(res.aftercorrect==1)&(res.prob_repeat<0.5),'ev_repeat'].dropna().round(2), res.loc[(res.aftercorrect==1)&(res.prob_repeat<0.5),'rep_resp'].dropna())
        axes.errorbar(psych_curve2.xdata, psych_curve2.ydata,
                        yerr=psych_curve2.fit_error, color='blue',  marker ='o', markersize=2, elinewidth=1,linestyle='none')# 
        axes.plot(np.linspace(-1, 1, 30), psych_curve2.fit, color='blue', linewidth=1, label='alt')
        # axes.annotate(str(round(psych_curve2.ydata[0], 2)), xy=(psych_curve2.xdata[0], psych_curve2.ydata[0]),
        #                 xytext=(psych_curve2.xdata[0] - 0.03, psych_curve2.ydata[0] + 0.05), fontsize=7, color='crimson', alpha=0.7)
        # axes.annotate(str(round(psych_curve2.ydata[-1], 2)), xy=(psych_curve2.xdata[-1], psych_curve2.ydata[-1]),
        #                 xytext=(psych_curve2.xdata[-1] - 0.1, psych_curve2.ydata[-1] - 0.09), fontsize=7, color='crimson', alpha=0.7)


        axes.legend(loc="lower right")

        # engagement in time
        ax = plt.subplot2grid((100, 100), (41, 36), rowspan=29, colspan=34)
        sns.scatterplot(toplot[:,0], toplot[:,1]/60, size=toplot[:,2], legend=False, ax=ax,color='k', edgecolor='gray')
        ax.set_xlabel('# trial')
        ax.set_title('engagement')
        ax.set_ylabel('time (min)', **label_kwargs)

        pdf.savefig()
        plt.close()

    return pdfpath


if __name__ == '__main__':
    # import sys
    # # import argparse
    # # parser = argparse.ArgumentParser(description='test to over-ride existing one', epilog='gl next time')
    # usagestr = 'usage:\npython custom_jordi.py csvpath framestamppath pdfpath'
    # # parser.add_argument("-h", "--help", help="show this msg", action="store_true")
    # # arg = parser.parse_args()
    # try:
    #     # goddamn argparse is overriding this
    #     # if sys.argv[1]=='-h' or sys.argv[1]=='--help':
    #     #     print(usagestr).
    #     # else:
    #     assert len(sys.argv)==4, usagestr
    #     report_noenv(*sys.argv[1:]) # is this a good practise?
    # except Exception as e:
    #     print(usagestr)
    #     raise e
    # working on a stand alone call by popular demand
    import sys
    
    from toolsR import UtilsR
    nargs = len(sys.argv)
    assert 1< nargs <4, 'args: LEXX [YYYYMMDD-hhmmss]'
    assert sys.argv[1][:2].lower() == 'le', '1st arg is subject, like here: LEXX(x)'
    homedir = os.path.expanduser('~')

    subjpath = f'{homedir}/projects/experiments/Jordi/setups/{sys.argv[1].lower()}/sessions/'

    if nargs == 2: # make report of most recent session
        targ_session = sorted(os.listdir(subjpath))[-1]
    elif nargs == 3: # make report of target session
        sess_list = [x for x in os.listdir(subjpath) if sys.argv[2] in x]
        assert len(sess_list)!=0, f'did not find any session containing provided pattern ({sys.argv[2]})'
        assert len(sess_list)>1, f'found more than one matching session: {sess_list}'

        targ_session = sess_list[0] # first and only item

    csvpath = f'{subjpath}{targ_session}/{targ_session}.csv'

    # look whether we find corresponding video .npy timestamps
    npypath = f'{homedir}/VIDEO_pybpod/{sys.argv[1].upper()}/{targ_session}.npy'
    if not os.path.exists(npypath):
        npypath=None

    outpdf = f'{homedir}/daily_reports/{sys.argv[1].upper()}/{targ_session}.pdf'

    pdfpath = report_noenv(csvpath, npypath, outpdf
    )

    if homedir=='/home/delarocha7':
        where_to_spam = "#ephys_experiments"
    else:
        where_to_spam = 'jordi'

    if 'SLACK_BOT_TOKEN' in os.environ:
        UtilsR.slack_spam(f'{targ_session} manually-generated report', pdfpath, where_to_spam)