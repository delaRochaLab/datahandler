import argparse
from datahandler.utils import Utils, Timing
# from datahandler import custom_jordi

# ARGUMENT PARSER
description = """
datahandler scans the selected directory recursively and generates data from all the csvs
"""
epilog = """
use 'datahandler' without arguments to open a window and select the options desired or
use one of the arguments above to run directly
"""

parser = argparse.ArgumentParser(description=description, epilog=epilog)

parser.add_argument("-l", "--local",
                    help="to use the local directories",
                    action="store_true")
parser.add_argument("-c", "--cluster",
                    help="to use the cluster directories",
                    action="store_true")
parser.add_argument("-o", "--overwrite",
                    help="to overwrite the reports",
                    action="store_true")
parser.add_argument("-O", "--overwriteall",
                    help="to overwrite the clean data and reports",
                    action="store_true")
parser.add_argument("-g", "--globalfiles",
                    help="to create global files (csv or matlab)",
                    action="store_true")
parser.add_argument("-f", "--filelog",
                    help="to save the log output to a file",
                    action="store_true")
parser.add_argument('file',
                    type=str,
                    nargs='*',
                    default='',
                    help="to run datahandler for one specific file only")

arg = parser.parse_args()
