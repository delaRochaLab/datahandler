import pandas as pd
import numpy as np
from scipy import stats
from scipy.optimize import minimize
from collections import namedtuple
from timeit import default_timer as timer


class Utils:

    # DEBUG PRINT
    @staticmethod
    def dprint(name, elem):
        """
        To print values in one line.
        """
        if isinstance(elem, list):
            if len(elem) >= 1:
                if isinstance(elem[0], list):
                    print(name, type(elem), len(elem), 'of', len(elem[0]), type(elem[0]))
                elif isinstance((elem[0]), np.ndarray):
                    print(name, type(elem), len(elem), 'of', elem[0].size, type(elem[0]))
                elif len(elem) > 3:
                    print(name, type(elem), len(elem), elem[0], elem[1], elem[2], '...')
                elif len(elem) == 3:
                    print(name, type(elem), len(elem), elem[0], elem[1], elem[2])
                elif len(elem) == 2:
                    print(name, type(elem), len(elem), elem[0], elem[1])
                elif len(elem) == 1:
                    print(name, type(elem), len(elem), elem[0])
            else:
                print(name, type(elem), len(elem))
        elif isinstance(elem, np.ndarray):
            if elem.size >= 1:
                if isinstance(elem[0], list):
                    print(name, type(elem), elem.shape[0], 'of', len(elem[0]), type(elem[0]))
                elif isinstance((elem[0]), np.ndarray):
                    print(name, type(elem), elem.shape[0], 'of', elem[0].size, type(elem[0]))
                elif elem.size > 3:
                    print(name, type(elem), elem.size, elem[0], elem[1], elem[2], '...')
                elif elem.size == 3:
                    print(name, type(elem), elem.size, elem[0], elem[1], elem[2])
                elif elem.size == 2:
                    print(name, type(elem), elem.size, elem[0], elem[1])
                elif elem.size == 1:
                    print(name, type(elem), elem.size, elem[0])
            else:
                print(name, type(elem), elem.size)
        elif isinstance(elem, pd.Series):
            if elem.size > 3:
                print(name, type(elem), elem.size, elem.iloc[0], elem.iloc[1], elem.iloc[2], '...')
            elif elem.size == 3:
                print(name, type(elem), elem.size, elem.iloc[0], elem.iloc[1], elem.iloc[2])
            elif elem.size == 2:
                print(name, type(elem), elem.size, elem.iloc[0], elem.iloc[1])
            elif elem.size == 1:
                print(name, type(elem), elem.size, elem.iloc[0])
            else:
                print(name, type(elem), elem.size)
        elif isinstance(elem, pd.Index):
            if elem.size > 3:
                print(name, type(elem), elem.size, elem[0], elem[1], elem[2], '...')
            elif elem.size == 3:
                print(name, type(elem), elem.size, elem[0], elem[1], elem[2])
            elif elem.size == 2:
                print(name, type(elem), elem.size, elem[0], elem[1])
            elif elem.size == 1:
                print(name, type(elem), elem.size, elem[0])
            else:
                print(name, type(elem), elem.size)
        else:
            print(name, type(elem), elem)

    # CONVERT STRING COLUMNS OF DF TO LISTS
    @staticmethod
    def convert_strings_to_lists(df, columns):
        """
        If the csv contains a column that is ',' separated, that column is read as a string.
        We want to convert that string to a list of values. We try to make the list float or string.
        """

        def tolist(stringvalue):
            if isinstance(stringvalue, str):
                try:
                    stringvalue = stringvalue.split(sep=',')
                    try:
                        val = np.array(stringvalue, dtype=float)
                    except:
                        val = np.array(stringvalue)
                except:  # is empty string we need [np.nan]
                    val = np.array([np.nan])
            else:
                val = np.array([stringvalue])
            return val

        for column in columns:
            df[column] = df[column].apply(tolist)
        return df

    # UNNESTING LISTS IN COLUMNS DATAFRAMES
    @staticmethod
    def unnesting(df, explode):
        """
        Unnest columns that contain list creating a new row for each element in the list.
        The number of elements must be the same for all the columns, row by row.
        """
        length = df[explode[0]].str.len()
        idx = df.index.repeat(length)
        df1 = pd.concat([
            pd.DataFrame({x: np.concatenate(df[x].values)}) for x in explode], axis=1)
        df1.index = idx
        finaldf = df1.join(df.drop(explode, 1), how='left')
        finaldf.reset_index(drop=True, inplace=True)

        length2 = [list(range(l)) for l in length]
        length2 = [item + 1 for sublist in length2 for item in sublist]
        name = explode[0] + '_index'
        finaldf[name] = length2

        for column in finaldf.columns:
            try:
                if set(finaldf[column]) <= {'True', 'False', 'nan'}:
                    replacing = {'True': True, 'False': False, 'nan': np.nan}
                    finaldf[column] = finaldf[column].map(replacing)
            except:
                pass
        return finaldf

    # COMPUTE WINDOW AVERAGE
    @staticmethod
    def compute_window(data, runningwindow):
        """
        Computes a rolling average with a length of runningwindow samples.
        """
        performance = []
        for i in range(len(data)):
            if i < runningwindow:
                performance.append(round(np.mean(data[0:i + 1]), 2))
            else:
                performance.append(round(np.mean(data[i - runningwindow:i]), 2))
        return performance

    # COMPUTE PSYCHOMETRIC CURVE
    @staticmethod
    def compute_psych_curve(x, y):
        """
        Computes a psychometric function.
        """

        def sigmoid_mme(fit_params: tuple):
            k, x0, b, p = fit_params

            # Function to fit:
            y_pred = b + (1 - b - p) / (1 + np.exp(-k * (xdata - x0)))

            # Calculate negative log likelihood:
            ll = - np.sum(stats.norm.logpdf(ydata, loc=y_pred))

            return ll

        coherence_dataframe = pd.DataFrame({'r_resp': y, 'evidence': x})

        info = coherence_dataframe.groupby(['evidence'])['r_resp'].mean()
        ydata = [np.around(elem, 3) for elem in info.values]
        xdata = info.index.values
        fit_error = [np.around(elem, 3) for elem in coherence_dataframe.groupby(['evidence'])['r_resp'].sem().values]

        initial_guess = np.array([1, 1, 0, 0])

        # Run the minimizer:
        ll = minimize(sigmoid_mme, initial_guess)

        # Fit parameters:
        k, x0, b, p = [np.around(param, 2) for param in ll['x']]

        # Compute the fit with 30 points:
        fit = b + (1 - b - p) / (1 + np.exp(-k * (np.linspace(-1, 1, 30) - x0)))
        fit = [np.around(elem, 3) for elem in fit]

        psych_curve = namedtuple('psych_curve',
                                 ['xdata',
                                  'ydata',
                                  'fit',
                                  'params',
                                  'fit_error'])

        if len(ydata) == 0:
            return psych_curve(xdata = [np.nan],
                               ydata = [np.nan],
                               fit = [np.nan] * 30,
                               params = [np.nan] * 4,
                               fit_error = [np.nan])
        else:
            return psych_curve(xdata=xdata,
                               ydata=ydata,
                               fit=fit,
                               params=[k, x0, b, p],
                               fit_error=fit_error)

    @staticmethod
    def addnew():
        print('hola')


# COMPUTE TIME
class Timing:
    """
    Print the time since the previous time() call. First time it prints zero.
    """

    def __init__(self):
        self.times = [0.0, 0.0]

    def time(self):
        t = timer()
        self.times[1] = t - self.times[0]
        self.times[0] = t

        return ' / time elapsed: ' + str(self.times[1])
