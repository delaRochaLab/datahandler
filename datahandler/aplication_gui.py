from tkinter import *
import tkinter.ttk as ttk
from tasks.selected_task import taskNames


class ApplicationGui(Frame):

    def __init__(self, settings, settings_file):

        self.window = Tk()
        self.window.title('DATAHANDLER')
        # self.window.attributes("-fullscreen", True)

        self.stop = True
        Frame.__init__(self, self.window)

        self._settings = settings
        self._settings_file = settings_file

        self._choice_default_task = StringVar()
        self._choice_overwrite = StringVar()

        self._var_local = BooleanVar()
        self._var_cluster = BooleanVar()

        self._var_global = BooleanVar()
        self._var_no_global = BooleanVar()

        self._var_log = BooleanVar()
        self._var_no_log = BooleanVar()

        self._blueStyle = ttk.Style()
        self._blueStyle.configure("blue.TButton", foreground="blue")
        self._redStyle = ttk.Style()
        self._redStyle.configure("red.TButton", foreground="red")

        width0 = 25
        width1 = 45
        width2 = 135
        width3 = 5
        row = 0

        Label(self, text='', anchor='e', width=width0).grid(row=row, column=0)
        row += 1

        self._local0 = Label(self, text='USE LOCAL DIRECTORIES', anchor='e', width=width0)
        self._local0.grid(row=row, column=0)
        Checkbutton(self, text='', width=width3, variable=self._var_local, anchor='w',
                    command=self._local).grid(row=row, column=1, sticky=W)
        row += 1

        self._raw = Label(self, text='raw data (csv):', anchor='e', width=width0)
        self._raw.grid(row=row, column=0)
        self._entry_raw = Entry(self, width=width2)
        self._entry_raw.insert(0, settings['LOCAL']['RAW_DIRECTORY'])
        self._entry_raw.grid(row=row, column=1, columnspan=3, sticky=W)
        row += 1

        self._clean = Label(self, text='clean data (csv):', anchor='e', width=width0)
        self._clean.grid(row=row, column=0)
        self._entry_clean = Entry(self, width=width2)
        self._entry_clean.insert(0, settings['LOCAL']['CLEAN_DIRECTORY'])
        self._entry_clean.grid(row=row, column=1, columnspan=3, sticky=W)
        row += 1

        self._report = Label(self, text='reports (pdf):', anchor='e', width=width0)
        self._report.grid(row=row, column=0)
        self._entry_report = Entry(self, width=width2)
        self._entry_report.insert(0, settings['LOCAL']['REPORT_DIRECTORY'])
        self._entry_report.grid(row=row, column=1, columnspan=3, sticky=W)
        row += 1

        Label(self, text='', anchor='e', width=width0).grid(row=row, column=0)
        row += 1

        self._cluster0 = Label(self, text='USE CLUSTER DIRECTORIES', anchor='e', width=width0)
        self._cluster0.grid(row=row, column=0)
        Checkbutton(self, text='', width=width3, variable=self._var_cluster, anchor='w',
                    command=self._cluster).grid(row=row, column=1, sticky=W)
        row += 1

        self._clean_cluster = Label(self, text='clean data (csv):', anchor='e', width=width0)
        self._clean_cluster.grid(row=row, column=0)
        self._entry_clean_cluster = Entry(self, width=width2)
        self._entry_clean_cluster.insert(0, settings['CLUSTER']['CLEAN_DIRECTORY'])
        self._entry_clean_cluster.grid(row=row, column=1, columnspan=3, sticky=W)
        row += 1

        self._report_cluster = Label(self, text='reports (pdf):', anchor='e', width=width0)
        self._report_cluster.grid(row=row, column=0)
        self._entry_report_cluster = Entry(self, width=width2)
        self._entry_report_cluster.insert(0, settings['CLUSTER']['REPORT_DIRECTORY'])
        self._entry_report_cluster.grid(row=row, column=1, columnspan=3, sticky=W)
        row += 1

        self._user_cluster = Label(self, text='cluster user:', anchor='e', width=width0)
        self._user_cluster.grid(row=row, column=0)
        self._entry_user_cluster = Entry(self, width=width2)
        self._entry_user_cluster.insert(0, settings['CLUSTER']['USER'])
        self._entry_user_cluster.grid(row=row, column=1, columnspan=3, sticky=W)
        row += 1

        self._password = Label(self, text='cluster password:', anchor='e', width=width0)
        self._password.grid(row=row, column=0)
        self._entry_password = Entry(self, width=width2)
        self._entry_password.insert(0, settings['CLUSTER']['PASSWORD'])
        self._entry_password.grid(row=row, column=1, columnspan=3, sticky=W)
        row += 1

        ttk.Separator(self, orient=HORIZONTAL).grid(row=row, column=0, columnspan=4, sticky='ew')
        row += 1
        Label(self, text='', anchor='e', width=width0).grid(row=row, column=0)
        row += 1

        self._default_task = Label(self, text='DEFAULT TASK:', anchor='e', width=width0)
        self._default_task.grid(row=row, column=0)

        self._menu_default_task = OptionMenu(self, self._choice_default_task, *taskNames,
                                                   command=self._task_choice).grid(row=row, column=1,
                                                                                   columnspan=2, sticky=W)

        default_task_text = "if the task name is not identified, we will try to use the default task"
        self._default_task2 = Label(self, text=default_task_text, anchor='w', width=width2)
        self._default_task2.grid(row=row, column=3)

        row += 1

        ttk.Separator(self, orient=HORIZONTAL).grid(row=row, column=0, columnspan=4, sticky='ew')
        row += 1
        Label(self, text='', anchor='e', width=width0).grid(row=row, column=0)
        row += 1

        self._over_label = Label(self, text='OVERWRITE:', anchor='e', width=width0)
        self._over_label.grid(row=row, column=0)

        self._overwrite_options = ['no', 'reports', 'reports and clean data']

        self._menu_overwrite = OptionMenu(self, self._choice_overwrite, *self._overwrite_options,
                                          command=self._overwrite_choice).grid(row=row, column=1,
                                                                               columnspan=2, sticky=W)

        self._over_label2 = Label(self, text='', anchor='w', width=width2)
        self._over_label2.grid(row=row, column=3)
        row += 1

        ttk.Separator(self, orient=HORIZONTAL).grid(row=row, column=0, columnspan=4, sticky='ew')
        row += 1
        Label(self, text='', anchor='e', width=width0).grid(row=row, column=0)
        row += 1

        self._global_label = Label(self, text='CREATE GLOBAL FILES:', anchor='e', width=width0)
        self._global_label.grid(row=row, column=0)
        Checkbutton(self, text='yes', width=width3, variable=self._var_global, anchor='w',
                    command=self._global_func).grid(row=row, column=1, sticky=W)
        Checkbutton(self, text='no', width=width3, variable=self._var_no_global, anchor='w',
                    command=self._no_global_func).grid(row=row, column=2, sticky=W)
        mytext = 'create global csv or matlab files and intersession_for_all from a selection of animals and dates'
        self._global_label2 = Label(self, text=mytext, anchor='w', width=width2)
        self._global_label2.grid(row=row, column=3)
        row += 1

        self._global = Label(self, text='global data directory (local):', anchor='e', width=width0)
        self._global.grid(row=row, column=0)
        self._entry_global = Entry(self, width=width2)
        self._entry_global.insert(0, settings['LOCAL']['GLOBAL_DIRECTORY'])
        self._entry_global.grid(row=row, column=1, columnspan=3, sticky=W)
        row += 1

        self._subject_label = Label(self, text='subject names:', anchor='e', width=width0)
        self._subject_label.grid(row=row, column=0)

        self._subject_label2 = Label(self,
                                     text="should be either 'all' or names separated by commas, e.g. LE30,LE26,LE36",
                                     anchor='w', width=width2)
        self._subject_label2.grid(row=row, column=3)

        self._entry_subject_name = Entry(self, width=width1)
        self._entry_subject_name.insert(0, settings['SELECTION']['SUBJECT_NAMES'])
        self._entry_subject_name.grid(row=row, column=1, columnspan=2, sticky=W)
        row += 1

        self._task_label = Label(self, text='task names:', anchor='e', width=width0)
        self._task_label.grid(row=row, column=0)

        self._task_label2 = Label(self,
                                     text="should be either 'all' or tasks separated by commas",
                                     anchor='w', width=width2)
        self._task_label2.grid(row=row, column=3)

        self._entry_task_name = Entry(self, width=width1)
        try:
            self._entry_task_name.insert(0, settings['SELECTION']['TASK_NAMES'])
        except:
            self._entry_task_name.insert(0, 'all')

        self._entry_task_name.grid(row=row, column=1, columnspan=2, sticky=W)
        row += 1

        self._date_label = Label(self, text='date range:', anchor='e', width=width0)
        self._date_label.grid(row=row, column=0)

        date_text = "should be either 'all' or 2 dates in a YYYYMMDD-YYYYMMDD format, or start-20180810 or 20180804-end"
        self._date_label2 = Label(self, text=date_text, anchor='w', width=width2)
        self._date_label2.grid(row=row, column=3)

        self._entry_date = Entry(self, width=width1)
        self._entry_date.insert(0, settings['SELECTION']['DATE_RANGE'])
        self._entry_date.grid(row=row, column=1, columnspan=2, sticky=W)
        row += 1

        self._exclude_label = Label(self, text='exclude:', anchor='e', width=width0)
        self._exclude_label.grid(row=row, column=0)

        exclude_text = "set up a date range to exclude it, put 'null' otherwise, separate multiple ranges with a comma"
        self._exclude_label2 = Label(self, text=exclude_text, anchor='w', width=width2)
        self._exclude_label2.grid(row=row, column=3)

        self._entry_exclude = Entry(self, width=width1)
        self._entry_exclude.insert(0, settings['SELECTION']['EXCLUDE'])
        self._entry_exclude.grid(row=row, column=1, columnspan=2, sticky=W)
        row += 1

        self._filter_label = Label(self, text='filter out:', anchor='e', width=width0)
        self._filter_label.grid(row=row, column=0)

        filter_text = "filter out sessions by postop or task variant strings, put 'null' if there's no filter to apply"
        self._filter_label2 = Label(self, text=filter_text, anchor='w', width=width2)
        self._filter_label2.grid(row=row, column=3)

        self._entry_filter = Entry(self, width=width1)
        self._entry_filter.insert(0, settings['SELECTION']['FILTERING'])
        self._entry_filter.grid(row=row, column=1, columnspan=2, sticky=W)
        row += 1

        ttk.Separator(self, orient=HORIZONTAL).grid(row=row, column=0, columnspan=4, sticky='ew')
        row += 1
        Label(self, text='', anchor='e', width=width0).grid(row=row, column=0)
        row += 1

        self._log_label = Label(self, text='LOG INFO:', anchor='e', width=width0)
        self._log_label.grid(row=row, column=0)
        Checkbutton(self, text='file', width=width3, variable=self._var_log, anchor='w',
                    command=self._log).grid(row=row, column=1, sticky=W)
        Checkbutton(self, text='terminal', width=10, variable=self._var_no_log, anchor='w',
                    command=self._no_log).grid(row=row, column=2, sticky=W)
        row += 1

        ttk.Separator(self, orient=HORIZONTAL).grid(row=row, column=0, columnspan=4, sticky='ew')
        row += 1
        Label(self, text='', anchor='e', width=width0).grid(row=row, column=0)
        row += 1

        Label(self, text='TERMINAL COMMANDS:', anchor='e', width=width0).grid(row=row, column=0)
        row += 1
        Label(self, text='to open this window:', anchor='e', width=width0).grid(row=row, column=0)
        Label(self, text='datahandler', anchor='w', width=width1,
              fg='blue').grid(row=row, column=1, columnspan=2, sticky='w')
        row += 1
        Label(self, text='to show help:', anchor='e', width=width0).grid(row=row, column=0)
        Label(self, text='datahandler --help          datahandler -h', anchor='w', width=width1,
              fg='blue').grid(row=row, column=1, columnspan=2, sticky='w')
        row += 1
        Label(self, text='to run selected options directly:',
              anchor='e', width=width0).grid(row=row, column=0)
        self._terminal = Label(self, text='datahandler', anchor='w', width=width2, fg='blue')
        self._terminal.grid(row=row, column=1, columnspan=3, sticky='w')
        row += 1

        ttk.Separator(self, orient=HORIZONTAL).grid(row=row, column=0, columnspan=4, sticky='ew')
        row += 1
        Label(self, text='', anchor='e', width=width0).grid(row=row, column=0)
        row += 1

        self._cancel = ttk.Button(self, text='CANCEL', command=self.cancel_action,
                                  style='red.TButton', width=20)
        self._cancel.grid(row=row, column=0, columnspan=2, sticky='e')
        self._save_and_go = ttk.Button(self, text='SAVE SETTINGS AND PROCEED', command=self.save_and_go_action,
                                       style='blue.TButton', width=35)
        self._save_and_go.grid(row=row, column=2, sticky='w')
        self._save = ttk.Button(self, text='SAVE SETTINGS AND EXIT', command=self.save_action, style='blue.TButton')
        self._save.grid(row=row, column=3, sticky='w')
        row += 1

        Label(self, text='', anchor='e', width=width0).grid(row=row, column=0)
        row += 1
        Label(self, text='', anchor='e', width=width0).grid(row=row, column=0)

        col_count, row_count = self.window.grid_size()
        for row in range(row_count):
            self.window.grid_rowconfigure(row, minsize=20)

        if settings['OPTIONS']['CLUSTER'] == 'True':
            self._cluster()
        else:
            self._local()

        if settings['OPTIONS']['GLOBALFILES'] == 'True':
            self._global_func()
        else:
            self._no_global_func()

        if settings['OPTIONS']['FILELOG'] == 'True':
            self._log()
        else:
            self._no_log()

        self._choice_default_task.set(settings['OPTIONS']['DEFAULT_TASK'])
        self._choice_overwrite.set(settings['OPTIONS']['OVERWRITE'])
        self._overwrite_choice(settings['OPTIONS']['OVERWRITE'])

        self.pack()

    def _task_choice(self, event):
        pass

    def _overwrite_choice(self, event):
        if event == 'no':
            self._over_label2['text'] = 'for each raw CSV, create the clean data and daily report only if they do not exist'
        elif event == 'reports':
            self._over_label2['text'] = 'create the reports again for all files'
        else:
            self._over_label2['text'] = 'create the clean data files and reports again for all files'
        self._update_label()

    def _local(self):
        self._var_local.set(True)
        self._var_cluster.set(False)
        self._local0['fg'] = 'black'
        self._raw['fg'] = 'black'
        self._clean['fg'] = 'black'
        self._report['fg'] ='black'
        self._entry_raw['state'] = 'normal'
        self._entry_clean['state'] = 'normal'
        self._entry_report['state'] = 'normal'
        self._cluster0['fg'] = 'grey'
        self._clean_cluster['fg'] = 'grey'
        self._report_cluster['fg'] = 'grey'
        self._user_cluster['fg'] = 'grey'
        self._password['fg'] = 'grey'
        self._entry_clean_cluster['state'] = 'disabled'
        self._entry_report_cluster['state'] = 'disabled'
        self._entry_user_cluster['state'] = 'disabled'
        self._entry_password['state'] = 'disabled'
        self._update_label()

    def _cluster(self):
        self._var_local.set(False)
        self._var_cluster.set(True)
        self._local0['fg'] = 'grey'
        self._raw['fg'] = 'grey'
        self._clean['fg'] = 'grey'
        self._report['fg'] = 'grey'
        self._entry_raw['state'] = 'disabled'
        self._entry_clean['state'] = 'disabled'
        self._entry_report['state'] = 'disabled'
        self._cluster0['fg'] = 'black'
        self._clean_cluster['fg'] = 'black'
        self._report_cluster['fg'] = 'black'
        self._user_cluster['fg'] = 'black'
        self._password['fg'] = 'black'
        self._entry_clean_cluster['state'] = 'normal'
        self._entry_report_cluster['state'] = 'normal'
        self._entry_user_cluster['state'] = 'normal'
        self._entry_password['state'] = 'normal'
        self._update_label()

    def _no_global_func(self):
        self._var_global.set(False)
        self._var_no_global.set(True)
        self._global_label2['fg'] = 'grey'
        self._global_label['fg'] = 'grey'
        self._global['fg'] = 'grey'
        self._subject_label['fg'] = 'grey'
        self._subject_label2['fg'] = 'grey'
        self._date_label['fg'] = 'grey'
        self._date_label2['fg'] = 'grey'
        self._exclude_label['fg'] = 'grey'
        self._exclude_label2['fg'] = 'grey'
        self._filter_label['fg'] = 'grey'
        self._filter_label2['fg'] = 'grey'
        self._entry_global['state'] = 'disabled'
        self._entry_subject_name['state'] = 'disabled'
        self._entry_date['state'] = 'disabled'
        self._entry_exclude['state'] = 'disabled'
        self._entry_filter['state'] = 'disabled'
        self._update_label()

    def _global_func(self):
        self._var_global.set(True)
        self._var_no_global.set(False)
        self._global_label2['fg'] = 'black'
        self._global_label['fg'] = 'black'
        self._global['fg'] = 'black'
        self._subject_label['fg'] = 'black'
        self._subject_label2['fg'] = 'black'
        self._date_label['fg'] = 'black'
        self._date_label2['fg'] = 'black'
        self._exclude_label['fg'] = 'black'
        self._exclude_label2['fg'] = 'black'
        self._filter_label['fg'] = 'black'
        self._filter_label2['fg'] = 'black'
        self._entry_global['state'] = 'normal'
        self._entry_subject_name['state'] = 'normal'
        self._entry_date['state'] = 'normal'
        self._entry_exclude['state'] = 'normal'
        self._entry_filter['state'] = 'normal'
        self._update_label()

    def _log(self):
        self._var_log.set(True)
        self._var_no_log.set(False)
        self._update_label()

    def _no_log(self):
        self._var_log.set(False)
        self._var_no_log.set(True)
        self._update_label()

    def _update_label(self):
        text = 'datahandler'
        text2 = 'datahandler'
        if self._var_cluster.get():
            text += ' --cluster'
            text2 += ' -c'
        if self._var_local.get():
            text += ' --local'
            text2 += ' -l'
        if self._choice_overwrite.get() == 'reports':
            text += ' --overwrite'
            text2 += ' -o'
        elif self._choice_overwrite.get() == 'reports and clean data':
            text += ' --overwriteall'
            text2 += ' -O'
        if self._var_global.get():
            text += ' --globalfiles'
            text2 += ' -g'
        if self._var_log.get():
            text += ' --filelog'
            text2 += ' -f'
        self._terminal['text'] = text + '          ' + text2

    def save_action(self):
        self._settings['LOCAL']['RAW_DIRECTORY'] = self._entry_raw.get()
        self._settings['LOCAL']['CLEAN_DIRECTORY'] = self._entry_clean.get()
        self._settings['LOCAL']['REPORT_DIRECTORY'] = self._entry_report.get()
        self._settings['LOCAL']['GLOBAL_DIRECTORY'] = self._entry_global.get()

        self._settings['CLUSTER']['CLEAN_DIRECTORY'] = self._entry_clean_cluster.get()
        self._settings['CLUSTER']['REPORT_DIRECTORY'] = self._entry_report_cluster.get()
        self._settings['CLUSTER']['USER'] = self._entry_user_cluster.get()
        self._settings['CLUSTER']['PASSWORD'] = self._entry_password.get()

        self._settings['OPTIONS']['DEFAULT_TASK'] = self._choice_default_task.get()
        self._settings['OPTIONS']['OVERWRITE'] = self._choice_overwrite.get()

        self._settings['SELECTION']['SUBJECT_NAMES'] = self._entry_subject_name.get()
        self._settings['SELECTION']['TASK_NAMES'] = self._entry_task_name.get()
        self._settings['SELECTION']['DATE_RANGE'] = self._entry_date.get()
        self._settings['SELECTION']['EXCLUDE'] = self._entry_exclude.get()
        self._settings['SELECTION']['FILTERING'] = self._entry_filter.get()

        if self._var_cluster.get():
            self._settings['OPTIONS']['CLUSTER'] = 'True'
        else:
            self._settings['OPTIONS']['CLUSTER'] = 'False'

        if self._var_global.get():
            self._settings['OPTIONS']['GLOBALFILES'] = 'True'
        else:
            self._settings['OPTIONS']['GLOBALFILES'] = 'False'

        if self._var_log.get():
            self._settings['OPTIONS']['FILELOG'] = 'True'
        else:
            self._settings['OPTIONS']['FILELOG'] = 'False'

        with open(self._settings_file, 'w') as file:
            self._settings.write(file)

        self._settings.read(self._settings_file)

        self.window.destroy()

    def save_and_go_action(self):
        self.stop = False
        self.save_action()

    def cancel_action(self):
        self.window.destroy()
