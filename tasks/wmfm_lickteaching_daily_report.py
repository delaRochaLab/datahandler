import os
import matplotlib as mpl
import matplotlib.pyplot as plt
import matplotlib.patches as mpatches
import seaborn as sns
import numpy as np
import pandas as pd
from matplotlib.backends.backend_pdf import PdfPages
from matplotlib.lines import Line2D
from datahandler import Utils
import logging
import warnings
if os.environ.get('DISPLAY', '') == '':
    mpl.use('Agg')


# LOGGING AND WARNINGS
logger = logging.getLogger(__name__)
warnings.simplefilter(action='ignore', category=UserWarning)
warnings.simplefilter(action='ignore', category=RuntimeWarning)
warnings.simplefilter(action='ignore', category=FutureWarning)
pd.options.mode.chained_assignment = None  # suppress warning about chaining assignment

# PLOT PARAMETERS
mpl.rcParams['lines.linewidth'] = 0.7
mpl.rcParams['lines.markersize'] = 1
mpl.rcParams['font.size'] = 7
mpl.rcParams['axes.spines.right'] = False
mpl.rcParams['axes.spines.top'] = False
label_kwargs = {'fontsize': 9, 'fontweight': 'roman'}

# PLOT COLORS
first_poke_c = '#C97F74'
second_poke_c = '#96367C'
all_poke_c = '#5B1132'

first_correct_c = 'green'
correct_c = 'lightgreen'
incorrect_c = 'red'
water_c = 'teal'
other_c = 'black'

lines_c = 'gray'
lines2_c = 'silver'



def wmfm_lickteaching_daily_report(trials_path, params_path, daily_report_path):
    """
    Creates a daily report with the performance and psychometric plots, if necessary.
    We use a dataframe with one row per trial: df.
    And a pandas series: params.
    At the end, we need to return params including the new values that we have calculated.
    """

    # READ THE CSVS
    # trials_path contains a csv with one row per trial, we read it as a dataframe
    # we need to set the index of the df using the column containing the number of trial
    # this is necessary if we want to unnest columns
    # when reading the df the columns that contain lists are read as string, transform them to lists

    try:
        df = pd.read_csv(trials_path, sep=';')
    except FileNotFoundError:
        logger.critical("Error: Reading trials CSV file. Exiting...")
        raise

    try:
        params = pd.read_csv(params_path, sep=';')
    except FileNotFoundError:
        logger.critical("Error: Reading params CSV file. Exiting...")
        raise

    # SET GLOBAL INDEX AS TRIAL
    df.index = df.trial

    # transform params df that only contains one row in a Series object
    params = params.iloc[0]

    # SUB-DATAFRAMES
    valid_trial_df = df.dropna(subset=['Reward_start'])
    invalid_trial_df = df.dropna(subset=['Punish_start'])

    # USEFUL VALUES
    total_trials = df.shape[0]
    total_valid_trials = valid_trial_df.shape[0]
    total_miss_trials = invalid_trial_df.shape[0]

    # CLASSIFY BY RW TYPE
    # df['trial_result_filter'] = 0
    # if params.reward_type == 0:
    #     print('water')
    # elif params.reward_type == 1:
    #     print('milkshake')


    # BINNING
    """
    Touchscreen active area: 1440*900 pixels --> 403.2*252 mm
    Stimulus radius: 35pix (9.8mm)
    x_positions: 35-1405 pix --> 9.8-393.4mm
    """
    l_edge = 9.8
    r_edge = 393.4
    bins_err = np.arange(-r_edge, r_edge, 5)
    bins_pos = np.linspace(l_edge, r_edge, 6)

    # PLOTING
    with PdfPages(daily_report_path) as pdf:

        # PAGE 1:
        plt.figure(figsize=(8.3, 11.7))  # A4 vertical

        # HEADER
        axes = plt.subplot2grid((50, 50), (0, 0), rowspan=10, colspan=50)
        s1 = ('Date: ' + str(params.day) + '-' + str(params.time) +
              '  /  Box: ' + str(params.box) +
              '  /  Subject name: ' + str(params.subject_name) +
              '  /  Weight: ' + str(params.weight) + " g" + '\n')

        s2 = ('Total trials: ' + str(total_trials) +
              '  /  Valid trials: ' + str(total_valid_trials) +
              '  /  Missed trials: ' + str(total_miss_trials) + '\n')

        if params.reward_type == 0:
            s3 = ('Water: ' + str(round(params.water_drunk, 3)) + "mL" +  '\n')

        elif params.reward_type == 1:
            s3 = ('Milkshake: ' + str(round(params.milkshake_eat, 3)) + "mL" + '\n')

        axes.text(0.1, 0.9, s1 + s2 + s3,  fontsize=8, transform=plt.gcf().transFigure)

        # Y AXES TO 100 %
        def pcn_y_ax():
            axes.set_ylim(0, 1.10)
            axes.set_yticks(list(np.arange(0, 1.1, 0.1)))
            axes.set_yticklabels(['0', '', '', '', '', '50', '', '', '', '', '100'])

        # LICKPORT LATENCY
        # axes.plot(df.trial, df.Reward_start, c = all_poke_c, marker = 'o', markersize = 2)
        # lickport_latency = valid_trial_df.Reward_start.mean()
        valid_trial_df['lickport_latency2'] = valid_trial_df['Reward_start'] - valid_trial_df['Correct_first_start']
        lickport_latency = valid_trial_df.lickport_latency2.mean()

        axes.plot(valid_trial_df.trial, valid_trial_df.lickport_latency2, c=all_poke_c, marker='o', markersize=2)


        # SAVING AND CLOSING PAGE
        pdf.savefig()
        plt.close()

    # RETURN NEW DATA FOR GENERAL PARAMS
    params['total_trials'] = total_trials
    params['total_valid_trials'] = total_valid_trials
    params['total_miss_trials'] = total_miss_trials
    params['lickport_latency'] = lickport_latency

    return params