import os
import matplotlib as mpl
import matplotlib.pyplot as plt
import matplotlib.patches as mpatches
import seaborn as sns
import numpy as np
import pandas as pd
from matplotlib.backends.backend_pdf import PdfPages
from matplotlib.lines import Line2D
from datahandler import Utils
import logging
import warnings
if os.environ.get('DISPLAY', '') == '':
    mpl.use('Agg')


# LOGGING AND WARNINGS
logger = logging.getLogger(__name__)
warnings.simplefilter(action='ignore', category=UserWarning)
warnings.simplefilter(action='ignore', category=RuntimeWarning)
warnings.simplefilter(action='ignore', category=FutureWarning)
pd.options.mode.chained_assignment = None  # suppress warning about chaining assignment

# PLOT PARAMETERS
mpl.rcParams['lines.linewidth'] = 0.7
mpl.rcParams['lines.markersize'] = 1
mpl.rcParams['font.size'] = 7
mpl.rcParams['axes.spines.right'] = False
mpl.rcParams['axes.spines.top'] = False
label_kwargs = {'fontsize': 9, 'fontweight': 'roman'}

# PLOT COLORS
first_poke_c = '#C97F74'
second_poke_c = '#96367C'
all_poke_c = '#5B1132'

first_correct_c = 'green'
correct_c = 'lightgreen'
incorrect_c = 'red'
water_c = 'teal'
other_c = 'black'

vg_c = '#0f1135'
wmi_c = '#52749b'
wmds_c = '#89abf7'
wmdm_c = '#3cdbb2'
wmdl_c= '#93ecf9'

stim_diam_c = '#ffe607'
vg_th_diam_c = '#ffa600'
wm_th_diam_c = '#fc6900'

lines_c = 'gray'
lines2_c = 'silver'

def wmfm_daily_report(trials_path, params_path, daily_report_path):
    """
    Creates a daily report with the performance and psychometric plots, if necessary.
    We use a dataframe with one row per trial: df.
    And a pandas series: params.
    At the end, we need to return params including the new values that we have calculated.
    """

    # READ THE CSVS
    # trials_path contains a csv with one row per trial, we read it as a dataframe
    # we need to set the index of the df using the column containing the number of trial
    # this is necessary if we want to unnest columns
    # when reading the df the columns that contain lists are read as string, transform them to lists

    try:
        df = pd.read_csv(trials_path, sep=';')
    except FileNotFoundError:
        logger.critical("Error: Reading trials CSV file. Exiting...")
        raise

    try:
        params = pd.read_csv(params_path, sep=';')
    except FileNotFoundError:
        logger.critical("Error: Reading params CSV file. Exiting...")
        raise

    # SET GLOBAL INDEX AS TRIAL
    df.index = df.trial

    # NEW USEFUL COLUMNS
    ### Correct & valid bool columns
    df['correct_first_bool'] = df.trial_result == 1
    df['correct_sec_bool'] = df.trial_result.isin([1, 2])
    df['correct_all_bool'] = df.trial_result > 0

    df['valid_bool'] = df.trial_result != 0

    ### Lickport latencies
    df['stimulus_len'] = df['stimulus_on'].abs() + df['stimulus_off']
    df['lickport_latency'] = df['lick_time'] - df['stop_respwin']

    ### Trial type and delay columns
    df['trial_type2'] = df['trial_type']
    df.loc[(df.delay_type == 'short'), 'trial_type2'] = 'WM_Ds'
    df.loc[(df.delay_type == 'med'), 'trial_type2'] = 'WM_Dm'
    df.loc[(df.delay_type == 'long'), 'trial_type2'] = 'WM_Dl'

    df['delay2'] = 0  # Delay short (0.1, 0.001 are not well detected)
    df.loc[(df.delay == 0), 'delay2'] = -0.1  # VG trials
    df.loc[(df.delay == 0.5), 'delay2'] = 0.5  # Delay med (0.5)
    df.loc[(df.delay == 1), 'delay2'] = 1  # Delay long (1)
    df.loc[(df.delay == 1.5), 'delay2'] = 1.5  # Delay long (1.5)

    df['trial_result_abs'] = df['trial_result'].abs()

    # EXPANDED DATAFRAMES
    # convert strings to lists, be careful df has lists in some cells now!
    df = Utils.convert_strings_to_lists(df, ['all_responses_time', 'incorrects', 'x_responses',
                                             'y_responses', 'x_errors', 'y_errors', 'errors', 'incorrect_attempts',
                                             'wm_bool', 'stimulus_onsets', 'fixation_breaks'])
    # transform params df that only contains one row in a Series object
    params = params.iloc[0]

    # unnest the lists
    resp_df = Utils.unnesting(df,
                              ['all_responses_time', 'incorrects', 'x_responses', 'y_responses', 'x_errors', 'y_errors',
                               'errors', 'incorrect_attempts', 'wm_bool'])
    onsets_df = Utils.unnesting(df, ['stimulus_onsets', 'fixation_breaks'])

    # set the response number index name
    resp_df.rename(columns={'all_responses_time_index': 'resp_index'}, inplace=True)

    # correct correct responses (no duplicates)
    resp_df['compare'] = resp_df.correct.eq(resp_df.correct.shift(-1))
    dup_index = resp_df[resp_df['compare'] == True].index
    resp_df.loc[dup_index, 'correct'] = np.NaN
    resp_df.drop('compare', axis=1)

    # SUB-DATAFRAMES
    valid_trial_df = df[df.trial_result != 0]

    #NEW COLUMNS
    resp_df['correct_bool'] = resp_df.correct > 0
    resp_df['resp_bool'] = resp_df.all_responses_time > 0

    ### Responses shuffling
    resp_df['x_responses_random'] = np.random.permutation(resp_df['x_responses'].values)
    resp_df['x_errors_random'] = resp_df['x_responses_random'] - resp_df['x_positions']

    # USEFUL VALUES
    total_trials = df.shape[0]
    total_valid_trials = valid_trial_df.shape[0]
    total_miss_trials = df[df.trial_result == 0].shape[0]

    total_acc_first = df[df.trial_result == 1].shape[0] / total_valid_trials * 100
    total_acc_sec = df[df.trial_result.isin([1, 2])].shape[0] / total_valid_trials * 100
    total_acc_all = df[df.trial_result > 0].shape[0] / total_valid_trials * 100
    total_perf_first = df[df.trial_result == 1].shape[0] / total_trials * 100
    total_perf_sec = df[df.trial_result.isin([1, 2])].shape[0] / total_trials * 100
    total_perf_all = df[df.trial_result > 0].shape[0] / total_trials * 100

    total_vg_trials = df[df.trial_type == 'VG'].shape[0]
    total_wmi_trials = df[df.trial_type == 'WM_I'].shape[0]
    total_wmd_trials = df[df.trial_type == 'WM_D'].shape[0]

    total_vg_valid_trials = df[(df.trial_result != 0) & (df.trial_type == 'VG')].shape[0]
    total_wmi_valid_trials = df[(df.trial_result != 0) & (df.trial_type == 'WM_I')].shape[0]
    total_wmd_valid_trials = df[(df.trial_result != 0) & (df.trial_type == 'WM_D')].shape[0]

    total_wmds_trials = df[df.delay_type == 'short'].shape[0]
    total_wmdm_trials = df[df.delay_type == 'med'].shape[0]
    total_wmdl_trials = df[df.delay_type == 'long'].shape[0]

    # we use np.divide because the denominators can be zero
    total_pcn_valid_vg = total_vg_valid_trials / total_vg_trials * 100 if total_vg_trials != 0 else 0
    total_pcn_valid_wmi = total_wmi_valid_trials / total_wmi_trials * 100 if total_wmi_trials != 0 else 0
    total_pcn_valid_wmd = total_wmd_valid_trials / total_wmd_trials * 100 if total_wmd_trials != 0 else 0

    total_perf_first_vg = df[(df.trial_result == 1) & (df.trial_type == 'VG')].shape[
                          0] / total_vg_trials * 100 if total_vg_trials != 0 else 0
    total_perf_first_wmi = df[(df.trial_result == 1) & (df.trial_type == 'WM_I')].shape[
                           0] / total_wmi_trials * 100 if total_wmi_trials != 0 else 0
    total_perf_first_wmd = df[(df.trial_result == 1) & (df.trial_type == 'WM_D')].shape[
                           0] / total_wmd_trials * 100 if total_wmd_trials != 0 else 0

    total_acc_first_vg = df[(df.trial_result == 1) & (df.trial_type == 'VG')].shape[
                          0] / total_vg_valid_trials * 100 if total_vg_valid_trials != 0 else 0
    total_acc_first_wmi = df[(df.trial_result == 1) & (df.trial_type == 'WM_I')].shape[
                           0] / total_wmi_valid_trials * 100 if total_wmi_valid_trials != 0 else 0
    total_acc_first_wmd = df[(df.trial_result == 1) & (df.trial_type == 'WM_D')].shape[
                          0] / total_wmd_valid_trials * 100 if total_wmd_valid_trials != 0 else 0

    x_err_mean = resp_df.x_errors.mean()
    x_err_std = resp_df.x_errors.std()
    x_err_median = resp_df.x_errors.median()


    # CHANCE CALCULATION
    screen_size = 1440 * 0.28
    vg_th_diam = params.vg_th_diameter
    wm_th_diam = params.wm_th_diameter
    vg_chance_p = 1 / (screen_size / vg_th_diam)
    wm_chance_p = 1 / (screen_size / wm_th_diam)

    # THRESHOLD LINES
    vg_th_lines = [-vg_th_diam, vg_th_diam]
    wm_th_lines = [-wm_th_diam, wm_th_diam]

    # BINNING
    """
    Touchscreen active area: 1440*900 pixels --> 403.2*252 mm
    Stimulus radius: 35pix (9.8mm)
    x_positions: 35-1405 pix --> 9.8-393.4mm
    """
    l_edge = 9.8
    r_edge = 393.4
    bins_err = np.arange(-r_edge, r_edge, 5)
    bins_pos = np.linspace(l_edge, r_edge, 6)

    # uncomment to create files containing the csvs to explore
    # df.to_csv('/home/balma/daily_reports/dfmodif_A1.csv', sep=';')
    # resp_df.to_csv('/home/balma/daily_reports/responses_A1.csv', sep=';')

    # PLOTING
    with PdfPages(daily_report_path) as pdf:

        # PAGE 1:
        plt.figure(figsize=(8.3, 11.7))  # A4 vertical

        # HEADER
        axes = plt.subplot2grid((50, 50), (0, 0), rowspan=10, colspan=50)

        s1 = ('Date: ' + str(params.day) + '-' + str(params.time) +
              '  /  Box: ' + str(params.box) +
              '  /  Subject name: ' + str(params.subject_name) +
              '  /  Weight: ' + str(params.weight) + " g" +
              '  /  Water: ' + str(round(params.water_drunk, 3)) + "mL" +
              '  /  Milkshake: ' + str(round(params.milkshake_eat, 3)) + "mL" + '\n')
        s2 = ('Stage: ' + str(int(params.stage_number)) +
              '  /  VG Th: ' + str(int(vg_th_diam)) + 'mm' +
              '  /  WM Th: ' + str(int(wm_th_diam)) + 'mm' +
              '  /  Stim time WMI: ' + str(params.stim_time_wmi) + 's' +
              '  /  Delay s: ' + str(params.delay_short) + 's' +
              '  /  Delay m: ' + str(params.delay_med) + 's' +
              '  /  Delay l: ' + str(params.delay_long) + 's' + '\n')
        s3 = ('' + '\n')
        s4 = ('Total trials: ' + str(total_trials) +
              '  /  Perf first poke: ' + str(int(total_perf_first)) + '%' +
              '  /  Perf sec poke: ' + str(int(total_perf_sec)) + '%' +
              '  /  Perf all poke: ' + str(int(total_perf_all)) + '%' + '\n')
        s5 = ('Valid trials: ' + str(total_valid_trials) +
              '  /  Acc first poke: ' + str(int(total_acc_first)) + '%' +
              '  /  Acc sec poke: ' + str(int(total_acc_sec)) + '%' +
              '  /  Acc all poke: ' + str(int(total_acc_all)) + '%' + '\n')
        s6 = ('Missed trials: ' + str(total_miss_trials) +
              '  /  VG Valids: ' +  str(total_vg_valid_trials) + ' (' + str(int(total_pcn_valid_vg)) + '%)' +
              '  /  WM_I valids: ' + str(total_wmi_valid_trials) + ' (' + str(int(total_pcn_valid_wmi)) + '%)' +
              '  /  WM_D valids: ' + str(total_wmd_valid_trials) + ' (' + str(int(total_pcn_valid_wmd)) + '%)' + '\n')
        s7 = ('VG first: Acc ' + str(int(total_acc_first_vg)) + '% Perf ' + str(int(total_perf_first_vg)) + '%' +
              '  /  WM_I Acc first: Acc ' + str(int(total_acc_first_wmi)) + '% Perf ' + str(int(total_perf_first_wmi)) + '%' +
              '  /  WM_D Acc first: Acc ' + str(int(total_acc_first_wmd)) + '% Perf ' + str(int(total_perf_first_wmd)) + '%' + '\n')

        axes.text(0.1, 0.9, s1 + s2 + s3 + s4 + s5 + s6 + s7, fontsize=8, transform=plt.gcf().transFigure)


        # Y AXES TO 100 %
        def pcn_y_ax():
            axes.set_ylim(0, 1.10)
            axes.set_yticks(list(np.arange(0, 1.1, 0.1)))
            axes.set_yticklabels(['0', '', '', '', '', '50', '', '', '', '', '100'])

        def trial_delay_type():
            if total_wmi_trials < 1:
                colors.remove(wmi_c)
                labels.remove('WM_I')
            if total_wmds_trials > 1:
                colors.append(wmds_c)
                labels.append('WM_Ds')
            if total_wmdm_trials > 1:
                colors.append(wmdm_c)
                labels.append('WM_Dm')
            if total_wmdl_trials > 1:
                colors.append(wmdl_c)
                labels.append('WM_Dl')


        # ACCURACY PLOT
        # we use the same axes than header
        valid_trial_df['acc_first_rw'] = Utils.compute_window(valid_trial_df.correct_first_bool, 20)
        valid_trial_df['acc_sec_rw'] = Utils.compute_window(valid_trial_df.correct_sec_bool, 20)
        valid_trial_df['acc_all_rw'] = Utils.compute_window(valid_trial_df.correct_all_bool, 20)

        axes.plot(valid_trial_df.trial, valid_trial_df.acc_all_rw, marker='o', markersize=1, color=all_poke_c)
        axes.plot(valid_trial_df.trial, valid_trial_df.acc_sec_rw, marker='o', markersize=1, color=second_poke_c)
        axes.plot(valid_trial_df.trial, valid_trial_df.acc_first_rw, marker='o', markersize=1, color=first_poke_c)
        axes.hlines(y=[0.5, 1], xmin=0, xmax=total_trials, color=lines_c, linestyle=':')
        axes.fill_between(df.trial, vg_chance_p, 0, facecolor=lines_c, alpha=0.4)
        axes.fill_between(df.trial, wm_chance_p, 0, facecolor=lines2_c, alpha=0.2)

        axes.set_xlim([1, total_trials + 1])
        axes.set_xlabel('Trials', label_kwargs)
        axes.set_ylabel('Accuracy (%)', label_kwargs)
        pcn_y_ax()

        colors = [first_poke_c, second_poke_c, all_poke_c]
        labels = ['First pokes', 'First & sec pokes', 'All pokes']
        lines = [Line2D([0], [0], color=c, marker='o', markersize=6, markerfacecolor=c) for c in colors]
        axes.legend(lines, labels, fontsize=6)

        # RASTER-LIKE PLOT
        axes = plt.subplot2grid((50, 50), (14, 0), rowspan=30, colspan=25)
        x_min = -2
        x_max = 15
        size_bins = 0.3

        correct_first_df = df[df.trial_result == 1]
        onsets_df['stimulus_onsets'] = onsets_df['stimulus_onsets'].fillna(0)
        onsets_df['fix_breaks_len'] = onsets_df['stimulus_onsets'].abs() + onsets_df['fixation_breaks']
        trial_list = list(df.trial)  # we need to create a list because putting directly in barh causes a small error
        onset_list = list(onsets_df.trial)
        stim_on_list = list(df.stimulus_on)
        stim_onset_list = list(onsets_df.stimulus_onsets)
        fix_break_len_list = list(onsets_df.fix_breaks_len)

        axes.barh(trial_list, df.stimulus_len, color=lines2_c, left=stim_on_list, alpha=0.6)  # dark bar signals stimulus lenght
        axes.barh(onset_list, fix_break_len_list, color=lines2_c, left=stim_onset_list, alpha=0.6) # dark bar signals fixation breaks
        axes.barh(trial_list, x_max, color=lines2_c, left=x_min, alpha=0.2)  # light bar signals each trial

        axes.scatter(resp_df.lick_time, resp_df.trial, s=2, c=water_c)
        axes.scatter(resp_df.incorrects, resp_df.trial, s=2, c=incorrect_c)
        axes.scatter(resp_df.correct, resp_df.trial, s=2, c=correct_c)
        axes.scatter(correct_first_df.correct, correct_first_df.trial, s=2, c= first_correct_c)
        axes.vlines(x=0, ymin=0, ymax=total_trials + 1, linewidth=1, color=lines_c)
        axes.vlines(x=params.timeup, ymin=0, ymax=total_trials + 1, color=lines_c, linestyle=':')

        axes.set_xlim(x_min, (x_max+x_min))
        axes.set_ylim(0, total_trials+1)
        axes.set_xticklabels([])
        axes.set_ylabel('Trial number', label_kwargs)

        # HISTOGRAM OF LATENCIES
        axes = plt.subplot2grid((50, 50), (44, 0), rowspan=6, colspan=25)
        bins = np.arange(0, x_max, size_bins)
        axes.hist(df.lick_time, histtype='step', bins=bins, alpha=0.7, color=water_c) #lickport pokes
        axes.hist(resp_df.incorrects.dropna(), histtype='step', bins=bins,
                  alpha=0.7, color=incorrect_c)
        axes.hist(df.correct, histtype='step', bins=bins, alpha=0.7, color=first_correct_c)

        axes.set_xlim(x_min, (x_max+x_min))
        axes.set_xlabel('Latency (sec)', label_kwargs)
        axes.set_ylabel('Number of pokes', label_kwargs)

        #legend
        colors = [first_correct_c, incorrect_c, water_c, lines2_c]
        labels = ['Correct', 'Incorrect', 'Water', 'Stimulus lenght']
        widths = [0, 0, 0, 4]
        markers = ['o', 'o', 'o', ',']
        lines = [Line2D([0], [0], color=colors[i], marker=markers[i], markersize=6,
                        markerfacecolor=colors[i], linewidth=widths[i]) for i in range(len(colors))]
        axes.legend(lines, labels, fontsize=6)

        # LICKPORT LATENCIES
        axes = plt.subplot2grid((50, 50), (14, 29), rowspan=10, colspan=21)

        df['trial_result_filter'] = 0
        df.loc[(df.trial_result < 0), 'trial_result_filter'] = 'I'
        df.loc[(df.trial_result == 0), 'trial_result_filter'] = 'M'
        df.loc[(df.trial_result == 1), 'trial_result_filter'] = 'Cf'
        df.loc[(df.trial_result > 1), 'trial_result_filter'] = 'Co'

        licklat_correct_first_mean = df[df.trial_result_filter == 'Cf'].lickport_latency.mean()
        licklat_correct_mean = df[df.trial_result_filter == 'Co'].lickport_latency.mean()
        licklat_incorrect_mean = df[df.trial_result_filter == 'I'].lickport_latency.mean()
        licklat_miss_mean = df[df.trial_result_filter == 'M'].lickport_latency.mean()

        licklat_correct_first_median = df[df.trial_result_filter == 'Cf'].lickport_latency.median()
        licklat_correct_median = df[df.trial_result_filter == 'Co'].lickport_latency.median()
        licklat_incorrect_median = df[df.trial_result_filter == 'I'].lickport_latency.median()
        licklat_miss_median = df[df.trial_result_filter == 'M'].lickport_latency.median()

        licklat_correct_first_std = df[df.trial_result_filter == 'Cf'].lickport_latency.std()
        licklat_correct_std = df[df.trial_result_filter == 'Co'].lickport_latency.std()
        licklat_incorrect_std = df[df.trial_result_filter == 'I'].lickport_latency.std()
        licklat_miss_std = df[df.trial_result_filter == 'M'].lickport_latency.std()

        colors = [vg_c, wmi_c]
        labels = ['VG', 'WM_I']
        trial_delay_type()

        sns.boxplot(y='lickport_latency', x='trial_result_filter', hue='trial_type2', data=df, order=['Cf', 'Co', 'I', 'M'],
                            width=0.8, linewidth = 0.7, color = 'white', fliersize=0.1)
        sns.stripplot(y='lickport_latency', x='trial_result_filter', hue='trial_type2', data=df, order=['Cf', 'Co', 'I', 'M'],
                      jitter=True, palette= colors, size=2, dodge=True)

        axes.set_ylim(0, 10)
        axes.set_xticklabels(['Correct\nFirst', 'Correct\nOther', 'Incorrect', 'Miss'], rotation=45)
        axes.set_ylabel("Lickport latency (sec)", label_kwargs)
        axes.set_xlabel("", label_kwargs)

        lines = [Line2D([0], [0], color=c, marker='o', markersize=6, markerfacecolor=c) for c in colors]
        axes.legend(lines, labels, fontsize=6, loc='upper left', bbox_to_anchor=(0, 1))

        # REPOKE PER TRIAL
        axes = plt.subplot2grid((50, 50), (27, 29), rowspan=10, colspan=21)

        sns.countplot(x='trial_result_abs', hue='trial_type2', hue_order=labels, data=df, palette=colors)
        axes.set_xlabel("Number of responses", label_kwargs)
        axes.legend().remove()

        # REPOKE PER TRIAL 2
        axes = plt.subplot2grid((50, 50), (40, 29), rowspan=10, colspan=21)

        sns.pointplot(x='trial_type2', y='trial_result_abs', data=valid_trial_df, order=labels, color=all_poke_c, estimator=np.median)
        axes.set_ylabel('nº responses/nºtrials', label_kwargs)
        axes.set_xlabel("Trial type", label_kwargs)
        axes.set_ylim(0, 15)

        # SAVING AND CLOSING PAGE 1
        pdf.savefig()
        plt.close()

        # PAGE 2:
        plt.figure(figsize=(8.3, 11.7))  # A4 vertical

        # X ERROR HISTOGRAM
        axes = plt.subplot2grid((50, 50), (0, 0), rowspan=4, colspan=50)
        axes.hist(resp_df.x_errors, bins=bins_err, histtype='step', color=all_poke_c, density=True)

        axes.set_xlim(-r_edge, r_edge)
        axes.set_xticklabels([])
        yticks = (axes.get_yticks())
        axes.set_yticks(yticks[yticks != 0])  # delete tick y=0 to avoid overlaping with next plot
        axes.set_ylabel('Touches', label_kwargs)

        # X,Y ERROR HISTOGRAM
        axes = plt.subplot2grid((50, 50), (4, 0), rowspan=7, colspan=50)
        if resp_df.y_positions.mean() == 15:
            resp_df['y_errors'] = 0

        plot = axes.hist2d(resp_df.x_errors, resp_df.y_errors, bins=bins_err)

        stim_diam_patch = mpatches.Circle((0, 0), params.stim_diameter/2, facecolor='None', edgecolor=stim_diam_c, lw=1,
                                    zorder=9)
        vg_th_diam_patch = mpatches.Circle((0, 0), vg_th_diam/2, facecolor='None',
                                     edgecolor=vg_th_diam_c, lw=1, zorder=9)
        wm_th_diam_patch = mpatches.Circle((0, 0), wm_th_diam/ 2, facecolor='None',
                                     edgecolor=wm_th_diam_c, lw=1, zorder=9)
        patches = [stim_diam_patch, vg_th_diam_patch, wm_th_diam_patch]
        for patch in patches:
            axes.add_patch(patch)

        axes.set_ylim(-25, 120)
        axes.set_xlim(-r_edge, r_edge)
        axes.set_ylabel('Error y (mm)',label_kwargs)
        axes.set_xlabel('Error x (mm)', label_kwargs)
        colors = [stim_diam_c, vg_th_diam_c, wm_th_diam_c]
        labels = ['Stimulus', 'VG Correct Window', 'WM Correct Window']
        lines = [Line2D([0], [0], color=c) for c in colors]
        axes.legend(lines, labels, fontsize=6)

        # horizontal colorbar
        axes = plt.subplot2grid((50, 50), (11, 42), rowspan=2, colspan=10)
        axes.spines['bottom'].set_visible(False)
        axes.spines['left'].set_visible(False)
        axes.set_xticks([])
        axes.set_yticks([])
        plt.colorbar(plot[3], orientation='horizontal')

        # TRIALS VS DISTANCE
        axes = plt.subplot2grid((50, 50), (14, 0), rowspan=25, colspan=50)

        correct_responses_wm_first_df = resp_df[resp_df.wm_bool & (resp_df.incorrect_attempts == 0) &
                                         (resp_df.trial_result == 1)]
        correct_responses_wm_other_df = resp_df[resp_df.wm_bool & (resp_df.incorrect_attempts == 0) &
                                                     (resp_df.trial_result > 1)]
        correct_responses_wm_none_df = resp_df[resp_df.wm_bool & (resp_df.incorrect_attempts > 0)]

        correct_responses_vg_first_df = resp_df[(resp_df.wm_bool == False) & (resp_df.incorrect_attempts == 0) &
                                               (resp_df.trial_result == 1)]
        correct_responses_vg_other_df = resp_df[(resp_df.wm_bool == False) & (resp_df.incorrect_attempts == 0) &
                                               (resp_df.trial_result > 1)]
        correct_responses_vg_none_df = resp_df[(resp_df.wm_bool == False) & (resp_df.incorrect_attempts > 0)]


        axes.scatter(correct_responses_wm_first_df.x_errors, correct_responses_wm_first_df.trial, s=3,
                     marker='^', c=first_correct_c)
        axes.scatter(correct_responses_vg_first_df.x_errors, correct_responses_vg_first_df.trial, s=3,
                     marker='o', c=first_correct_c)

        axes.scatter(correct_responses_wm_other_df.x_errors, correct_responses_wm_other_df.trial, s=3,
                     marker='^', c=correct_c)
        axes.scatter(correct_responses_vg_other_df.x_errors, correct_responses_vg_other_df.trial, s=3,
                     marker='o', c=correct_c)

        axes.scatter(correct_responses_wm_none_df.x_errors, correct_responses_wm_none_df.trial, s=3,
                     marker='^', c=correct_responses_wm_none_df.incorrect_attempts, cmap='OrRd_r')
        axes.scatter(correct_responses_vg_none_df.x_errors, correct_responses_vg_none_df.trial, s=3,
                     marker='o', c=correct_responses_vg_none_df.incorrect_attempts, cmap='OrRd_r')

        axes.hlines(y=df.trial, xmin=-200, xmax=200, color=lines_c, linewidth=0.2, linestyle=':', zorder=0)
        axes.vlines(x=[-params.vg_th_diameter/2, params.vg_th_diameter/2], ymin=0, ymax=total_trials + 1,
                    color=vg_th_diam_c, zorder=0)
        axes.vlines(x=[-params.wm_th_diameter / 2, params.wm_th_diameter/ 2], ymin=0, ymax=total_trials + 1,
                    color=wm_th_diam_c, zorder=0)
        axes.vlines(x=[-params.stim_diameter / 2, params.stim_diameter/ 2], ymin=0, ymax=total_trials + 1,
                    color=stim_diam_c, zorder=0)

        axes.set_xlim(-200, 200)
        axes.set_xlabel('$Error\ (r_{t}\ -\ x_{t})\ (mm)$', label_kwargs)
        axes.set_ylabel('Trials', label_kwargs)

        colors = [first_correct_c, incorrect_c, stim_diam_c, vg_th_diam_c, vg_th_diam_c, other_c, other_c]
        widths = [0, 0, 1, 1, 1, 0, 0]
        markers = ['s', 's', ',', ',', ',', 'o', '^']
        labels = ['Correct', 'Incorrect', 'Stimulus', 'VG threshold', 'WM threshold', 'Visually guided', "WM guided"]
        lines = [Line2D([0], [0], color=colors[i], marker=markers[i], markersize=6,
                        markerfacecolor=colors[i], linewidth=widths[i]) for i in range(len(colors))]
        axes.legend(lines, labels, fontsize=6)

        # RESPONSES VS STIMULUS POSITION
        axes = plt.subplot2grid((50, 50), (42, 0), rowspan=10, colspan=13)

        axes.hist(resp_df.x_responses, bins=bins_pos, color=all_poke_c, alpha=0.8, histtype='bar', density=True)
        axes.title.set_text('All responses')
        axes.set_xlabel('$Response\ position\ (r_{t})\ (mm)$', label_kwargs)
        axes.set_ylabel('Nº responses', label_kwargs)

        # FIRST RESPONSES VS STIMULUS POSITION
        axes = plt.subplot2grid((50, 50), (42, 16), rowspan=10, colspan=13)
        colors = [vg_c, wmi_c]
        labels = ['VG', 'WM_I']
        colors_idx = 0
        trial_delay_type()

        clean_resp_df = resp_df.dropna(subset=['all_responses_time'])  # delete no valid trials
        first_resp = clean_resp_df.drop_duplicates(subset='trial', keep='first', inplace=False)

        for d, dtype in first_resp.groupby('delay2'):
            sns.distplot(dtype.x_responses, bins=bins_pos,  color=colors[colors_idx], norm_hist=True, kde=False,  ax=axes)
            colors_idx += 1
        axes.title.set_text('First responses')

        axes.set_xlabel('$Response\ position\ (r_{t})\ (mm)$', label_kwargs)
        axes.set_ylabel('', label_kwargs)

        # FIRST RESPONSES ERROR
        axes = plt.subplot2grid((50, 50), (42, 35), rowspan=10, colspan=16)
        bins = np.linspace(-r_edge, r_edge, 16)
        colors_idx = 0

        for d, dtype in first_resp.groupby('delay2'):
            sns.distplot(dtype.x_errors, bins=bins,  color=colors[colors_idx], norm_hist=True, kde=True, ax=axes)
            colors_idx += 1

        axes.vlines(x=vg_th_lines, ymin=0, ymax=0.04, color=vg_th_diam_c, linestyle=':')
        axes.vlines(x=wm_th_lines, ymin=0, ymax=0.04, color=wm_th_diam_c, linestyle=':')


        axes.set_xlabel('$Error\ (r_{t}\ -\ x_{t})\ (mm)$', label_kwargs)
        axes.set_ylabel('Nº responses', label_kwargs)
        axes.set_ylim(0, 0.035)

        colors = [all_poke_c] + colors
        labels = ['All responses'] + labels
        lines = [Line2D([0], [0], color=c, marker='o', markersize=6, markerfacecolor=c) for c in colors]
        axes.legend(lines, labels, fontsize=6, loc='upper right', bbox_to_anchor=(1.3, 1))

        # SAVING AND CLOSING PAGE 2
        pdf.savefig()
        plt.close()

        # PAGE 3:
        plt.figure(figsize=(8.3, 11.7))  # A4 vertical

        # ACCURACY VS STIMULUS POSITION
        axes = plt.subplot2grid((50, 50), (0, 0), rowspan=10, colspan=23)

        resp_df['xt_bins'] = pd.cut(resp_df.x_positions, bins=bins_pos, labels=False, include_lowest=True)
        clean_resp_df = resp_df.dropna(subset=['all_responses_time'])  # delete no valid trials
        first_resp = clean_resp_df.drop_duplicates(subset='trial', keep='first', inplace=False)
        last_resp = clean_resp_df.drop_duplicates(subset='trial', keep='last', inplace=False)

        x_ax_ticks = list(np.arange(-0.75, 4.5, 1.3))
        sns.pointplot(x='xt_bins', y="correct_bool", data=first_resp, s=3, color=first_poke_c)
        sns.pointplot(x='xt_bins', y="correct_bool", data=last_resp, s=3, color=all_poke_c)
        axes.hlines(y=[0.5, 1], xmin=min(x_ax_ticks), xmax=max(x_ax_ticks), color=lines_c, linestyle=':')
        axes.fill_between(x_ax_ticks, vg_chance_p, 0, facecolor=lines_c, alpha=0.4)
        axes.fill_between(x_ax_ticks, wm_chance_p, 0, facecolor=lines2_c, alpha=0.2)

        axes.set_xlabel('$Stimulus\ position\ (r_{t})\ (mm)$', label_kwargs)
        axes.set_xticks(x_ax_ticks)
        axes.set_xticklabels(['0', '100', '200', '300', '400'])
        axes.set_ylabel('Accuracy (%)', label_kwargs)
        pcn_y_ax()

        colors = [all_poke_c, first_poke_c]
        labels = ['All responses', 'First responses']
        lines = [Line2D([0], [0], color=c, marker='o', markersize=6) for c in colors]
        axes.legend(lines, labels, fontsize=6, loc='upper left', bbox_to_anchor=(0, 1.1))

        # ERROR VS STIMULUS POSITION
        axes = plt.subplot2grid((50, 50), (0, 27), rowspan=5, colspan=23)
        sns.pointplot(x='xt_bins', y="x_errors", data=resp_df, s=3, color=all_poke_c)
        axes.hlines(y=0, xmin=min(x_ax_ticks), xmax=max(x_ax_ticks), color=lines_c, linestyle=':')
        axes.set_ylabel('')
        axes.set_xticklabels('')
        axes.set_xticks(x_ax_ticks)

        colors = [vg_c, wmi_c]
        labels = ['VG', 'WM_I']
        trial_delay_type()

        axes = plt.subplot2grid((50, 50), (5, 27), rowspan=6, colspan=22)
        sns.pointplot(x='xt_bins', y="x_errors", hue='trial_type', palette= colors, data=first_resp, s=3, color=all_poke_c)
        axes.hlines(y=0, xmin=min(x_ax_ticks), xmax=max(x_ax_ticks), color=lines_c, linestyle=':')

        axes.set_ylabel('$Error\ (r_{t}\ -\ x_{t})\ (mm)$', label_kwargs)
        axes.set_xlabel('$Stimulus\ position\ (r_{t})\ (mm)$', label_kwargs)
        axes.set_xticks(x_ax_ticks)
        axes.set_xticklabels(['0', '100', '200', '300', '400'])

        # TRIAL TYPE PLOT
        axes = plt.subplot2grid((50, 50), (13, 0), rowspan=4, colspan=50)

        colored_lines = 2
        colors = [vg_c, wmi_c]
        labels = ['VG', 'WM_I']

        trial_delay_type()

        if total_wmi_trials < 1:
            colored_lines -= 1
        if total_wmds_trials > 1:
            colored_lines += 1
        if total_wmdm_trials > 1:
            colored_lines += 1
        if total_wmdl_trials > 1:
            colored_lines += 1

        colored_lines_list = list(range(0, colored_lines))
        axes.plot(df.trial_type2, c=other_c, linewidth=0, marker='o', markersize=1)
        axes.hlines(y=colored_lines_list, xmin=0, xmax=total_trials, color=colors, linestyle='solid')

        axes.set_xlim([-1, total_trials + 1])
        axes.set_ylim(-1, 5)
        axes.set_yticklabels(labels)
        axes.axis('off')

        labels.reverse()
        colors.reverse()
        lines = [Line2D([0], [0], color=c, marker='o', markersize=6, markerfacecolor=c) for c in colors]
        axes.legend(lines, labels, fontsize=6, loc='upper right', bbox_to_anchor=(1.12, 0.9))

        # ACC TRIAL TYPE ACROSS SESSION
        axes = plt.subplot2grid((50, 50), (17, 0), rowspan=5, colspan=50)

        vg_df = df[df.trial_type2 == 'VG']
        vg_valid_df = valid_trial_df[valid_trial_df.trial_type2 == 'VG']
        wmi_df = df[df.trial_type2 == 'WM_I']
        wmi_valid_df = valid_trial_df[valid_trial_df.trial_type2 == 'WM_I']
        wmds_df = df[df.trial_type2 == 'WM_Ds']
        wmds_valid_df = valid_trial_df[valid_trial_df.trial_type2 == 'WM_Ds']
        wmdm_df = df[df.trial_type2 == 'WM_Dm']
        wmdm_valid_df = valid_trial_df[valid_trial_df.trial_type2 == 'WM_Dm']
        wmdl_df = df[df.trial_type2 == 'WM_Dl']
        wmdl_valid_df = valid_trial_df[valid_trial_df.trial_type2 == 'WM_Dl']

        vg_valid_df['acc_vg_rw'] = Utils.compute_window(vg_valid_df.correct_first_bool, 20)
        wmi_valid_df['acc_wmi_rw'] = Utils.compute_window(wmi_valid_df.correct_first_bool, 20)
        wmds_valid_df['acc_wmds_rw'] = Utils.compute_window(wmds_valid_df.correct_first_bool, 20)
        wmdm_valid_df['acc_wmdm_rw'] = Utils.compute_window(wmdm_valid_df.correct_first_bool, 20)
        wmdl_valid_df['acc_wmdl_rw'] = Utils.compute_window(wmdl_valid_df.correct_first_bool, 20)

        vg_df['valids_vg_rw'] = Utils.compute_window(vg_df.valid_bool, 20)
        wmi_df['valids_wmi_rw'] = Utils.compute_window(wmi_df.valid_bool, 20)
        wmds_df['valids_wmds_rw'] = Utils.compute_window(wmds_df.valid_bool, 20)
        wmdm_df['valids_wmdm_rw'] = Utils.compute_window(wmdm_df.valid_bool, 20)
        wmdl_df['valids_wmdl_rw'] = Utils.compute_window(wmdl_df.valid_bool, 20)

        axes.plot(vg_valid_df.trial, vg_valid_df.acc_vg_rw, marker='o', markersize=2, linewidth=1,
                  color=vg_c)
        axes.plot(wmi_valid_df.trial, wmi_valid_df.acc_wmi_rw, marker='o', markersize=2, linewidth=1,
                  color=wmi_c)
        axes.plot(wmds_valid_df.trial, wmds_valid_df.acc_wmds_rw, marker='o', markersize=2, linewidth=1,
                  color=wmds_c)
        axes.plot(wmdm_valid_df.trial, wmdm_valid_df.acc_wmdm_rw, marker='o', markersize=2, linewidth=1,
                  color=wmdm_c)
        axes.plot(wmdl_valid_df.trial, wmdl_valid_df.acc_wmdl_rw, marker='o', markersize=2, linewidth=1,
                  color=wmdl_c)
        axes.hlines(y=[0.5, 1], xmin=0, xmax=total_trials, color=lines_c, linestyle=':')

        axes.set_ylabel('Accuracy (%)', label_kwargs)
        axes.set_xticklabels([])
        pcn_y_ax()

        # VALIDS TRIAL TYPE ACROSS SESSION
        axes = plt.subplot2grid((50, 50), (23, 0), rowspan=5, colspan=50)

        axes.plot(vg_df.trial, vg_df.valids_vg_rw, marker='o', markersize=2, linewidth=1,
                  color=vg_c)
        axes.plot(wmi_df.trial, wmi_df.valids_wmi_rw, marker='o', markersize=2, linewidth=1,
                  color=wmi_c)
        axes.plot(wmds_df.trial, wmds_df.valids_wmds_rw, marker='o', markersize=2, linewidth=1,
                  color=wmds_c)
        axes.plot(wmdm_df.trial, wmdm_df.valids_wmdm_rw, marker='o', markersize=2, linewidth=1,
                  color=wmdm_c)
        axes.plot(wmdl_df.trial, wmdl_df.valids_wmdl_rw, marker='o', markersize=2, linewidth=1,
                  color=wmdl_c)
        axes.hlines(y=[0.5, 1], xmin=0, xmax=total_trials, color=lines_c, linestyle=':')

        axes.set_ylabel('Valids (%)', label_kwargs)
        axes.set_xlabel('Trials', label_kwargs)
        pcn_y_ax()

        # ACC VS TRIAL TYPE
        axes = plt.subplot2grid((50, 50), (30, 0), rowspan=10, colspan=20)

        clean_resp_df = resp_df.dropna(subset=['all_responses_time'])  # delete no valid trials
        first_resp = clean_resp_df.drop_duplicates(subset='trial', keep='first', inplace=False)
        last_resp = clean_resp_df.drop_duplicates(subset='trial', keep='last', inplace=False)
        labels.reverse()

        sns.pointplot(x="trial_type2", y="correct_bool", order= labels, data=first_resp, s=3, color=first_poke_c) #first resp
        sns.pointplot(x="trial_type2", y="correct_bool", order = labels, data=last_resp, s=3, color=all_poke_c) #all resp
        axes.hlines(y=[0.5, 1], xmin=min(colored_lines_list), xmax=max(colored_lines_list), color=lines_c, linestyle=':')
        axes.fill_between(colored_lines_list, vg_chance_p, 0, facecolor=lines_c, alpha=0.4)
        axes.fill_between(colored_lines_list, wm_chance_p, 0, facecolor=lines2_c, alpha=0.2)

        axes.set_xlabel('', label_kwargs)
        axes.set_ylabel('Accuracy (%)', label_kwargs)
        pcn_y_ax()

        # VALIDS VS TRIAL TYPE
        axes = plt.subplot2grid((50, 50), (41, 0), rowspan=10, colspan=20)

        sns.pointplot(x="trial_type2", y="valid_bool", order= labels, data=resp_df, s=3, color=all_poke_c, ci=None)
        axes.hlines(y=[0.5, 1], xmin=min(colored_lines_list), xmax=max(colored_lines_list), color=lines_c, linestyle=':')

        axes.set_xlabel('Trial type', label_kwargs)
        axes.set_ylabel('Valids (%)', label_kwargs)
        pcn_y_ax()

        # RESPONSE TIME LATENCIES DEPENDING ON ALPHA TYPE
        axes = plt.subplot2grid((50, 50), (34, 24), rowspan=17, colspan=26)

        colors = [first_correct_c, incorrect_c]
        labels2 = ['Correct', 'Incorrect']

        resp_df['answer_type'] = 0
        resp_df.loc[(resp_df.correct > 0), 'answer_type'] = 'C'
        resp_df.loc[(resp_df.incorrects > 0), 'answer_type'] = 'I'

        sns.boxplot(x='trial_type2', y='all_responses_time', hue='answer_type', order=labels, hue_order=['C', 'I'], data=resp_df, color='white', fliersize=0.1)
        sns.stripplot(x='trial_type2', y='all_responses_time', hue='answer_type', order= labels,  hue_order=['C', 'I'], data=resp_df,
                      palette = colors, jitter=True, size=2, dodge=True)

        axes.set_ylim(0, 15)
        axes.set_ylabel("Response time (sec)", label_kwargs)
        axes.set_xlabel('')

        lines = [Line2D([0], [0], color=c, marker='o', markersize=6, markerfacecolor=c) for c in colors]
        axes.legend(lines, labels2, fontsize=6, loc='upper right', bbox_to_anchor=(1.1, 1.2))

        # SAVING AND CLOSING PAGE 3
        pdf.savefig()
        plt.close()


    # RETURN NEW DATA FOR GENERAL PARAMS
    params['total_trials'] = total_trials
    params['total_valid_trials'] = total_valid_trials
    params['total_miss_trials'] = total_miss_trials
    params['total_acc_first'] = total_acc_first
    params['total_acc_sec'] = total_acc_sec
    params['total_acc_all'] = total_acc_all
    params['total_perf_first'] = total_perf_first
    params['total_perf_sec'] = total_perf_sec
    params['total_perf_all'] = total_perf_all

    params['licklat_correct_first_mean'] = licklat_correct_first_mean
    params['licklat_correct_mean'] = licklat_correct_mean
    params['licklat_incorrect_mean'] = licklat_incorrect_mean
    params['licklat_miss_mean'] = licklat_miss_mean
    params['licklat_correct_first_median'] = licklat_correct_first_median
    params['licklat_correct_median'] = licklat_correct_median
    params['licklat_incorrect_median'] = licklat_incorrect_median
    params['licklat_miss_median'] = licklat_miss_median
    params['licklat_correct_first_std'] = licklat_correct_first_std
    params['licklat_correct_std'] = licklat_correct_std
    params['licklat_incorrect_std'] = licklat_incorrect_std
    params['licklat_miss_std'] = licklat_miss_std

    params['total_acc_first_vg'] = total_acc_first_vg
    params['total_acc_first_wmi'] = total_acc_first_wmi
    params['total_acc_first_wmd'] = total_acc_first_wmd
    params['total_pcn_valid_vg'] = total_pcn_valid_vg
    params['total_pcn_valid_wmi'] = total_pcn_valid_wmi
    params['total_pcn_valid_wmd'] = total_pcn_valid_wmd

    params['x_err_mean'] = x_err_mean
    params['x_err_std'] = x_err_std
    params['x_err_median'] = x_err_median
    return params

   # RETURN NEW DATA FOR GENERAL TRIALS
    # trials[''] =
    # return trials