import numpy as np
import pandas as pd
import logging
from numpy import linalg as ln
import warnings
from collections import defaultdict

# LOGGING AND WARNINGS
logger = logging.getLogger(__name__)
warnings.simplefilter(action='ignore', category=UserWarning)
warnings.simplefilter(action='ignore', category=RuntimeWarning)
warnings.simplefilter(action='ignore', category=FutureWarning)

def parsed_events(df, trials):
    """
    Extracts information about the session's state timestamps. Works with any session
    type. State_list contains a list of all the unique states; state_timestamps contains the
    timestamps for each state, both _start and _end.
    """

    # --------------- Computation of the states list ----------#

    # NUMBER OF TRIALS
    trial_start = df.query("TYPE=='TRIAL' and MSG=='New trial'").index[1]
    trial_end = df.query("TYPE=='END-TRIAL'").index[0]
    trial_band_states = df.query("TYPE=='STATE' and index > @trial_end and index < @trial_start")
    state_list = set(trial_band_states['MSG'].values)

    states = defaultdict(list)

    new_trial_indexes = df.query("TYPE=='TRIAL' and MSG=='New trial'").index[:trials]
    for t in range(trials):
        if t != trials - 1:
            trial_band = df[new_trial_indexes[t]:new_trial_indexes[t + 1]]
        else:  # last trial
            trial_band = df[new_trial_indexes[t]:]
        trial_band_states = trial_band.query("TYPE=='STATE'")
        # -------------------------- state timestaps ----------------------------------- #
        for state in state_list:
            start_times = trial_band_states.query("MSG==@state")['BPOD-INITIAL-TIME'].values.tolist()
            end_times = trial_band_states.query("MSG==@state")['BPOD-FINAL-TIME'].values.tolist()
            states[str(state) + '_start'].append(start_times)
            states[str(state) + '_end'].append(end_times)

    statelist = {'state_list': list(state_list)}

    return [statelist, states]


# MAIN PARSE FUNCTION
def wmfm_lickteaching_parse(file):
    """
    File is an object containing the raw csv we want to read and some properties that have been calculated in the main
    routine:

    file.path = the path of the file
    file.session_name = the name of the session extracted from the filename
    file.task = the name of the task extracted from the filename
    file.subject_name = the name of the subject extracted from the filename
    file.date = the date extracted from the filename
    file.day = the day extracted from the date
    file.time = the time extracted from the date

    First we read the csv into a df and the we create 2 dictionaries.
    Params dictionary has one value (or one list or one numpy array) for each key.
    Trials dictionary contains n values (or n lists or n numpy arrays) for each key, where n is the number of trials.
    From this dictionaries, 2 csv files containing one row and n rows respectively, will be automatically created in
    the main routine.
    """

    # READ THE CSV INTO A DATAFRAME
    try:
        df = pd.read_csv(file.path, skiprows=6, sep=';')
    except FileNotFoundError:
        logger.critical("Error: Reading CSV file. Exiting...")
        raise

    # VALUES WE HAVE ALREADY EXTRACTED FROM THE FILENAME
    session_name = file.session_name
    task = file.task
    subject_name = file.subject_name
    date = file.date
    day = file.day
    time = file.time

    # METADATA AND VARIABLES
    try:
        box = df[df.MSG == 'VAR_BOX']['+INFO'].values[0]
    except IndexError:
        box = "Unknown box"
        logger.warning("Box name not found.")

    try:
        reward_type = df[df.MSG == 'VAR_MILK_RW']['+INFO'].values[0]
    except IndexError:
        reward_type = np.nan
        logger.warning('Reward type not found')

    weight = df[df.MSG == 'VAR_WEIGHT']['+INFO'].astype(float).values
    weight = weight[0] if weight.size else np.nan

    water_drunk = df[df.MSG =='WATER_DRUNK']['+INFO'].astype(float).values
    water_drunk = water_drunk[-1] * 0.001 if water_drunk.size else 0  # last value of water_drunk converted to mL

    milkshake_eat = df[df.MSG == 'MILKSHAKE_EAT']['+INFO'].astype(float).values
    milkshake_eat = milkshake_eat[-1] * 0.001 if milkshake_eat.size else 0

    logger.info(str(subject_name) + str(day) + '-' + str(time))
    logger.info("Metadata loaded.")

    # CREATES CHANGING VARIABLES
    time_end_list = []

    # NUMBER OF TRIALS
    trial_start = np.flatnonzero(df['MSG'] == 'New trial')
    trial_end = np.flatnonzero(df['TYPE'] == 'END-TRIAL')
    trials = int(len(trial_end))  # only ended trials

    # TOTAL DURATION
    durations = df[df.MSG == 'TRIAL-BPOD-TIME']['BPOD-FINAL-TIME'].values[:trials].astype(float)
    try:
        total_duration = max(durations)
    except:
        total_duration = 0

    # ITERATE FOR EVERY TRIAL
    for elem in range(trials):

        # GET TRIAL
        trial = df[trial_start[elem]:trial_end[elem] + 1]

        #TRIAL END TIME
        time_end_trial = trial.query(
            "TYPE=='END-TRIAL' and MSG=='The trial ended'")['PC-TIME'].values
        if time_end_trial.size:
            time_end_trial = time_end_trial[0]
        else:
            time_end_trial = []
        time_end_list.append(time_end_trial)

    time_end = time_end_list[-1]  # last time end is in fact when session ends
    time_end = time_end[11:19]

    # TRIAL NUMBER
    trial = list(range(1, trials + 1))

    session_params = {'session_name': session_name,
                      'subject_name': subject_name,
                      'weight': weight,
                      'reward_type': reward_type, #if 0 = water; if 1 = MKSH.
                      'task': task,
                      'date': date,
                      'day': day,
                      'time': time,
                      'time_end': time_end,
                      'box': box,
                      'total_trials': trials,
                      'total_duration': total_duration,
                      'water_drunk': water_drunk,
                      'milkshake_eat': milkshake_eat,
                      }

    session_trials = {'trial': trial,
                      }

    statelist, states = parsed_events(df, trials)

    session_params.update(statelist)
    session_trials.update(states)

    return [session_params, session_trials]
