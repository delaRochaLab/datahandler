import numpy as np
import pandas as pd
import logging
import warnings
from datetime import datetime, timedelta


# LOGGING AND WARNINGS
logger = logging.getLogger(__name__)
warnings.simplefilter(action='ignore', category=UserWarning)
warnings.simplefilter(action='ignore', category=RuntimeWarning)
warnings.simplefilter(action='ignore', category=FutureWarning)

# VARIABLES NEEDED
water_reward = 0.024  # ml of water reward


# MAIN PARSE FUNCTION
def cellex4A_opto_parse(file):
    """
    File is an object containing the raw csv we want to read and some properties that have been calculated in the main
    routine:

    file.path = the path of the file
    file.session_name = the name of the session extracted from the filename
    file.task = the name of the task extracted from the filename
    file.subject_name = the name of the subject extracted from the filename
    file.date = the date extracted from the filename
    file.day = the day extracted from the date
    file.time = the time extracted from the date

    First we read the csv into a df and the we create 2 dictionaries.
    Params dictionary has one value (or one list or one numpy array) for each key.
    Trials dictionary contains n values (or n lists or n numpy arrays) for each key, where n is the number of trials.
    From this dictionaries, 2 csv files containing one row and n rows respectively, will be automatically created in
    the main routine.
    """

    # READ THE CSV INTO A DATAFRAME
    try:
        df = pd.read_csv(file.path, skiprows=6, sep=';')
    except FileNotFoundError:
        logger.critical("Error: Reading CSV file. Exiting...")
        raise

    # VALUES WE HAVE ALREADY EXTRACTED FROM THE FILENAME
    session_name = file.session_name
    task = file.task
    subject_name = file.subject_name
    date = file.date
    day = file.day
    time = file.time

    # PARSING THE DF
    try:
        postop = df[df.MSG == 'VAR_POSTOP']['+INFO'].iloc[0]
    except IndexError:
        postop = 'no_injection'
    try:
        variation = df[df.MSG == 'VAR_VARIATION']['+INFO'].iloc[0]
    except IndexError:
        variation = 'no_injection'
    try:
        stage_number = df[df.MSG == 'STAGE_NUMBER']['+INFO'].iloc[0]
    except IndexError:
        stage_number = -1
    try:
        box = df[df.MSG == 'BOARD-NAME']['+INFO'].iloc[0]
    except IndexError:
        box = "Unknown box"

    zt_threshold = df[df.MSG == 'zt_threshold']['+INFO'].iloc[0]
    try:
        proportion_light = df[df.MSG == 'proportion_light']['+INFO'].iloc[0]
    except:
        proportion_light = 0
    try:
        states_with_light = df[df.MSG == 'states_with_light']['+INFO'].iloc[0]
    except:
        states_with_light = 'iti_and_fixation'
    try:
        max_duration_light = float(df[df.MSG == 'max_duration_light']['+INFO'].iloc[0])
    except:
        max_duration_light = 0
    try:
        duration_ramp = float(df[df.MSG == 'duration_ramp']['+INFO'].iloc[0])
    except:
        duration_ramp = 0

    new_trial_indexes = df.query("TYPE=='TRIAL' and MSG=='New trial'").index

    punish_data = []
    invalids = []
    reward_data = []

    length = new_trial_indexes.size - 1

    if length == 0:
        return [None, None]

    # ITERATE FOR EVERY TRIAL
    for i in range(length):

        trial = df[new_trial_indexes[i]:new_trial_indexes[i + 1]]

        punishes_trial = trial.query(
            "TYPE=='STATE' and (MSG=='Punish' or MSG=='PunishLight')")['BPOD-FINAL-TIME'].astype(float).values

        try:
            punish = np.nanmax(punishes_trial)
        except:
            punish = np.nan

        invalids_trial = trial.query(
            "TYPE=='STATE' and MSG=='Invalid'")['BPOD-FINAL-TIME'].astype(float).values
        try:
            invalid = np.nanmax(invalids_trial)
        except:
            invalid = np.nan

        rewards_trial = trial.query(
            "TYPE=='STATE' and (MSG=='Reward' or MSG=='RewardLight')")['BPOD-FINAL-TIME'].astype(float).values
        try:
            reward = np.nanmax(rewards_trial)
        except:
            reward = np.nan

        punish_data.append(punish)
        invalids.append(invalid)
        reward_data.append(reward)

    invalids = [True if np.isnan(x) else False for x in invalids]
    punish_data = [True if np.isnan(x) else False for x in punish_data]

    #  reward_side contains a 1 if the correct answer was the (right)ight side, 0 otherwise:
    try:
        reward_side = df[df.MSG == "REWARD_SIDE"]['+INFO'].iloc[-1][1:-1].split(',')
    except IndexError:  # Compatibility with old files
        try:
            reward_side = df[df.MSG == 'VECTOR_CHOICE']['+INFO'].iloc[-1][1:-1].split(',')
        except IndexError:
            logging.critical("Neither REWARD_SIDE nor VECTOR_CHOICE found. Exiting...")
            raise
    reward_side = np.transpose(np.array([int(x) for x in reward_side[:length]], dtype=np.object))

    coherence = df[df['MSG'] == 'coherence01']['+INFO'].values[:length]
    coherence = coherence.astype(float)

    light_on = df[df['MSG'] == 'light_on_trial']['+INFO'].values[:length]
    light_off = df[df['MSG'] == 'light_off_trial']['+INFO'].values[:length]
    zt = df.loc[(df['MSG'] == 'zt') & (df['TYPE'] =='VAL')]['+INFO'].values[:length]


    #  List of possible, unique coherences:
    coherence_vector = sorted(list(set(coherence)))
    #  Coherences for every trial, as indices of coherence_vector. Remember
    #  that Matlab indices start at 1:
    coherence_number = [coherence_vector.index(elem) + 1 for elem in coherence]

    # includes silence_trial data
    alltrials = df.query("TYPE=='TRIAL' and MSG=='New trial'")['PC-TIME']
    siltrials = df.query("TYPE=='VAL' and MSG=='silence_trial'")['PC-TIME']
    silence = []
    for trial_time in alltrials:
        trial_sum = 0
        for silence_time in siltrials:
            if trial_time[11:19] in silence_time[11:19]:
                trial_sum += 1
        if trial_sum > 0:
            silence.append(1)
        else:
            silence.append(0)

    # last trial could be incomplete
    silence = silence[:length]

    # includes delay data
    delaytrials = df[df['MSG'] == 'delay_trial']['+INFO'].values[:length]
    delay = delaytrials.astype(float)
    if delay.size == 0:
        delay = np.repeat(np.nan, length)

    hithistory = compute_hithistory(punish_data, invalids)
    total_trials = length
    correct_trials = sum([h == 1 for h in hithistory])
    invalid_trials = sum([h == -3 for h in hithistory])
    valid_trials = total_trials - invalid_trials

    water = np.around(correct_trials * water_reward, 3)

    # left_envelopes contains a list of strings:
    left_envelopes = df.query("TYPE=='VAL' and MSG=='left_envelope'")['+INFO'].values[:length]
    right_envelopes = df.query("TYPE=='VAL' and MSG=='right_envelope'")['+INFO'].values[:length]

    left = [np.array(list(map(float, elem[1:-1].split(',')))) for elem in left_envelopes]
    right = [np.array(list(map(float, elem[1:-1].split(',')))) for elem in right_envelopes]

    # DETECTING SOUND FAIL
    # adding a coherence index
    df.loc[(df.TYPE == 'VAL') &
           (df['MSG'] == 'coherence01'), 'coherence_index'] = \
        np.arange(0, len(df.loc[(df.TYPE == 'VAL') &
                                (df['MSG'] == 'coherence01')]))
    df['coherence_index'].fillna(method='ffill', inplace=True)

    # sound_fail = boolean list indicating when play is not present
    sr_play = df.loc[(df.MSG.str.startswith('SoundR: Play.')) &
                     (df.TYPE == 'stdout'), 'coherence_index'].values
    all_trials = np.arange(length)
    sound_fail = np.isin(all_trials, sr_play, assume_unique=True, invert=True)

    # making left and right zero when sound_fail
    left = [np.repeat(0, len(left[i])) if sound_fail[i] else left[i] for i in range(len(left))]
    right = [np.repeat(0, len(right[i])) if sound_fail[i] else right[i] for i in range(len(right))]

    # giving coherence list an absurd value (-100) when sound_fail
    for (i, item) in enumerate(coherence_number):
        if sound_fail[i]:
            coherence_number[i] = -100

    # REAL SOUNDONSET AND SOUNDOFFSET
    # we get the values of onset and offset from messages 60, 61, 62, 63
    test = df[df['MSG'].isin(['60', '61', '62', '63'])]
    test = test[test.TYPE != 'STATE']

    testa = test[test.MSG.isin(['60', '62'])].drop_duplicates(subset=['coherence_index'], keep='first')
    testa.index = testa.coherence_index
    testa = testa.reindex(all_trials)
    sound_onset = testa['BPOD-INITIAL-TIME']

    testb = test[test.MSG.isin(['61', '63'])].drop_duplicates(subset=['coherence_index'], keep='last')
    testb.index = testb.coherence_index
    testb = testb.reindex(all_trials)
    sound_offset = testb['BPOD-INITIAL-TIME']

    # STIMULUS
    envelope_diff = [elem + right[mm] for (mm, elem) in enumerate(left)]
    stimulus_ind = list(range(1, len(left_envelopes) + 1))

    prob_repeat = df[df['MSG'] == 'prob_repeat']['+INFO'].values[:length]
    if not prob_repeat.size:  # The session is old, and prob_repeat is missing
        # So we compute it:
        # prob_repeat = compute_prob_repeat(df, reward_side)[:length] / 100
        prob_repeat = np.array([np.nan] * length)

    # prob_repeat = prob_repeat.astype(float)
    if type(prob_repeat[0]) is not str:
        prob_repeat = prob_repeat.astype(float)

    env_prob_repeat = list(prob_repeat)

    trial = list(range(1, length + 1))

    reward = reward_side * 2 - 1
    hit = np.array(hithistory) * 2 - 1
    response = reward * hit
    response = (response + 1) / 2
    reward_sidelist = reward_side + 1  # 1 for left, 2 for right

    durations = df.query("TYPE=='INFO' and MSG=='TRIAL-BPOD-TIME'")['BPOD-FINAL-TIME'].values[:length].astype(float)
    try:
        total_duration = max(durations)
    except:
        total_duration = 0

    if light_off.size == 0:
        light_off = [False] * length
        for i in range(1, length):
            if abs(float(zt[i-1])) >= float(zt_threshold) and hithistory[i-1] == 1 and light_on[i] == 'False':
                light_off[i] = True

    session_params = {'session_name': session_name,
                      'stage_number': stage_number,
                      'subject_name': subject_name,
                      'task': task,
                      'date': date,
                      'day': day,
                      'time': time,
                      'box': box,
                      'states_with_light': states_with_light,
                      'total_trials': total_trials,
                      'valid_trials': valid_trials,
                      'invalid_trials': invalid_trials,
                      'correct_trials': correct_trials,
                      'variation': variation,
                      'postop': postop,
                      'zt_threshold': zt_threshold,
                      'proportion_light': proportion_light,
                      'water': water,
                      'coherence_vector': coherence_vector,
                      'total_duration': total_duration
                      }

    session_trials = {'trial': trial,
                      'reward_side': reward_sidelist,
                      'hithistory': hithistory,
                      'response': response,
                      'coherence': coherence,
                      'coherence_number': coherence_number,
                      'silence': silence,
                      'delay': delay,
                      'stimulus_ind': stimulus_ind,
                      'env_prob_repeat': env_prob_repeat,
                      'sound_fail': sound_fail,
                      'sound_onset': sound_onset,
                      'sound_offset': sound_offset,
                      'envelope_diff': envelope_diff,
                      'stimulus_right': right,
                      'stimulus_left': left,
                      'light_on': light_on,
                      'light_off': light_off,
                      'zt': zt,
                      }

    parsed = parsed_events(df, length, task, states_with_light, max_duration_light, duration_ramp)
    session_trials.update(parsed)

    return [session_params, session_trials]


def compute_hithistory(punish_data, invalids):
    """
    Computes the HitHistory vector: contains 1 for correct trials, 0 for incorrect trialsvand -3 for invalid trials
    (old BControl convention).
    """
    hithistory = []
    for (ii, elem) in enumerate(punish_data):
        if not invalids[ii]:
            hithistory.append(-3)
        elif elem:
            hithistory.append(1)
        else:
            hithistory.append(0)
    return hithistory


def compute_prob_repeat(df, reward_side):
    """
    This function computes the probability of repeat of the blocks that form the session.
    It is used in those sessions which were p4 but which didn't have yet the data in the CSV
    (sessions before the 10th of august, 2018).
    """

    def getrep(trial_list, blen):
        nblocks = int(round(len(trial_list) / blen, 0))
        transitions = np.arange(nblocks, step=1) * blen
        segmentrepprobs = []
        for item in transitions:
            segment = trial_list[item:item + blen]
            rep = 0
            for jj, elem in enumerate(segment, 1):
                if elem == segment[jj - 1]:
                    rep += 1
            segmentrepprobs = segmentrepprobs + [rep]
        thereps = np.array(segmentrepprobs) * 100 / (blen - 1)
        first, second = np.arange(0, nblocks, step=2), np.arange(1, nblocks, step=2)

        probreporder = [80, 20] if thereps[first].mean() > thereps[second].mean() else [20, 80]

        return np.repeat(probreporder * int(nblocks / 2), blen)

    bsize = int(df.loc[(df.TYPE == 'VAL') &
                       (df.MSG == 'VAR_BLEN'), '+INFO'].values[0])

    return getrep(reward_side, bsize)


def parsed_events(df, length, task, states_with_light, max_duration_light, duration_ramp):
    """
    This method takes the session df and parses the timestamps, converting from
    PyBPOD state names to old BControl names.
    """

    # GET 3 ACTIVE PORTS
    dict_events = dict(zip([x for x in range(68, 85, 2)], [f'Port{x + 1}' for x in range(8)]))
    active_ports_in = sorted([x for x in dict_events.keys()
                              if x in df.loc[df.TYPE == 'EVENT', 'MSG'].astype(int).unique()])
    active_ports_out = sorted([x + 1 for x in active_ports_in])
    active_ports = [dict_events[x] for x in active_ports_in]

    # refactor this shit, trying to check wich port is central port when we only have 2 ports
    if len(active_ports_in) != 3:
        if len(active_ports_in) == 2:
            first_port = str(active_ports_in[0])
            second_port = str(active_ports_in[1])
            indices = df.query("TYPE=='EVENT' and MSG==@first_port").index
            central = df.iloc[indices + 1].query("TYPE=='TRANSITION' and MSG=='StartSound' or MSG=='Fixation'")
            left = df.iloc[indices + 1].query("TYPE=='TRANSITION' and (MSG=='Reward' or MSG=='Punish' or MSG=='RewardLight' or MSG=='PunishLight')")
            if central.shape[0] > 0 and left.shape[0] == 0:
                first_port = 'central'
            elif central.shape[0] == 0 and left.shape[0] > 0:
                first_port = 'left'
            else:
                first_port = ''
            indices = df.query("TYPE=='EVENT' and MSG==@second_port").index
            central = df.iloc[indices + 1].query("TYPE=='TRANSITION' and MSG=='StartSound' or MSG=='Fixation'")
            right = df.iloc[indices + 1].query("TYPE=='TRANSITION' and (MSG=='Reward' or MSG=='Punish' or MSG=='RewardLight' or MSG=='PunishLight')")
            if central.shape[0] > 0 and left.shape[0] == 0:
                second_port = 'central'
            elif central.shape[0] == 0 and right.shape[0] > 0:
                second_port = 'right'
            else:
                second_port = ''
            if first_port == 'left' and second_port == 'central':
                active_ports_in = active_ports_in + [-1]
                active_ports_out = active_ports_out + [-1]
            elif first_port == 'central' and second_port == 'right':
                active_ports_in = [-1] + active_ports_in
                active_ports_out = [-1] + active_ports_out
            elif first_port == 'left' and second_port == 'right':
                active_ports_in = [active_ports_in[0], -1, active_ports_in[1]]
                active_ports_out = [active_ports_out[0], -1, active_ports_out[1]]
            else:
                logger.critical('Number of used ports is not 3')
                raise
        else:
            logger.critical('Number of used ports is not 3')
            raise
    # end of shity code

    active_ports_in = list(map(str, active_ports_in))
    active_ports_out = list(map(str, active_ports_out))

    trial_start = df.query("TYPE=='INFO' and MSG=='TRIAL-BPOD-TIME'")['BPOD-INITIAL-TIME'].values[:length].astype(float)

    trial_end = df.query("TYPE=='INFO' and MSG=='TRIAL-BPOD-TIME'")['BPOD-FINAL-TIME'].values[:length].astype(float)

    intertrial_interval_start = trial_end
    intertrial_interval_end = np.append(trial_start[1:], trial_end[-1])

    new_trial_indexes = df.query("TYPE=='TRIAL' and MSG=='New trial'").index[:length]

    wait_cpoke_start = []
    wait_cpoke_end = []
    pre_stim_delay_start = []
    pre_stim_delay_end = []
    early_withdrawal_pre = []
    l_poke_start = []
    l_poke_end = []
    r_poke_start = []
    r_poke_end = []
    c_poke_start = []
    c_poke_end = []

    wait_apoke_start = []
    wait_apoke_end = []
    play_target_start = []
    play_target_end = []
    reward_start = []
    reward_end = []

    fixation_breaks = []
    duration_light = []

    for jj in range(length):

        if jj != length - 1:
            trial_band = df[new_trial_indexes[jj]:new_trial_indexes[jj + 1]]
        else:
            trial_band = df[new_trial_indexes[jj]:]
        trial_band_events = trial_band.query("TYPE=='EVENT'")
        trial_band_states = trial_band.query("TYPE=='STATE'")

        wait_cpoke_start_trial = trial_band_states.query(
            "MSG=='WaitCPoke' or MSG=='WaitCPokeLight'")['BPOD-INITIAL-TIME'].values.astype(float) + trial_start[jj]

        wait_cpoke_end_trial = trial_band_states.query(
            "MSG=='WaitCPoke' or MSG=='WaitCPokeLight'")['BPOD-FINAL-TIME'].values.astype(float) + trial_start[jj]

        pre_stim_delay_start_trial = trial_band_states.query(
            "MSG=='Fixation' or MSG=='FixationLight'")['BPOD-INITIAL-TIME'].values.astype(float) + trial_start[jj]

        pre_stim_delay_end_trial = trial_band_states.query(
            "MSG=='Fixation' or MSG=='FixationLight'")['BPOD-FINAL-TIME'].values.astype(float) + trial_start[jj]

        try:
            wait_apoke_start_trial = trial_band_states.query(
                "MSG=='WaitResponse' or MSG=='WaitResponseLight'")['BPOD-INITIAL-TIME'].values.astype(float)
            wait_apoke_start_trial = np.nanmax(wait_apoke_start_trial) + trial_start[jj]
        except:
            wait_apoke_start_trial = np.nan

        try:
            wait_apoke_end_trial = trial_band_states.query(
                "MSG=='WaitResponse' or MSG=='WaitResponseLight'")['BPOD-FINAL-TIME'].values.astype(float)
            wait_apoke_end_trial = np.nanmax(wait_apoke_end_trial) + trial_start[jj]
        except:
            wait_apoke_end_trial = np.nan

        try:
            play_target_start_trial = trial_band_states.query(
                "TYPE=='STATE' and (MSG=='StartSound' or MSG=='StartSoundLight')")['BPOD-INITIAL-TIME'].values.astype(float)
            play_target_start_trial = np.nanmax(play_target_start_trial) + trial_start[jj]
        except:
            play_target_start_trial = np.nan

        try:
            play_target_end_trial = trial_band_states.query(
                "TYPE=='STATE' and (MSG=='StartSound' or MSG=='StartSoundLight')")['BPOD-FINAL-TIME'].values.astype(float)
            play_target_end_trial = np.nanmax(play_target_end_trial) + trial_start[jj]
        except:
            play_target_end_trial = np.nan

        try:
            reward_start_trial = trial_band_states.query(
                "TYPE=='STATE' and (MSG=='Reward' or MSG=='RewardLight')")['BPOD-INITIAL-TIME'].values.astype(float)
            reward_start_trial = np.nanmax(reward_start_trial) + trial_start[jj]
        except:
            reward_start_trial = np.nan

        try:
            reward_end_trial = trial_band_states.query(
                "TYPE=='STATE' and (MSG=='Reward' or MSG=='RewardLight')")['BPOD-FINAL-TIME'].values.astype(float)
            reward_end_trial = np.nanmax(reward_end_trial) + trial_start[jj]
        except:
            reward_end_trial = np.nan

        early_withdrawal_pre_trial = pre_stim_delay_end_trial[:-1]

        wait_cpoke_start.append(wait_cpoke_start_trial)
        wait_cpoke_end.append(wait_cpoke_end_trial)

        if sum(~np.isnan(wait_cpoke_start_trial)) > 1:
            fixation_breaks.append(True)
        else:
            fixation_breaks.append(False)

        wait_apoke_start.append(wait_apoke_start_trial)
        wait_apoke_end.append(wait_apoke_end_trial)
        play_target_start.append(play_target_start_trial)
        play_target_end.append((play_target_end_trial))
        reward_start.append(reward_start_trial)
        reward_end.append((reward_end_trial))

        pre_stim_delay_start.append(pre_stim_delay_start_trial)
        pre_stim_delay_end.append(pre_stim_delay_end_trial)
        early_withdrawal_pre.append(early_withdrawal_pre_trial)

        found_l_in = found_c_in = found_r_in = found_l_out = found_c_out = found_r_out = False
        c_start = []
        l_start = []
        r_start = []
        c_end = []
        l_end = []
        r_end = []
        for (_, row) in trial_band_events.iterrows():
            if row['MSG'] == active_ports_in[0]:  # Port1In
                found_l_in = True
                if found_l_out:
                    l_start.append(np.nan)
                    found_l_out = False
                l_start.append(row['BPOD-INITIAL-TIME'])
            elif row['MSG'] == active_ports_in[1]:  # Port2In
                found_c_in = True
                if found_c_out:
                    c_start.append(np.nan)
                    found_c_out = False
                c_start.append(row['BPOD-INITIAL-TIME'])
            elif row['MSG'] == active_ports_in[2]:  # Port3In
                found_r_in = True
                if found_r_out:
                    r_start.append(np.nan)
                    found_r_out = False
                r_start.append(row['BPOD-INITIAL-TIME'])
            elif row['MSG'] == active_ports_out[0]:  # Port1Out
                if not found_l_in:
                    found_l_out = True
                else:
                    found_l_in = False
                l_end.append(row['BPOD-INITIAL-TIME'])
            elif row['MSG'] == active_ports_out[1]:  # Port2Out
                if not found_c_in:
                    found_c_out = True
                else:
                    found_c_in = False
                c_end.append(row['BPOD-INITIAL-TIME'])
            elif row['MSG'] == active_ports_out[2]:  # Port3Out
                if not found_r_in:
                    found_r_out = True
                else:
                    found_r_in = False
                r_end.append(row['BPOD-INITIAL-TIME'])
            else:
                pass

        if found_l_in:
            l_end.append(np.nan)
        if found_c_in:
            c_end.append(np.nan)
        if found_r_in:
            r_end.append(np.nan)

        if found_l_out:
            l_start.append(np.nan)
        if found_c_out:
            c_start.append(np.nan)
        if found_r_out:
            r_start.append(np.nan)

        c_start = [x + trial_start[jj] for x in c_start]
        c_end = [x + trial_start[jj] for x in c_end]
        l_start = [x + trial_start[jj] for x in l_start]
        l_end = [x + trial_start[jj] for x in l_end]
        r_start = [x + trial_start[jj] for x in r_start]
        r_end = [x + trial_start[jj] for x in r_end]

        c_poke_start.append(c_start)
        c_poke_end.append(c_end)
        l_poke_start.append(l_start)
        l_poke_end.append(l_end)
        r_poke_start.append(r_start)
        r_poke_end.append(r_end)

        duration_light_trial = 0

        trial_end_trial = trial_band.query("TYPE=='INFO' and MSG=='TRIAL-BPOD-TIME'")['+INFO'].values.astype(float)[0]

        if task == 'p4_opto_ttl':

            if states_with_light == 'iti_and_fixation':

                try:
                    start = trial_band_states.query(
                        "MSG=='WaitCPokeLight'")['BPOD-INITIAL-TIME'].values.astype(float)[0]

                    end = trial_band_states.query(
                        "MSG=='WaitCPoke' or MSG=='StartSound'")['BPOD-INITIAL-TIME'].values.astype(float)

                    end = np.nanmin(end)

                    if np.isnan(end):
                        end = trial_end_trial

                    duration_light_trial = end - start

                except:
                    duration_light_trial = 0

            elif states_with_light == 'fixation_and_stim_and_reward':

                try:
                    start = trial_band_states.query("MSG=='FixationLight'")['BPOD-INITIAL-TIME'].values.astype(float)[0]

                    end = trial_band_states.query(
                        "MSG=='WaitCPoke' or MSG=='WaitResponse' or MSG=='PunishLight2'")['BPOD-INITIAL-TIME'].values.astype(float)

                    end = np.nanmin(end)
                    if np.isnan(end):
                        end = trial_end_trial

                    duration_light_trial = end - start

                except:
                    duration_light_trial = 0

        elif task == 'p4_opto_ramp' or task == 'p4_opto_diffalt':

            start = trial_band.query(" TYPE=='SOFTCODE' and MSG=='1'")['PC-TIME'].values
            end = trial_band.query(" TYPE=='SOFTCODE' and MSG=='2'")['PC-TIME'].values

            if start.size == 0:
                duration_light_trial = 0
            elif end.size == 0:
                duration_light_trial = max_duration_light
            else:
                end = datetime.strptime(end[0][11:], '%H:%M:%S.%f')
                start = datetime.strptime(start[0][11:], '%H:%M:%S.%f')
                duration_light_trial = ((end-start) / timedelta(seconds=1)) + duration_ramp

        duration_light.append(min(duration_light_trial, max_duration_light))

    wait_apoke_start = np.array(wait_apoke_start)
    wait_apoke_end = np.array(wait_apoke_end)
    if wait_apoke_start.size != 0:
        wait_apoke_start += trial_start
    if wait_apoke_end.size != 0:
        wait_apoke_end += trial_start

    return {'state_0': 'state_0',
            'final_state': 'final_state',
            'check_next_trial_ready': 'check_next_trial_ready',
            'trial_start': trial_start,
            'trial_end': trial_end,
            'play_target_start': play_target_start,
            'play_target_end': play_target_end,
            'wait_apoke_start': wait_apoke_start,
            'wait_apoke_end': wait_apoke_end,
            'reward_start': reward_start,
            'reward_end': reward_end,
            'intertrial_interval_start': intertrial_interval_start,
            'intertrial_interval_end': intertrial_interval_end,
            'wait_cpoke_start': wait_cpoke_start,
            'wait_cpoke_end': wait_cpoke_end,
            'pre_stim_delay_start': pre_stim_delay_start,
            'pre_stim_delay_end': pre_stim_delay_end,
            'early_withdrawal_pre': early_withdrawal_pre,
            'c_poke_start': c_poke_start,
            'c_poke_end': c_poke_end,
            'l_poke_start': l_poke_start,
            'l_poke_end': l_poke_end,
            'r_poke_start': r_poke_start,
            'r_poke_end': r_poke_end,
            'fixation_breaks': fixation_breaks,
            'duration_light': duration_light
            }
