import logging
import warnings
import os
import re
import datetime
import matplotlib as mpl
import matplotlib.dates as mdates
import matplotlib.pyplot as plt
import numpy as np
import seaborn as sns
import pandas as pd
from matplotlib.backends.backend_pdf import PdfPages
from matplotlib.lines import Line2D
from matplotlib.ticker import PercentFormatter
import matplotlib.patches as mpatches
from numpy import linalg as ln
import logging
if os.environ.get('DISPLAY', '') == '':
    mpl.use('Agg')


# LOGGING AND WARNINGS
logger = logging.getLogger(__name__)
warnings.simplefilter(action='ignore', category=UserWarning)
warnings.simplefilter(action='ignore', category=RuntimeWarning)
warnings.simplefilter(action='ignore', category=FutureWarning)


# PLOT PARAMETERS
mpl.rcParams['lines.linewidth'] = 0.7
mpl.rcParams['lines.markersize'] = 1
mpl.rcParams['font.size'] = 7
mpl.rcParams['axes.spines.right'] = False
mpl.rcParams['axes.spines.top'] = False
label_kwargs = {'fontsize': 9, 'fontweight': 'roman'}

# PLOT COLORS AND MARKS
stage_marks = ['o', '^', 's', 'D']

# PLOT COLORS
first_poke_c = '#C97F74'
second_poke_c = '#96367C'
all_poke_c = '#5B1132'

first_correct_c = 'green'
correct_c = 'lightgreen'
incorrect_c = 'red'
water_c = 'teal'
other_c = 'black'

vg_c = '#0f1135'
wmi_c = '#52749b'
wmds_c = '#89abf7'
wmdl_c= '#93ecf9'

delay_s_c = 0
delay_l_c = 0

stim_diam_c = '#ffe607'
vg_th_diam_c = '#ffa600'
wm_th_diam_c = '#fc6900'

lines_c = 'gray'
lines2_c = 'silver'

def wmfm_intersession_report(intersession_df_path, intersession_report_path):

    try:
        df = pd.read_csv(intersession_df_path, sep=';')
    except FileNotFoundError:
        logger.critical("Error: Reading trials CSV file. Exiting...")
        raise

    # we set the index of the df as number of session, necessary if we want to unnest columns
    df.index = df.session

    # USEFUL VALUES
    total_sessions = df.shape[0]
    max_stage = df.stage_number.max()
    basal_weight = df['basal_weight'].iloc[0]
    average_weight = df.weight.mean()

    # CHANCE CALCULATION
    screen_size = 1440 * 0.28
    vg_th_diam = df.vg_th_diameter
    wm_th_diam = df.wm_th_diameter
    vg_chance_p = 1 / (screen_size / vg_th_diam)
    wm_chance_p = 1 / (screen_size / wm_th_diam)

    with PdfPages(intersession_report_path) as pdf:

        # Y AXES TO 100 %
        def pcn_y_ax():
            axes.set_ylim(0, 1.10)
            axes.set_yticks(list(np.arange(0, 1.1, 0.1)))
            axes.set_yticklabels(['0', '', '', '', '', '50', '', '', '', '', '100'])

        # X AXES TO SESSION NUMBER
        def session_x_ax():
            sessions = ['']
            for s in range(total_sessions):
                s = str(-total_sessions + s + 1)
                sessions.append(s)
            axes.set_xticklabels(sessions)

        # PAGE 1:
        plt.figure(figsize=(8.3, 11.7))  # A4 vertical

        # HEADER
        axes = plt.subplot2grid((50, 50), (0, 0), rowspan=8, colspan=50)

        s1 = ('Subject name: ' + str(df['subject_name'].iloc[0]) +
              '  / Basal weight: ' + str(basal_weight) + " g" +
              '  / Average weight: ' + str(round( average_weight, 2)) + " g" +
              '  / Last weight: ' + str(df['weight'].iloc[-1]) + " g" + '\n')
        axes.text(0.1, 0.9, s1,  fontsize=8, transform=plt.gcf().transFigure)

        # ACCURACY VS SESSION
        axes.plot(df.session, df.total_acc_all, c=all_poke_c)
        axes.plot(df.session, df.total_acc_sec, c=second_poke_c)
        axes.plot(df.session, df.total_acc_first, c=first_poke_c)
        axes.hlines(y=[50, 100], xmin=0, xmax=total_sessions, color=lines_c, linestyle=':')
        axes.fill_between(df.session, vg_chance_p, 0, facecolor=lines_c, alpha=0.5)
        axes.fill_between(df.session, wm_chance_p, 0, facecolor=lines2_c, alpha=0.3)

        df_stages = [df[df.stage_number == i + 1] for i in range(int(max_stage))]   #different dots for different stages
        for i in range(int(max_stage)):
            axes.plot(df_stages[i].session, df_stages[i].total_acc_all, fillstyle='none', linewidth=0,
                      c=other_c, marker=stage_marks[i], zorder=10)
            axes.plot(df_stages[i].session, df_stages[i].total_acc_sec, fillstyle='none', linewidth=0,
                      c=other_c, marker=stage_marks[i], zorder=10)
            axes.plot(df_stages[i].session, df_stages[i].total_acc_first, fillstyle='none', linewidth=0,
                      c=other_c, marker=stage_marks[i], zorder=10)

        axes.set_ylabel('Accuracy (%)', label_kwargs)
        axes.set_xticks(df.session) # list(np.arange(0, total_sessions + 1, 1)
        plt.xticks(rotation=70)

        # legend
        colors = [first_poke_c, second_poke_c, all_poke_c, stim_diam_c, vg_th_diam_c, wm_th_diam_c]
        linestyles = ['-', '-', '-', '--', '--', '--']
        labels = ['First pokes', 'First & sec pokes' , 'All pokes', 'Stimulus', 'Correct window VG', 'Correct window WM']
        lines = [Line2D([0], [0], color = colors[i], linestyle = linestyles[i]) for i in range(len(colors))]
        for i in range(int(max_stage)):
            lines.append(Line2D([0], [0], marker=stage_marks[i], fillstyle='none',
                                markersize=6, markeredgecolor=other_c, linewidth=0))
            labels.append('Stage ' + str(i + 1))
        axes.legend(lines, labels, fontsize=6, loc='center', bbox_to_anchor=(1, 1.2))


        # CORRECT WINDOW THRESHOLD VS SESSION
        axes = plt.subplot2grid((50, 50), (10, 0), rowspan=4, colspan=50)
        axes.plot(df.session, df.vg_th_diameter, c=vg_th_diam_c, linestyle='--', linewidth=1)
        axes.plot(df.session, df.wm_th_diameter, c=wm_th_diam_c, linestyle='--', linewidth=1)
        axes.plot(df.session, df.stim_diameter, c=stim_diam_c, linestyle='--', linewidth=1)

        axes.set_xticks(df.session)  # list(np.arange(0, total_sessions + 1, 1)
        plt.xticks(rotation=70)
        axes.set_ylabel("Threshold (mm)", label_kwargs)

        # WEIGHT PLOT
        df['relative_weight'] = df['weight'] / df['basal_weight'] * 100

        axes = plt.subplot2grid((50, 50), (16, 0), rowspan=4, colspan=50)
        axes.plot(df.session, df.relative_weight, c=other_c, marker=stage_marks[i])
        axes.hlines(y=[80, 100], xmin=0, xmax=total_sessions, color=lines_c, linestyle=':')

        axes.set_xticks(df.session)  # list(np.arange(0, total_sessions + 1, 1)
        plt.xticks(rotation=70)
        axes.set_ylabel("Relative\nWeight (%)", label_kwargs)

        # NUMBER OF TRIALS VS SESSION
        axes = plt.subplot2grid((50, 50), (22, 0), rowspan=4, colspan=50)

        axes.plot(df.session, df.total_trials, c=all_poke_c)
        for i in range(int(max_stage)):
            axes.plot(df_stages[i].session, df_stages[i].total_trials, fillstyle='none', linewidth=0,
                      c=other_c, marker=stage_marks[i], zorder=10)

        axes.set_ylabel("Number of trials", label_kwargs)
        axes.set_xlabel("Session Number", label_kwargs)
        axes.set_xlim([0.5, total_sessions + 0.5])
        axes.set_xticks(list(np.arange(0, total_sessions + 1, 1)))
        axes.set_xticks(df.session)  # list(np.arange(0, total_sessions + 1, 1)
        plt.xticks(rotation=70)
        labels = ['Nº trials']

        # LICK PORT LATENCIES
        axes = plt.subplot2grid((50, 50), (29, 0), rowspan=12, colspan=22)

        # axes.errorbar(df.session, df.licklat_miss_mean, yerr=df.licklat_miss_std.values / np.sqrt(total_sessions),
        #               c=other_c, marker='o', markersize=2)
        axes.errorbar(df.session, df.licklat_incorrect_median,
                      yerr=df.licklat_incorrect_std.values / np.sqrt(total_sessions),
                      c=incorrect_c, marker='o', markersize=2)
        axes.errorbar(df.session, df.licklat_correct_median, yerr=df.licklat_correct_std.values / np.sqrt(total_sessions),
                      c=correct_c, marker='o', markersize=2)
        axes.errorbar(df.session, df.licklat_correct_first_median,
                      yerr=df.licklat_correct_first_std.values / np.sqrt(total_sessions),
                      c=first_correct_c, marker='o', markersize=2)

        axes.set_ylim(0, 10)
        axes.set_xlabel("Session number", label_kwargs)
        axes.set_ylabel("Median lick port latency (sec)", label_kwargs)

        colors = [first_correct_c, correct_c, incorrect_c, other_c]
        labels = ['Correct First', 'Correct Other', 'Incorrect', 'Miss']
        lines = [Line2D([0], [0], color=c, marker='o', markersize=5, markerfacecolor=c) for c in colors]
        axes.legend(lines, labels, fontsize=6)

        # DISTANCES STIMULUS (ERROR)
        axes = plt.subplot2grid((50, 50), (29, 28), rowspan=12, colspan=22)

        axes.errorbar(df.session, df.x_err_median, yerr=df.x_err_std.values / np.sqrt(total_sessions),
                       c=all_poke_c, marker='o', markersize=2)
        axes.hlines(y=0, xmin=0, xmax=total_sessions, color=lines_c, linestyle=':')

        axes.set_xlabel("Session number", label_kwargs)
        axes.set_ylabel("$ Median\ Error\ (r_{t}\ -\ x_{t})\ (mm)$", label_kwargs)

        # SAVING AND CLOSING PAGE 1
        pdf.savefig()
        plt.close()
    #
    #     # PAGE 2:
    #     plt.figure(figsize=(8.3, 11.7))  # A4 vertical
    #
    #     # DISTANCES STIMULUS (ERROR)
    #     axes = plt.subplot2grid((50, 50), (0, 0), rowspan=10, colspan=22)
    #
    #     axes.errorbar(df.session, df.x_distance_mean, yerr=df.x_distance_std.values / np.sqrt(number_of_sessions),
    #                    c=all_poke_c, marker='o', markersize=2)
    #
    #     #axes.plot(df.session, df.x_distance_mean, c=all_poke_c, marker='o', markersize=2)
    #     axes.hlines(y=0, xmin=0, xmax=number_of_sessions - 1, color=lines_c, linestyle=':')
    #
    #     axes.set_xlabel("Session number", label_kwargs)
    #     axes.set_ylabel("Distance stimulus (mm)", label_kwargs)
    #     axes.set_xticks(list(np.arange(0, number_of_sessions + 1, 1)))
    #     sessions = ['']
    #     for s in range(number_of_sessions):
    #         s = str(-number_of_sessions + s + 1)
    #         sessions.append(s)
    #     axes.set_xticklabels(sessions)
    #
    #     # ACCURACY DEPENDING ON ALPHA TYPE
    #     axes = plt.subplot2grid((50, 50), (13, 0), rowspan=6, colspan=50)
    #
    #     axes.plot(df.session, df.accuracy_alpha1, c=easy_alpha, marker='o', markersize=2)
    #     axes.plot(df.session, df.accuracy_alpha2, c=med_alpha, marker='o', markersize=2)
    #     axes.plot(df.session, df.accuracy_alpha3, c=dif_alpha, marker='o', markersize=2)
    #     axes.hlines(y=[0.5, 1], xmin=0, xmax=number_of_sessions, color=lines_c, linestyle=':')
    #     axes.fill_between(df.session, chance_prob, -0.1, facecolor=lines_c, alpha=0.2)
    #
    #     axes.set_ylim(-0.1, 1.1)
    #     axes.set_yticks(list(np.arange(0, 1.1, 0.1)))
    #     axes.set_yticklabels(['0', '', '', '', '', '50', '', '', '', '', '100'])
    #     axes.set_ylabel('Accuracy (%)', label_kwargs)
    #     axes.set_xlim([0.5, number_of_sessions + 0.5])
    #     axes.set_xticks(list(np.arange(0, number_of_sessions + 1, 1)))
    #     axes.set_xticklabels([''])
    #
    #     # %VALIDS DEPENDING ON ALPHA TYPE
    #     axes = plt.subplot2grid((50, 50), (19, 0), rowspan=6, colspan=50)
    #
    #     axes.plot(df.session, df.valids_percent_alpha1, c=easy_alpha, marker='o', markersize=2)
    #     axes.plot(df.session, df.valids_percent_alpha2, c=med_alpha, marker='o', markersize=2)
    #     axes.plot(df.session, df.valids_percent_alpha3, c=dif_alpha, marker='o', markersize=2)
    #     axes.hlines(y=[0.5, 1], xmin=0, xmax=number_of_sessions, color=lines_c, linestyle=':')
    #     axes.fill_between(df.session, chance_prob, -0.1, facecolor=lines_c, alpha=0.2)
    #
    #     axes.set_ylim(-0.1, 1.1)
    #     axes.set_yticks(list(np.arange(0, 1.1, 0.1)))
    #     axes.set_yticklabels(['0', '', '', '', '', '50', '', '', '', '', '100'])
    #     axes.set_ylabel('Valids (%)', label_kwargs)
    #     axes.set_xlim([0.5, number_of_sessions + 0.5])
    #     axes.set_xticks(list(np.arange(0, number_of_sessions + 1, 1)))
    #     sessions = ['']
    #     for s in range(number_of_sessions):
    #         s = str(-number_of_sessions + s + 1)
    #         sessions.append(s)
    #     axes.set_xticklabels(sessions)
    #
    #     colors = [easy_alpha, med_alpha, dif_alpha]
    #     labels = ['Alpha easy', 'Alpha med', 'Alpha dif']
    #     lines = [Line2D([0], [0], color=c, marker='o', markersize=5, markerfacecolor=c) for c in colors]
    #     axes.legend(lines, labels, fontsize=6, loc = 'lower left', bbox_to_anchor = (0, 0))
    #
    #     # ACCURACY VG vs WM
    #     axes = plt.subplot2grid((50, 50), (28, 0), rowspan=6, colspan=50)
    #
    #     axes.plot(df.session, df.vg_accuracy, c=vg_c, marker='o', markersize=2)
    #     axes.plot(df.session, df.wm_accuracy, c=wm_c, marker='o', markersize=2)
    #     axes.hlines(y=[0.5, 1], xmin=0, xmax=number_of_sessions, color=lines_c, linestyle=':')
    #     axes.fill_between(df.session, chance_prob, 0, facecolor=lines_c, alpha=0.4)
    #
    #     axes.set_ylim(0, 1)
    #     axes.set_yticks(list(np.arange(0, 1.1, 0.1)))
    #     axes.set_yticklabels(['0', '', '', '', '', '50', '', '', '', '', '100'])
    #     axes.set_ylabel("Accuracy (%)", label_kwargs)
    #     axes.set_xlim([0.5, number_of_sessions + 0.5])
    #     axes.set_xlabel("Session number", label_kwargs)
    #     axes.set_xticks(list(np.arange(0, number_of_sessions + 1, 1)))
    #     sessions = ['']
    #     for s in range(number_of_sessions):
    #         s = str(-number_of_sessions + s + 1)
    #         sessions.append(s)
    #     axes.set_xticklabels(sessions)
    #
    #     colors = [vg_c, wm_c]
    #     labels = ['Visually\nGuided', 'WM\nGuided']
    #     lines = [Line2D([0], [0], color=c, marker='o', markersize=5, markerfacecolor=c) for c in colors]
    #     axes.legend(lines, labels, fontsize=6, loc = 'lower left', bbox_to_anchor = (0, 0))
    #
    #     # RESPONSE TIME DEPENDING ON ALPHA TIME
    #     axes = plt.subplot2grid((50, 50), (37, 0), rowspan=13, colspan=50)
    #
    #     x_array = np.asarray(df.session)
    #     axes.bar(x_array, height=df.alpha1_correct_pokes_time_mean, color=first_correct_c, width=0.4)
    #     axes.bar(x_array+0.4, height=df.alpha1_incorrect_pokes_time_mean, color=incorrect_c, width=0.4)
    #
    #     sessions = []
    #     for s in range(number_of_sessions):
    #         s = str(-number_of_sessions + s + 1)
    #         sessions.append(s)
    #     axes.set_xticklabels(sessions)
    #     axes.set_xticks(x_array+0.2)
    #
    #     axes.set_ylim(0, 6)
    #     axes.set_ylabel("Mean response time (sec)", label_kwargs)
    #     axes.set_xlabel("Session number", label_kwargs)
    #
    #     colors = [first_correct_c, incorrect_c]
    #     labels = ['Correct', 'Incorrect']
    #     lines = [Line2D([0], [0], color=c, marker='o', markersize=6, markerfacecolor=c) for c in colors]
    #     axes.legend(lines, labels, fontsize=6, loc='upper right', bbox_to_anchor=(1, 1))
    #
    #
    #     # SAVING AND CLOSING PAGE 2
    #     
    #     pdf.savefig()
    #     plt.close()

    # ACC TRIAL TYPE
    #     axes.plot(df.session, df.alpha3_time, c=dif_alpha, marker='o', markersize=2)
    #     axes.plot(df.session, df.alpha2_time, c=med_alpha, marker='o', markersize=2)
    #     axes.hlines(y=[0.5, 1, 1.5, 2], xmin=0, xmax=number_of_sessions, color=lines_c, linestyle=':')
    #
    #     axes.set_ylabel("Alpha (sec)", label_kwargs)
    #     axes.set_ylim(0, 3)
    #     axes.set_xticks(list(np.arange(0, number_of_sessions + 1, 1)))
    #     sessions = ['']
    #     for s in range(number_of_sessions):
    #         s = str(-number_of_sessions + s + 1)
    #         sessions.append(s)
    #     axes.set_xticklabels(sessions)
    #
    #     colors = [med_alpha, dif_alpha]
    #     labels = ['Alpha med', 'Alpha dif']
    #     lines = [Line2D([0], [0], color=c, marker='o', markersize=5, markerfacecolor=c) for c in colors]
    #     axes.legend(lines, labels, fontsize=6)

