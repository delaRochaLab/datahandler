import os
import matplotlib as mpl
import matplotlib.pyplot as plt
import matplotlib.patches as mpatches
import seaborn as sns
import numpy as np
import pandas as pd
from matplotlib.backends.backend_pdf import PdfPages
from matplotlib.lines import Line2D
from datahandler import Utils
import logging
import warnings
if os.environ.get('DISPLAY', '') == '':
    mpl.use('Agg')


# LOGGING AND WARNINGS
logger = logging.getLogger(__name__)
warnings.simplefilter(action='ignore', category=UserWarning)
warnings.simplefilter(action='ignore', category=RuntimeWarning)
warnings.simplefilter(action='ignore', category=FutureWarning)
pd.options.mode.chained_assignment = None  # suppress warning about chaining assignment

# PLOT PARAMETERS
mpl.rcParams['lines.linewidth'] = 0.7
mpl.rcParams['lines.markersize'] = 1
mpl.rcParams['font.size'] = 7
mpl.rcParams['axes.spines.right'] = False
mpl.rcParams['axes.spines.top'] = False
label_kwargs = {'fontsize': 9, 'fontweight': 'roman'}

# PLOT COLORS
first_poke_c = 'palevioletred'
second_poke_c = 'mediumslateblue'
all_poke_c = 'black'

first_correct_c = 'green'
other_correct_c = 'lightgreen'
incorrect_c = 'red'

easy_alpha = 'skyblue'
med_alpha = 'lightseagreen'
dif_alpha = 'midnightblue'

vg_c = 'purple'
wm_c = 'lightseagreen'

water_c = 'blue'
other_c = 'darkcyan'

diameter_c = 'orange'
threshold_c = 'darkgoldenrod'

lines_c = 'gray'
bar_lines = 'gainsboro'
bar_lines2 = 'whitesmoke'
axes_c = 'black'


def wmfm_daily_report_old(trials_path, params_path, daily_report_path):
    """
    Creates a daily report with the performance and psychometric plots, if necessary.
    We use a dataframe with one row per trial: df.
    And a pandas series: params.
    At the end, we need to return params including the new values that we have calculated.
    """

    # READ THE CSVS
    # trials_path contains a csv with one row per trial, we read it as a dataframe
    # we need to set the index of the df using the column containing the number of trial
    # this is necessary if we want to unnest columns
    # when reading the df the columns that contain lists are read as string, transform them to lists

    try:
        df = pd.read_csv(trials_path, sep=';')
    except FileNotFoundError:
        logger.critical("Error: Reading trials CSV file. Exiting...")
        raise

    try:
        params = pd.read_csv(params_path, sep=';')
    except FileNotFoundError:
        logger.critical("Error: Reading params CSV file. Exiting...")
        raise

    df.index = df.trial
    df = Utils.convert_strings_to_lists(df, ['all_responses_time', 'incorrects', 'wm_guided_responses', 'x_responses',
                                             'y_responses', 'x_distances', 'y_distances', 'distances', 'numbers',
                                             'stimulus_onsets', 'fixation_breaks'])

    # transform params df that only contains one row in a Series object
    params = params.iloc[0]

    # NEW USEFUL COLUMNS
    df['correct_first_poke_bool'] = df.trial_result == 1
    df['correct_first_second_poke_bool'] = df.trial_result.isin([1, 2])
    df['correct_all_poke_bool'] = df.trial_result > 0

    # EXPANDED DATAFRAMES
    responses_df = Utils.unnesting(df, ['all_responses_time', 'incorrects', 'wm_guided_responses', 'x_responses',
                                        'y_responses', 'x_distances', 'y_distances', 'distances', 'numbers'])
    onsets_df = Utils.unnesting(df, ['stimulus_onsets', 'fixation_breaks'])


    # SUB-DATAFRAMES
    valids_df = df[df.trial_result != 0]
    df['valids_df_bool'] = df.trial_result != 0 #redundant no?

    clean_responses_df = responses_df.dropna(subset=['all_responses_time'])

    first_responses_df = clean_responses_df.drop_duplicates(subset='trial', keep='first', inplace=False)
    first_responses_df[
        'first_responses_stimoff_time'] = first_responses_df.all_responses_time - first_responses_df.stimulus_off

    responses_df['answer_stimoff_time'] = responses_df.all_responses_time - responses_df.stimulus_off

    valids_df['accuracy_first_poke_rw'] = Utils.compute_window(valids_df.correct_first_poke_bool, 20)
    valids_df['accuracy_first_second_poke_rw'] = Utils.compute_window(valids_df.correct_first_second_poke_bool, 20)
    valids_df['accuracy_all_poke_rw'] = Utils.compute_window(valids_df.correct_all_poke_bool, 20)

    correct_first_poke_df = df[df.trial_result == 1]
    correct_other_poke_df = df[df.trial_result > 1]
    incorrect_df = df[df.trial_result < 1]
    miss_df = df[df.trial_result == 0]

    correct_responses_wm_first_df = responses_df[responses_df.wm_guided_responses & (responses_df.numbers == 0) &
                                         (responses_df.trial_result == 1)]
    correct_responses_wm_other_df = responses_df[responses_df.wm_guided_responses & (responses_df.numbers == 0) &
                                         (responses_df.trial_result > 1)]
    correct_responses_wm_none_df = responses_df[responses_df.wm_guided_responses & (responses_df.numbers > 0)]
    correct_responses_vg_first_df = responses_df[(responses_df.wm_guided_responses == False) & (responses_df.numbers == 0) &
                                               (responses_df.trial_result == 1)]
    correct_responses_vg_other_df = responses_df[(responses_df.wm_guided_responses == False) & (responses_df.numbers == 0) &
                                               (responses_df.trial_result > 1)]
    correct_responses_vg_none_df = responses_df[(responses_df.wm_guided_responses == False) & (responses_df.numbers > 0)]

    vg_df = first_responses_df[first_responses_df.wm_guided_responses == False]
    wm_df = first_responses_df[first_responses_df.wm_guided_responses == True]

    corrects_vg_df = vg_df[vg_df.trial_result == 1]         #same as correct_responses_vg_first_df
    corrects_wm_df = wm_df[wm_df.trial_result == 1]

    alpha1_df = df[df.alpha_type == 1]
    alpha2_df = df[df.alpha_type == 2]
    alpha3_df = df[df.alpha_type == 3]

    alpha1_valids_df = valids_df[valids_df.alpha_type == 1]
    alpha2_valids_df = valids_df[valids_df.alpha_type == 2]
    alpha3_valids_df = valids_df[valids_df.alpha_type == 3]

    alpha1_df['alpha1_valids_rw'] = Utils.compute_window(alpha1_df.valids_df_bool, 20)
    alpha2_df['alpha2_valids_rw'] = Utils.compute_window(alpha2_df.valids_df_bool, 20)
    alpha3_df['alpha3_valids_rw'] = Utils.compute_window(alpha3_df.valids_df_bool, 20)

    alpha1_df['performance_alpha1_rw'] = Utils.compute_window(alpha1_df.correct_first_poke_bool, 20)
    alpha2_df['performance_alpha2_rw'] = Utils.compute_window(alpha2_df.correct_first_poke_bool, 20)
    alpha3_df['performance_alpha3_rw'] = Utils.compute_window(alpha3_df.correct_first_poke_bool, 20)

    alpha1_valids_df['accuracy_alpha1_rw'] = Utils.compute_window(alpha1_valids_df.correct_first_poke_bool, 20)
    alpha2_valids_df['accuracy_alpha2_rw'] = Utils.compute_window(alpha2_valids_df.correct_first_poke_bool, 20)
    alpha3_valids_df['accuracy_alpha3_rw'] = Utils.compute_window(alpha3_valids_df.correct_first_poke_bool, 20)

    # USEFUL VALUES
    total_trials = df.shape[0]

    correct_first_poke_sum = df[df.trial_result == 1].shape[0]
    correct_first_second_poke_sum = df[df.trial_result.isin([1, 2])].shape[0]
    correct_all_poke_sum = df[df.trial_result > 0].shape[0]

    valid_trials_sum = df[df.trial_result != 0].shape[0]
    missed_trials_sum = df[df.trial_result == 0].shape[0]

    lick_first_correct_time = correct_first_poke_df.lick_time - correct_first_poke_df.correct
    lick_other_corrects_time = correct_other_poke_df.lick_time - correct_other_poke_df.correct
    lick_incorrect_time = incorrect_df.lick_time - params.timeup
    lick_miss_time = miss_df.lick_time - params.timeup

    accuracy_first_poke_alls = correct_first_poke_sum / valid_trials_sum * 100
    accuracy_first_second_poke_alls = correct_first_second_poke_sum / valid_trials_sum * 100
    accuracy_all_poke_alls = correct_all_poke_sum / valid_trials_sum * 100
    performance_first_poke_alls = correct_first_poke_sum / total_trials * 100
    performance_first_second_poke_alls = correct_first_second_poke_sum / total_trials * 100
    performance_all_poke_alls = correct_all_poke_sum / total_trials * 100

    vg_trials_sum = vg_df.shape[0] #at first poke
    wm_trials_sum = wm_df.shape[0]
    correct_vg_trials_sum = corrects_vg_df.shape[0] #at first poke
    correct_wm_trials_sum = corrects_wm_df.shape[0]

    trials_alpha1_sum = alpha1_df.shape[0]
    trials_alpha2_sum = alpha2_df.shape[0]
    trials_alpha3_sum = alpha3_df.shape[0]

    valid_trials_alpha1_sum = alpha1_df[alpha1_df.trial_result != 0].shape[0]
    valid_trials_alpha2_sum = alpha2_df[alpha2_df.trial_result != 0].shape[0]
    valid_trials_alpha3_sum = alpha3_df[alpha3_df.trial_result != 0].shape[0]

    valids_percent_alpha1_sum = valid_trials_alpha1_sum / trials_alpha1_sum if valid_trials_alpha1_sum != 0 else np.nan
    valids_percent_alpha2_sum = valid_trials_alpha2_sum / trials_alpha2_sum if valid_trials_alpha2_sum != 0 else np.nan
    valids_percent_alpha3_sum = valid_trials_alpha3_sum / trials_alpha3_sum if valid_trials_alpha3_sum != 0 else np.nan

    correct_first_poke_alpha1_sum = alpha1_df[alpha1_df.trial_result == 1].shape[0]
    correct_first_poke_alpha2_sum = alpha2_df[alpha2_df.trial_result == 1].shape[0]
    correct_first_poke_alpha3_sum = alpha3_df[alpha3_df.trial_result == 1].shape[0]

    # we use np.divide because the denominators can be zero
    vg_accuracy_mean = len(corrects_vg_df) / len(vg_df) if len(vg_df) != 0 else np.nan
    wm_accuracy_mean = len(corrects_wm_df) / len(wm_df) if len(wm_df) != 0 else np.nan

    accuracy_alpha1_alls = correct_first_poke_alpha1_sum / valid_trials_alpha1_sum if valid_trials_alpha1_sum != 0 else np.nan
    accuracy_alpha2_alls = correct_first_poke_alpha2_sum / valid_trials_alpha2_sum if valid_trials_alpha2_sum != 0 else np.nan
    accuracy_alpha3_alls = correct_first_poke_alpha3_sum / valid_trials_alpha3_sum if valid_trials_alpha3_sum != 0 else np.nan
    performance_alpha1_alls = correct_first_poke_alpha1_sum / trials_alpha1_sum if trials_alpha1_sum != 0 else np.nan
    performance_alpha2_alls = correct_first_poke_alpha2_sum / trials_alpha2_sum if trials_alpha2_sum != 0 else np.nan
    performance_alpha3_alls = correct_first_poke_alpha3_sum / trials_alpha3_sum if trials_alpha3_sum != 0 else np.nan

    alpha1_acc_first_poke_alls = sum(alpha1_valids_df.correct_first_poke_bool) / len(
        alpha1_valids_df.trial) if len(alpha1_valids_df.trial) != 0 else np.nan
    alpha1_acc_first_second_poke_alls = sum(alpha1_valids_df.correct_first_second_poke_bool) / len(
        alpha1_valids_df.trial) if len(alpha1_valids_df.trial) != 0 else np.nan
    alpha1_acc_all_poke_alls = sum(alpha1_valids_df.correct_all_poke_bool) / len(
        alpha1_valids_df.trial) if len(alpha1_valids_df.trial) != 0 else np.nan

    alpha2_acc_first_poke_alls = sum(alpha2_valids_df.correct_first_poke_bool) / len(
        alpha2_valids_df.trial) if len(alpha2_valids_df.trial) != 0 else np.nan
    alpha2_acc_first_second_poke_alls = sum(alpha2_valids_df.correct_first_second_poke_bool) / len(
        alpha2_valids_df.trial) if len(alpha2_valids_df.trial) != 0 else np.nan
    alpha2_acc_all_poke_alls = sum(alpha2_valids_df.correct_all_poke_bool) / len(
        alpha2_valids_df.trial) if len(alpha2_valids_df.trial) != 0 else np.nan

    alpha3_acc_first_poke_alls = sum(alpha3_valids_df.correct_first_poke_bool) / len(
        alpha3_valids_df.trial) if len(alpha3_valids_df.trial) != 0 else np.nan
    alpha3_acc_first_second_poke_alls = sum(alpha3_valids_df.correct_first_second_poke_bool) / len(
        alpha3_valids_df.trial) if len(alpha3_valids_df.trial) != 0 else np.nan
    alpha3_acc_all_poke_alls = sum(alpha3_valids_df.correct_all_poke_bool) / len(
        alpha3_valids_df.trial) if len(alpha3_valids_df.trial) != 0 else np.nan

    alpha1_correct_pokes_time = clean_responses_df[clean_responses_df.alpha_type == 1].correct
    alpha1_incorrect_pokes_time = clean_responses_df[clean_responses_df.alpha_type == 1].incorrects
    alpha2_correct_pokes_time = clean_responses_df[clean_responses_df.alpha_type == 2].correct
    alpha2_incorrect_pokes_time = clean_responses_df[clean_responses_df.alpha_type == 2].incorrects
    alpha3_correct_pokes_time = clean_responses_df[clean_responses_df.alpha_type == 3].correct
    alpha3_incorrect_pokes_time = clean_responses_df[clean_responses_df.alpha_type == 3].incorrects

    alpha1_correct_pokes_time = alpha1_correct_pokes_time[~np.isnan(alpha1_correct_pokes_time)]
    alpha1_incorrect_pokes_time = alpha1_incorrect_pokes_time[~np.isnan(alpha1_incorrect_pokes_time)]
    alpha2_correct_pokes_time = alpha2_correct_pokes_time[~np.isnan(alpha2_correct_pokes_time)]
    alpha2_incorrect_pokes_time = alpha2_incorrect_pokes_time[~np.isnan(alpha2_incorrect_pokes_time)]
    alpha3_correct_pokes_time = alpha3_correct_pokes_time[~np.isnan(alpha3_correct_pokes_time)]
    alpha3_incorrect_pokes_time = alpha3_incorrect_pokes_time[~np.isnan(alpha3_incorrect_pokes_time)]

    alpha1_correct_pokes_time_mean = alpha1_correct_pokes_time.mean()
    alpha1_incorrect_pokes_time_mean = alpha1_incorrect_pokes_time.mean()
    alpha2_correct_pokes_time_mean = alpha2_correct_pokes_time.mean()
    alpha2_incorrect_pokes_time_mean = alpha2_incorrect_pokes_time.mean()
    alpha3_correct_pokes_time_mean = alpha3_correct_pokes_time.mean()
    alpha3_incorrect_pokes_time_mean = alpha3_incorrect_pokes_time.mean()

    fixation_breaks_mean = onsets_df.fixation_breaks.mean()
    correct_first_latencies_mean = df[df.trial_result == 1].correct.mean()
    correct_other_latencies_mean = df[df.trial_result > 1].correct.mean()
    incorrect_latencies_mean = clean_responses_df.incorrects.mean()
    correct_first_lick_mean = df[df.trial_result == 1].lick_time.mean()
    correct_other_lick_mean = df[df.trial_result > 1].lick_time.mean()
    incorrect_lick_mean = df[df.trial_result <= 0].lick_time.mean()
    distance_mean = clean_responses_df.distances.mean()
    x_distance_mean = clean_responses_df.x_distances.mean()

    fixation_breaks_std = onsets_df.fixation_breaks.std()
    correct_first_latencies_std = df[df.trial_result == 1].correct.std()
    correct_other_latencies_std = df[df.trial_result > 1].correct.std()
    incorrect_latencies_std = clean_responses_df.incorrects.std()
    correct_first_lick_std = df[df.trial_result == 1].lick_time.std()
    correct_other_lick_std = df[df.trial_result > 1].lick_time.std()
    incorrect_lick_std = df[df.trial_result <= 0].lick_time.std()
    distance_std = clean_responses_df.distances.std()
    x_distance_std = clean_responses_df.x_distances.std()

    fixation_breaks_median = onsets_df.fixation_breaks.median()
    correct_first_latencies_median = df[df.trial_result == 1].correct.median()
    correct_other_latencies_median = df[df.trial_result > 1].correct.median()
    incorrect_latencies_median = clean_responses_df.incorrects.median()
    correct_first_lick_median = df[df.trial_result == 1].lick_time.median()
    correct_other_lick_median = df[df.trial_result > 1].lick_time.median()
    incorrect_lick_median = df[df.trial_result <= 0].lick_time.median()
    distance_median = clean_responses_df.distances.median()
    x_distance_median = clean_responses_df.x_distances.median()

    # CHANCE CALCULATION
    screen_size = 1440 * 0.28
    reward_area_size = params.threshold
    chance_prob = 1 / (screen_size / reward_area_size)

    # uncomment to create files containing the csvs to explore
    responses_df.to_csv('data_responses.csv')
    onsets_df.to_csv('data_onsets.csv')


    # PLOTING
    with PdfPages(daily_report_path) as pdf:

        # PAGE 1:
        plt.figure(figsize=(8.3, 11.7))  # A4 vertical

        # HEADER
        axes = plt.subplot2grid((50, 50), (0, 0), rowspan=10, colspan=50)

        s1 = ('Date: ' + str(params.day) + '-' + str(params.time) +
              '  / Subject name: ' + str(params.subject_name) +
              '  /  Water: ' + str(params.water_drunk) + "mL"'\n')
        s2 = ('Stage: ' + str(params.stage_number) +
              '  /  Threshold: ' + str(round(params.threshold, 2)) + 'mm' +
              '  /  Alpha easy: ' + str(params.alpha1_time) +
              '  /  Alpha medium: ' + str(params.alpha2_time) +
              '  /  Alpha difficult: ' + str(params.alpha3_time) +
              '  /  Fixation: ' + str(params.fixation_time) + '\n')
        s3 = ('Valid trials: ' + str(valid_trials_sum) +
              '  /  Accuracy first poke: ' + str(round(accuracy_first_poke_alls, 2)) + '%' +
              '  /  Accuracy first and second poke: ' + str(round(accuracy_first_second_poke_alls, 2)) + '%' +
              '  /  Accuracy all poke: ' + str(round(accuracy_all_poke_alls, 2)) + '%' + '\n')
        s4 = ('Total trials: ' + str(total_trials) +
              '  /  Performance first poke: ' + str(round(performance_first_poke_alls, 2)) + '%' +
              '  /  Performance first and second poke: ' + str(round(performance_first_second_poke_alls, 2)) + '%' +
              '  /  Performance all poke: ' + str(round(performance_all_poke_alls, 2)) + '%' + '\n')
        s5 = ('Missed trials: ' + str(missed_trials_sum) +
              '  /  Visually guided trials: ' + str(vg_trials_sum) +
              '  /  WM guided trials: ' + str(wm_trials_sum) +
              '  /  VG correct 1st poke: ' + str(correct_vg_trials_sum) +
              '  /  WM correct 1st poke: ' + str(correct_wm_trials_sum) + '\n')

        axes.text(0.1, 0.9, s1 + s2 + s3 + s4 + s5, fontsize=8, transform=plt.gcf().transFigure)

        # ACCURACY PLOT
        # we use the same axes than header
        axes.plot(valids_df.trial, valids_df.accuracy_all_poke_rw, marker='o', markersize=1, color=all_poke_c)
        axes.plot(valids_df.trial, valids_df.accuracy_first_second_poke_rw, marker='o', markersize=1, color=second_poke_c)
        axes.plot(valids_df.trial, valids_df.accuracy_first_poke_rw, marker='o', markersize=1, color=first_poke_c)
        axes.hlines(y=[0.5, 1], xmin=0, xmax=total_trials, color=lines_c, linestyle=':')
        axes.fill_between(valids_df.trial, chance_prob, 0, facecolor=lines_c, alpha=0.2)

        axes.set_xlim([1, total_trials + 1])
        axes.set_ylim(0, 1.10)
        axes.set_yticks(list(np.arange(0, 1.1, 0.1)))
        axes.set_yticklabels(['0', '', '', '', '', '50', '', '', '', '', '100'])
        axes.set_xlabel('Trials', label_kwargs)
        axes.set_ylabel('Accuracy (%)', label_kwargs)

        colors = [first_poke_c, second_poke_c, all_poke_c]
        labels = ['First pokes', 'First and second pokes', 'All pokes']
        lines = [Line2D([0], [0], color=c, marker='o', markersize=6, markerfacecolor=c) for c in colors]
        axes.legend(lines, labels, fontsize=6)

        # RASTER-LIKE PLOT
        axes = plt.subplot2grid((50, 50), (14, 0), rowspan=30, colspan=34)

        #minimum_time = data_onsets.stimulus_onsets.min()
        x_min = -2
        if np.isnan(x_min):
            x_min = 0
        x_max = 16
        size_bins = 0.3

        axes.scatter(df.lick_time, df.trial, s=2, c=water_c)
        axes.scatter(responses_df.incorrects, responses_df.trial, s=2, c=incorrect_c)
        axes.scatter(correct_first_poke_df.correct, correct_first_poke_df.trial, s=2, c=first_correct_c)
        axes.scatter(correct_other_poke_df.correct, correct_other_poke_df.trial, s=2, c=other_correct_c)
        axes.vlines(x=0, ymin=0, ymax=total_trials + 1, linewidth=1, color=lines_c)
        heights = np.repeat(0.1, df.shape[0])
        trials = list(df.trial) # we need to create a list because putting directly in barh causes a small error
        onset_trials = list(onsets_df.trial)
        axes.barh(trials, -params.fixation_time, color=bar_lines, zorder=0)
        axes.barh(trials, -params.fixation_time, height=heights, color=bar_lines, zorder=0)
        axes.barh(trials, df.stimulus_off, color=bar_lines, zorder=0) #dark bar until all responses
        axes.barh(onset_trials, onsets_df.fixation_breaks - onsets_df.stimulus_onsets,
                  left=onsets_df.stimulus_onsets, color=bar_lines, zorder=0) #dark bar until incorrect response
        axes.barh(trials, df.real_alpha, color=bar_lines, zorder=0, alpha=0.2) #light bar until teoretical stimulus display time

        axes.set_xlim(x_min, x_max)
        axes.set_ylim(0, total_trials + 1)
        axes.set_xticklabels([])
        yticks = (axes.get_yticks())
        axes.set_yticks(yticks[(yticks != 0) & (yticks <= total_trials + 5)])
        axes.set_ylabel('Trial number', label_kwargs)

        # HISTOGRAM OF LATENCIES
        axes = plt.subplot2grid((50, 50), (44, 0), rowspan=6, colspan=34)

        axes.hist(df.lick_time, histtype='step', bins=np.arange(0, x_max, size_bins), alpha=0.7, color=water_c)
        axes.hist(responses_df.incorrects.dropna(), histtype='step', bins=np.arange(0, x_max, size_bins),
                  alpha=0.7, color=incorrect_c)
        axes.hist(df.correct, histtype='step', bins=np.arange(0, x_max, size_bins), alpha=0.7, color=first_correct_c)

        axes.set_xlim(x_min, x_max)
        axes.set_xlabel('Latency (sec)', label_kwargs)
        axes.set_ylabel('Number of pokes', label_kwargs)

        #legend
        colors = [first_correct_c, incorrect_c, water_c, bar_lines, bar_lines2]
        labels = ['Correct', 'Incorrect', 'Water collection', 'Stimulus visible', 'Alpha']
        widths = [0, 0, 0, 4, 4]
        markers = ['o', 'o', 'o', ',', ',']
        lines = [Line2D([0], [0], color=colors[i], marker=markers[i], markersize=6,
                        markerfacecolor=colors[i], linewidth=widths[i]) for i in range(len(colors))]
        axes.legend(lines, labels, fontsize=6)

        # LICKPORT LATENCIES
        axes = plt.subplot2grid((50, 50), (14, 38), rowspan=15, colspan=12)

        elements = [lick_first_correct_time, lick_other_corrects_time, lick_incorrect_time, lick_miss_time]

        if len(lick_first_correct_time) > 1 and len(lick_other_corrects_time) > 1 and len(lick_incorrect_time) > 1 and len(lick_miss_time) > 1:
            box = axes.boxplot(elements, showfliers=False)
            for _, line_list in box.items():
                for line in line_list:
                    line.set(color = lines_c, linewidth = 0.5)

        for i in range(4):
            y = elements[i]
            x = np.random.normal(1 + i, 0.06, size=len(y))
            axes.plot(x, y, 'r.', alpha=0.8, color=other_c, markersize=1, zorder=10)

        axes.set_ylim(0, 40)
        axes.set_xticks([1, 2, 3, 4])
        axes.set_xticklabels(['Correct\nFirst', 'Correct\nOther', 'Incorrect', 'Miss'], rotation='vertical')
        axes.set_ylabel("Lick port latency (sec)", label_kwargs)

        #HISTOGRAM ACCURACY VG vs WM
        axes = plt.subplot2grid((50, 50), (35, 40), rowspan=15, colspan=10)

        axes.bar((0.75, 1.75), color=other_c, height=(vg_accuracy_mean, wm_accuracy_mean))
        axes.hlines(y=[0.5, 1], xmin=0, xmax=2.5, color=lines_c, linestyle=':')
        axes.fill_between((0, 2.5), chance_prob, 0, facecolor=lines_c, alpha=0.4)

        axes.set_ylim(0, 1)
        axes.set_yticks(list(np.arange(0, 1.1, 0.1)))
        axes.set_yticklabels(['0', '', '', '', '', '50', '', '', '', '', '100'])
        axes.set_xticks([0.75, 1.75])
        axes.set_xticklabels(['Visually\nGuided', 'WM\nGuided'])
        axes.set_ylabel("Accuracy (%)", label_kwargs)

        # SAVING AND CLOSING PAGE 1
        pdf.savefig()
        plt.close()

        # PAGE 2:
        plt.figure(figsize=(8.3, 11.7))  # A4 vertical

        # X HISTOGRAM
        axes = plt.subplot2grid((50, 50), (0, 0), rowspan=4, colspan=50)

        bins = np.arange(-300, 301, 5)

        axes.hist(clean_responses_df.x_distances, bins=bins, histtype='step', color=other_c)

        axes.set_xticklabels([])
        yticks = (axes.get_yticks())
        axes.set_yticks(yticks[yticks != 0])  # delete tick y=0 to avoid overlaping with next plot
        axes.set_ylabel('Touches', label_kwargs)

        # X,Y HISTOGRAM
        axes = plt.subplot2grid((50, 50), (4, 0), rowspan=7, colspan=50)

        h = axes.hist2d(clean_responses_df.x_distances, clean_responses_df.y_distances, bins=bins)
        rew_window = mpatches.Circle((0, 0), params.threshold/2, facecolor='None',
                                     edgecolor=threshold_c, lw=1, zorder=9)
        stimulus = mpatches.Circle((0, 0), params.diameter/2, facecolor='None',
                                   edgecolor=diameter_c, lw=1, zorder=9)
        axes.add_patch(stimulus)
        axes.add_patch(rew_window)

        axes.tick_params(top=True, right=True)
        axes.set_ylim(-25, 90)
        axes.set_yticks([-25, 0, 25, 50, 75])
        axes.set_ylabel('Distance y (mm)',label_kwargs)

        colors = [diameter_c, threshold_c]
        labels = ['Stimulus', 'Correct Window']
        lines = [Line2D([0], [0], color=c) for c in colors]
        axes.legend(lines, labels, fontsize=6)

        # horizontal colorbar
        axes = plt.subplot2grid((50, 50), (11, 35), rowspan=2, colspan=10)
        axes.spines['bottom'].set_visible(False)
        axes.spines['left'].set_visible(False)
        axes.set_xticks([])
        axes.set_yticks([])
        plt.colorbar(h[3], orientation='horizontal')

        # TRIALS VS DISTANCE
        axes = plt.subplot2grid((50, 50), (13, 0), rowspan=25, colspan=50)

        axes.scatter(correct_responses_wm_first_df.x_distances, correct_responses_wm_first_df.trial, s=3,
                     marker='^', c=first_correct_c)
        axes.scatter(correct_responses_vg_first_df.x_distances, correct_responses_vg_first_df.trial, s=3,
                     marker='o', c=first_correct_c)

        axes.scatter(correct_responses_wm_other_df.x_distances, correct_responses_wm_other_df.trial, s=3,
                     marker='^', c=other_correct_c)
        axes.scatter(correct_responses_vg_other_df.x_distances, correct_responses_vg_other_df.trial, s=3,
                     marker='o', c=other_correct_c)

        axes.scatter(correct_responses_wm_none_df.x_distances, correct_responses_wm_none_df.trial, s=3,
                     marker='^', c=correct_responses_wm_none_df.numbers, cmap='OrRd_r')
        axes.scatter(correct_responses_vg_none_df.x_distances, correct_responses_vg_none_df.trial, s=3,
                     marker='o', c=correct_responses_vg_none_df.numbers, cmap='OrRd_r')

        axes.hlines(y=df.trial, xmin=-150, xmax=150, color=lines_c, linewidth=0.2, linestyle=':', zorder=0)
        axes.vlines(x=[-params.threshold/2, params.threshold/2], ymin=0, ymax=total_trials + 1,
                    color=threshold_c, zorder=0)
        axes.vlines(x=[-params.diameter / 2, params.diameter/ 2], ymin=0, ymax=total_trials + 1,
                    color=diameter_c, zorder=0)

        axes.set_xlim(-150, 150)
        axes.set_xlabel('Distance x (mm)', label_kwargs)
        axes.set_ylabel('Trials', label_kwargs)

        colors = [first_correct_c, incorrect_c, diameter_c, threshold_c, axes_c, axes_c]
        widths = [0, 0, 1, 1, 0, 0]
        markers = ['s', 's', ',', ',', 'o', '^']
        labels = ['Correct', 'Incorrect', 'Stimulus x', 'Correct window x', 'Visually guided', "WM guided"]
        lines = [Line2D([0], [0], color=colors[i], marker=markers[i], markersize=6,
                        markerfacecolor=colors[i], linewidth=widths[i]) for i in range(len(colors))]
        axes.legend(lines, labels, fontsize=6)

        # PERFORMANCE VS STIMULUS POSITION
        axes = plt.subplot2grid((50, 50), (40, 0), rowspan=10, colspan=23)

        bin0 = df[df.x_psycho <= 80]
        bin1 = df[(df.x_psycho > 80) & (df.x_psycho <= 160)]
        bin2 = df[(df.x_psycho > 160) & (df.x_psycho <= 240)]
        bin3 = df[(df.x_psycho > 240) & (df.x_psycho <= 320)]
        bin4 = df[df.x_psycho > 320]

        bins = [bin0, bin1, bin2, bin3, bin4]

        performance_first_bins = [(b.trial_result == 1).mean() for b in bins]
        performance_first_second_bins = [(b.trial_result.isin([1, 2])).mean() for b in bins]
        performance_all_bins = [(b.trial_result > 0).mean() for b in bins]

        performance_first_bins_std = [(b.trial_result == 1).std() for b in bins]
        performance_first_second_bins_std = [(b.trial_result.isin([1, 2])).std() for b in bins]
        performance_all_bins_std = [(b.trial_result > 0).std() for b in bins]

        bins = [0, 1, 2, 3, 4]
        bins0 = [x - 0.1 for x in bins]
        bins1 = [x + 0.1 for x in bins]

        bin_names = ['0-80', '81-160', '161-240', '241-320', '321-400']

        axes.errorbar(bins0, performance_first_bins, yerr=performance_first_bins_std,
                      marker='o', markersize=6, color=first_poke_c)
        axes.errorbar(bins, performance_first_second_bins, yerr=performance_first_second_bins_std,
                      marker='o', markersize=6, color=second_poke_c)
        axes.errorbar(bins1, performance_all_bins, yerr=performance_all_bins_std,
                      marker='o', markersize=6, color=all_poke_c)
        axes.hlines(y=[0.5, 1], xmin=0, xmax=4.3, color=lines_c, linestyle=':')
        axes.fill_between((-0.3, 4.3), chance_prob, 0, facecolor=lines_c, alpha=0.2)

        axes.set_xticks(bins)
        axes.set_xticklabels(bin_names)
        axes.set_ylim(0, 1.10)
        axes.set_yticks(list(np.arange(0, 1.1, 0.1)))
        axes.set_yticklabels(['0', '', '', '', '', '50', '', '', '', '', '100'])
        axes.set_xlabel('Stimulus position (mm)', label_kwargs)
        axes.set_ylabel('Performance (%)', label_kwargs)

        colors = [first_poke_c, second_poke_c, all_poke_c]
        labels = ['First pokes', 'First and second pokes', 'All pokes']
        lines = [Line2D([0], [0], color=c, marker='o', markersize=6, markerfacecolor=c) for c in colors]
        axes.legend(lines, labels, fontsize=6, loc = 'upper right', bbox_to_anchor = (1.1, 1))

        # ERROR VS STIMULUS POSITION
        axes = plt.subplot2grid((50, 50), (40, 30), rowspan=5, colspan=20)

        bin0 = responses_df[responses_df.x_psycho <= 80]
        bin1 = responses_df[(responses_df.x_psycho > 80) & (responses_df.x_psycho <= 160)]
        bin2 = responses_df[(responses_df.x_psycho > 160) & (responses_df.x_psycho <= 240)]
        bin3 = responses_df[(responses_df.x_psycho > 240) & (responses_df.x_psycho <= 320)]
        bin4 = responses_df[responses_df.x_psycho > 320]

        bins_values = [bin0, bin1, bin2, bin3, bin4]
        bins = [0, 1, 2, 3, 4]

        error_means = [b.x_distances.mean() for b in bins_values]
        error_stds = [b.x_distances.std() for b in bins_values]

        axes.errorbar(bins, error_means, yerr=error_stds, marker='o', markersize=6, color=all_poke_c)
        axes.hlines(y=0, xmin=0, xmax=len(bins)-1, color=lines_c, linestyle=':')

        axes.set_ylabel('Distance x (mm)', {'fontsize': 7})
        axes.set_xticklabels('')
        axes.set_xticks(bins)
        axes.set_xticklabels(bin_names)

        colors = [all_poke_c, lines_c, vg_c, wm_c]
        labels = ['All responses', 'All first pokes', 'VG trials', 'WM trials']
        lines = [Line2D([0], [0], color=c, marker='o', markersize=3) for c in colors]
        axes.legend(lines, labels, fontsize=6, loc = 'upper right', bbox_to_anchor = (1.1, 1.1))

        # ERROR VS STIMULUS POSITION IN VG vs WM TRIALS
        axes = plt.subplot2grid((50, 50), (45, 30), rowspan=5, colspan=20)

        bin0 = vg_df[vg_df.x_psycho <= 80]
        bin1 = vg_df[(vg_df.x_psycho > 80) & (vg_df.x_psycho <= 160)]
        bin2 = vg_df[(vg_df.x_psycho > 160) & (vg_df.x_psycho <= 240)]
        bin3 = vg_df[(vg_df.x_psycho > 240) & (vg_df.x_psycho <= 320)]
        bin4 = vg_df[vg_df.x_psycho > 320]

        bin00 = wm_df[wm_df.x_psycho <= 80]
        bin10 = wm_df[(wm_df.x_psycho > 80) & (wm_df.x_psycho <= 160)]
        bin20 = wm_df[(wm_df.x_psycho > 160) & (wm_df.x_psycho <= 240)]
        bin30 = wm_df[(wm_df.x_psycho > 240) & (wm_df.x_psycho <= 320)]
        bin40 = wm_df[wm_df.x_psycho > 320]

        bin000 = first_responses_df[first_responses_df.x_psycho <= 80]
        bin100 = first_responses_df[(first_responses_df.x_psycho > 80) & (first_responses_df.x_psycho <= 160)]
        bin200 = first_responses_df[(first_responses_df.x_psycho > 160) & (first_responses_df.x_psycho <= 240)]
        bin300 = first_responses_df[(first_responses_df.x_psycho > 240) & (first_responses_df.x_psycho <= 320)]
        bin400 = first_responses_df[first_responses_df.x_psycho > 320]

        bins_VG = [bin0, bin1, bin2, bin3, bin4]
        bins_WM = [bin00, bin10, bin20, bin30, bin40]
        bins_all = [bin000, bin100, bin200, bin300, bin400]

        error_means_VG = [b.x_distances.mean() for b in bins_VG]
        error_stds_VG = [b.x_distances.std() for b in bins_VG]
        error_means_WM = [b.x_distances.mean() for b in bins_WM]
        error_stds_WM = [b.x_distances.mean() for b in bins_WM]
        error_means_all = [b.x_distances.mean() for b in bins_all]
        error_stds_all = [b.x_distances.std() for b in bins_all]

        axes.errorbar(bins, error_means_all, yerr=error_stds_all, marker='o', markersize=6, color=lines_c)
        axes.errorbar(bins, error_means_VG, yerr=error_stds_VG, marker='o', markersize=6, color=vg_c)
        axes.errorbar(bins, error_means_WM, yerr=error_stds_WM, marker='o', markersize=6, color=wm_c)
        axes.hlines(y=0, xmin=0, xmax=len(bins) - 1, color=lines_c, linestyle=':')

        axes.set_xlabel('Stimulus position (mm)', label_kwargs)
        axes.set_ylabel('Distance x (mm)', {'fontsize': 7})
        axes.set_xticks(bins)
        axes.set_xticklabels(bin_names)

        # SAVING AND CLOSING PAGE 2
        pdf.savefig()
        plt.close()

        # PAGE 3:
        if params.alpha_changes:
            plt.figure(figsize=(8.3, 11.7))  # A4 vertical

            # ALPHA PLOT
            axes = plt.subplot2grid((50, 50), (0, 0), rowspan=4, colspan=45)

            axes.plot(df.alpha_type, c=all_poke_c, linewidth=0, marker='o', markersize=1)
            axes.hlines(y=[1, 2, 3], xmin=0, xmax=total_trials, color=[easy_alpha, med_alpha, dif_alpha],
                        linestyle='solid')

            axes.set_xlim([-1, total_trials + 1])
            axes.set_yticks([1, 2, 3])
            axes.set_ylim(0.5, 3.5)
            axes.set_yticklabels(['Alpha easy', 'Alpha med', 'Alpha dif'])

            ## legend
            colors = [easy_alpha, med_alpha, dif_alpha]
            labels = ['Easy alpha', 'Medium alpha', 'Difficult alpha']
            lines = [Line2D([0], [0], color=c, marker='o', markersize=6, markerfacecolor=c) for c in colors]
            axes.legend(lines, labels, fontsize=6, loc='center left', bbox_to_anchor=(1, 0.5))

            # ACCURACY EACH ALPHA ACROSS SESSION
            axes = plt.subplot2grid((50, 50), (5, 0), rowspan=6, colspan=45)

            axes.plot(alpha1_valids_df.trial, alpha1_valids_df.accuracy_alpha1_rw, marker='o', markersize=1, color=easy_alpha)
            axes.plot(alpha2_valids_df.trial, alpha2_valids_df.accuracy_alpha2_rw, marker='o', markersize=1, color=med_alpha)
            axes.plot(alpha3_valids_df.trial, alpha3_valids_df.accuracy_alpha3_rw, marker='o', markersize=1, color=dif_alpha)
            axes.hlines(y=[0.5, 1], xmin=0, xmax=total_trials, color=lines_c, linestyle = ':')
            axes.fill_between(alpha1_valids_df.trial, chance_prob, -0.1, facecolor=lines_c, alpha=0.2)

            axes.set_xlim([1, total_trials + 1])
            axes.set_ylim(-0.1, 1.10)
            axes.set_yticks(list(np.arange(0, 1.1, 0.1)))
            axes.set_yticklabels(['0', '', '', '', '', '50', '', '', '', '', '100'])
            axes.set_ylabel('Accuracy (%)', label_kwargs)

            # % VALIDS EACH ALPHA ACROSS SESSION
            axes = plt.subplot2grid((50, 50), (12, 0), rowspan=6, colspan=45)

            axes.plot(alpha1_df.trial, alpha1_df.alpha1_valids_rw, marker='o', markersize=1, color=easy_alpha)
            axes.plot(alpha2_df.trial, alpha2_df.alpha2_valids_rw, marker='o', markersize=1, color=med_alpha)
            axes.plot(alpha3_df.trial, alpha3_df.alpha3_valids_rw, marker='o', markersize=1, color=dif_alpha)
            axes.hlines(y=[0.5, 1], xmin=0, xmax=total_trials, color=lines_c, linestyle=':')

            axes.set_xlim([1, total_trials + 1])
            axes.set_xlabel('Trials', label_kwargs)
            axes.set_ylim(-0.1, 1.10)
            axes.set_yticks(list(np.arange(0, 1.1, 0.1)))
            axes.set_yticklabels(['0', '', '', '', '', '50', '', '', '', '', '100'])
            axes.set_ylabel('Valids (%)', label_kwargs)

            # ACCURACY ALPHA TYPE
            axes = plt.subplot2grid((50, 50), (20, 0), rowspan=12, colspan=22)

            first_poke = [alpha1_acc_first_poke_alls, alpha2_acc_first_poke_alls, alpha3_acc_first_poke_alls]
            second_poke = [alpha1_acc_first_second_poke_alls, alpha2_acc_first_second_poke_alls,
                           alpha3_acc_first_second_poke_alls]
            all_poke = [alpha1_acc_all_poke_alls, alpha2_acc_all_poke_alls, alpha3_acc_all_poke_alls]

            axes.set_ylim(0, 1.10)
            x_first_poke = [0, 1, 2]
            x_second_poke = [0.1, 1.1, 2.1]
            x_all_poke = [0.2, 1.2, 2.2]
            axes.plot(x_first_poke, first_poke, color=first_poke_c, marker='o', linewidth=1, markersize=6)
            axes.plot(x_second_poke, second_poke, color=second_poke_c, marker='o', linewidth=1, markersize=6)
            axes.plot(x_all_poke, all_poke, color=all_poke_c, marker='o', linewidth=1, markersize=6)
            axes.hlines(y=[0.5, 1], xmin=0, xmax=2.4, color=lines_c, linestyle=':')
            axes.fill_between((0, 2.4), chance_prob, 0, facecolor=lines_c, alpha=0.2)

            axes.set_yticks(list(np.arange(0, 1.1, 0.1)))
            axes.set_yticklabels(['0', '', '', '', '', '50', '', '', '', '', '100'])
            axes.set_xticks([0.1, 1.1, 2.1])
            axes.set_xticklabels(['Alpha easy', 'Alpha medium', 'Alpha difficult'])
            axes.set_ylabel('Accuracy (%)', label_kwargs)

            colors = [first_poke_c, second_poke_c, all_poke_c]
            labels = ['First pokes', 'First and second pokes', 'All pokes']
            lines = [Line2D([0], [0], color=c, marker='o', markersize=6, markerfacecolor=c) for c in colors]
            axes.legend(lines, labels, fontsize=6, loc='upper right', bbox_to_anchor=(1.1, 0.9))

            # VALID(%) DEPENDING ON ALPHA TYPE
            axes = plt.subplot2grid((50, 50), (20, 28), rowspan=12, colspan=22)

            valids_percent_alphas = [valids_percent_alpha1_sum, valids_percent_alpha2_sum, valids_percent_alpha3_sum]
            x_positions = [0, 1, 2]

            axes.plot(x_positions, valids_percent_alphas, color=all_poke_c, marker='o', linewidth=1, markersize=6)
            axes.hlines(y=[0.5, 1], xmin=0, xmax=2.4, color=lines_c, linestyle=':')
            axes.fill_between((0, 2.4), chance_prob, 0, facecolor=lines_c, alpha=0.2)

            axes.set_ylim(0, 1.10)
            axes.set_yticks(list(np.arange(0, 1.1, 0.1)))
            axes.set_yticklabels(['0', '', '', '', '', '50', '', '', '', '', '100'])
            axes.set_xticks([0.1, 1.1, 2.1])
            axes.set_xticklabels(['Alpha easy', 'Alpha medium', 'Alpha difficult'])
            axes.set_ylabel('Valids (%)', label_kwargs)

            # ACCURACY DEPENDING ON TIME STIMULUS OFF
            axes = plt.subplot2grid((50, 50), (34, 0), rowspan=15, colspan=22)

            x = first_responses_df.first_responses_stimoff_time

            conditions = [x <= 0, ((0 < x) & (x <= 0.5)), ((0.5 < x) & (x <= 1)), ((1 < x) & (x <= 1.5)),
                          ((1.5 < x) & (x <= 2)), ((2 < x) & (x <= 2.5)), ((2.5 < x) & (x <= 3)), ((3 < x) & (x <= 3.5)),
                          ((3.5 < x) & (x <= 4)), ((4 < x) & (x <= 4.5)), ((4 < x) & (x <= 5)), x > 5]
            choices = ['VG', '0.5', '1', '1.5', '2', '2.5', '3', '3.5', '4', '4.5', '5', '>5']

            first_responses_df['bins'] = np.select(conditions, choices)

            sns.pointplot(x=first_responses_df.bins, y=first_responses_df.correct_first_poke_bool,
                          color=all_poke_c, ax=axes, ci=None, capsize=.2,
                          order=('VG', '0.5', '1', '1.5', '2', '2.5', '3', '3.5', '4', '4.5', '5', '>5'))
            axes.hlines(y=[0.5, 1], xmin=0, xmax=len(choices), color=lines_c, linestyle=':')
            axes.fill_between((0, len(choices)), chance_prob, 0, facecolor=lines_c, alpha=0.2)

            axes.set_ylim([0, 1.1])
            axes.set_yticks(list(np.arange(0, 1.1, 0.1)))
            axes.set_yticklabels(['0', '', '', '', '', '50', '', '', '', '', '100'])
            axes.set_ylabel('Accuracy (%)', label_kwargs)
            axes.set_xlabel('Response time after stimulus offset (sec)', label_kwargs)

            # plot the ammount of trials per bin
            relevances = []
            for i in choices:
                bin = []
                for trial in first_responses_df.bins:
                    if trial == i:
                        bin.append(1)
                relevance_bin = len(bin) / len(first_responses_df.bins)
                relevances.append(relevance_bin)

            sns.pointplot(x=choices, y=relevances, color=lines_c, ci=None, linestyles='--',
                          order=('VG', '0.5', '1', '1.5', '2', '2.5', '3', '3.5', '4', '4.5', '5', '>5'))

            labels = ['responses/bin (%)']
            lines = [Line2D([0], [0], color=lines_c, marker='o', markersize=3, markerfacecolor=lines_c)]
            axes.legend(lines, labels, fontsize=6, loc='upper right', bbox_to_anchor=(1.1, 0.9))

            # RESPONSE TIME LATENCIES DEPENDING ON ALPHA TYPE
            axes = plt.subplot2grid((50, 50), (34, 28), rowspan=15, colspan=22)

            elements = [alpha1_correct_pokes_time, alpha1_incorrect_pokes_time, alpha2_correct_pokes_time,
                        alpha2_incorrect_pokes_time, alpha3_correct_pokes_time, alpha3_incorrect_pokes_time]

            for i in range(6):
                if i == 0 or i == 2 or i == 4:
                    color = first_correct_c
                else:
                    color = incorrect_c
                y = elements[i]
                x = np.random.normal(1 + i, 0.06, size=len(y))
                box = axes.boxplot(elements, showfliers=False)
                for _, line_list in box.items():
                    for line in line_list:
                        line.set(color = lines_c, linewidth = 0.5)
                axes.plot(x, y, 'r.', alpha=0.8, color=color, markersize=1, zorder=10)

            axes.set_ylim(0, 10)
            axes.set_xticks([1.5, 3.5, 5.5])
            axes.set_xticklabels(['Alpha easy', 'Alpha medium', 'Alpha difficult'])
            axes.set_ylabel("Response time (sec)", label_kwargs)

            colors = [first_correct_c, incorrect_c]
            labels = ['Correct', 'Incorrect']
            lines = [Line2D([0], [0], color=c, marker='o', markersize=6, markerfacecolor=c) for c in colors]
            axes.legend(lines, labels, fontsize=6, loc='upper right', bbox_to_anchor=(1.1, 1))

            # SAVING AND CLOSING PAGE 3
            pdf.savefig()
            plt.close()

    # RETURN NEW DATA FOR GENERAL PARAMS
    params['distance_mean'] = distance_mean
    params['x_distance_mean'] = x_distance_mean
    params['total_trials'] = total_trials
    params['valid_trials'] = valid_trials_sum
    params['missed_trials'] = missed_trials_sum
    params['correct_first_poke'] = correct_first_poke_sum
    params['accuracy_first_poke'] = accuracy_first_poke_alls
    params['accuracy_first_second_poke'] = accuracy_first_second_poke_alls
    params['accuracy_all_poke'] = accuracy_all_poke_alls
    params['performance_first_poke'] = performance_first_poke_alls
    params['performance_first_second_poke'] = performance_first_second_poke_alls
    params['performance_all_poke'] = performance_all_poke_alls
    params['vg_accuracy'] = vg_accuracy_mean
    params['wm_accuracy'] = wm_accuracy_mean
    params['accuracy_alpha1'] = accuracy_alpha1_alls
    params['accuracy_alpha2'] = accuracy_alpha2_alls
    params['accuracy_alpha3'] = accuracy_alpha3_alls
    params['performance_alpha1'] = performance_alpha1_alls
    params['performance_alpha2'] = performance_alpha2_alls
    params['performance_alpha3'] = performance_alpha3_alls
    params['valids_percent_alpha1'] = valids_percent_alpha1_sum
    params['valids_percent_alpha2'] = valids_percent_alpha2_sum
    params['valids_percent_alpha3'] = valids_percent_alpha3_sum
    params['correct_first_latencies_mean'] = correct_first_latencies_mean
    params['correct_other_latencies_mean'] = correct_other_latencies_mean
    params['incorrect_latencies_mean'] = incorrect_latencies_mean
    params['correct_first_lick_mean'] = correct_first_lick_mean
    params['correct_other_lick_mean'] = correct_other_lick_mean
    params['incorrect_lick_mean'] = incorrect_lick_mean
    params['fixation_breaks_mean'] = fixation_breaks_mean
    params['correct_first_latencies_std'] = correct_first_latencies_std
    params['correct_other_latencies_std'] = correct_other_latencies_std
    params['incorrect_latencies_std'] = incorrect_latencies_std
    params['correct_first_lick_std'] = correct_first_lick_std
    params['correct_other_lick_std'] = correct_other_lick_std
    params['incorrect_lick_std'] = incorrect_lick_std
    params['fixation_breaks_std'] = fixation_breaks_std
    params['distance_std'] = distance_std
    params['x_distance_std'] = x_distance_std
    params['correct_first_latencies_median'] = correct_first_latencies_median
    params['correct_other_latencies_median'] = correct_other_latencies_median
    params['incorrect_latencies_median'] = incorrect_latencies_median
    params['correct_first_lick_median'] = correct_first_lick_median
    params['correct_other_lick_median'] = correct_other_lick_median
    params['incorrect_lick_median'] = incorrect_lick_median
    params['fixation_breaks_median'] = fixation_breaks_median
    params['distance_median'] = distance_median
    params['x_distance_median'] = x_distance_median
    params['distance_median'] = distance_median
    params['x_distance_median'] = x_distance_median
    params['alpha1_correct_pokes_time_mean'] = alpha1_correct_pokes_time_mean
    params['alpha1_incorrect_pokes_time_mean'] = alpha1_incorrect_pokes_time_mean
    params['alpha2_correct_pokes_time_mean'] = alpha2_correct_pokes_time_mean
    params['alpha2_incorrect_pokes_time_mean'] = alpha2_incorrect_pokes_time_mean
    params['alpha3_correct_pokes_time_mean'] = alpha3_correct_pokes_time_mean
    params['alpha3_incorrect_pokes_time_mean'] = alpha3_incorrect_pokes_time_mean
    params['alpha1_acc_first_poke_alls'] = alpha1_acc_first_poke_alls
    params['alpha1_acc_first_second_poke_alls'] = alpha1_acc_first_second_poke_alls
    params['alpha1_acc_all_poke_alls'] = alpha1_acc_all_poke_alls
    params['alpha2_acc_first_poke_alls'] = alpha2_acc_first_poke_alls
    params['alpha2_acc_first_second_poke_alls'] = alpha2_acc_first_second_poke_alls
    params['alpha2_acc_all_poke_alls'] = alpha2_acc_all_poke_alls
    params['alpha3_acc_first_poke_alls'] = alpha3_acc_first_poke_alls
    params['alpha3_acc_first_second_poke_alls'] = alpha3_acc_first_second_poke_alls
    params['alpha3_acc_all_poke_alls'] = alpha3_acc_all_poke_alls

    return params

   # RETURN NEW DATA FOR GENERAL TRIALS

    # trials[''] =
    #
    # return trials