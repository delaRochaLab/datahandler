def wmfm_glue_sessions():
    # check this variables are the same as a condition to glue the sessions
    variables_to_check = { }
    # when having several sessions in the same day, for some parameters we simply select the ones of the first session

    # other parameters are the sum of all the sessions
    params_sum = {'water_drunk', 'total_trials'}

    # columns in trials df that are concatenate directly

    # columns in trials df to which we add total_duration
    trials_plus_total_duration = { }

    # columns in trials df to which we add total_trials
    trials_plus_total_trials = {'trial'}

    return variables_to_check, params_sum, trials_plus_total_duration, trials_plus_total_trials
