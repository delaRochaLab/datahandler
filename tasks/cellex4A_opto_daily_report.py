import os
import matplotlib as mpl
import matplotlib.pyplot as plt
import seaborn as sns
import numpy as np
import pandas as pd
from matplotlib.backends.backend_pdf import PdfPages
from matplotlib.lines import Line2D
from datahandler import Utils
import logging
import warnings
if os.environ.get('DISPLAY', '') == '':
    mpl.use('Agg')


# LOGGING AND WARNINGS
logger = logging.getLogger(__name__)
warnings.simplefilter(action='ignore', category=UserWarning)
warnings.simplefilter(action='ignore', category=RuntimeWarning)
warnings.simplefilter(action='ignore', category=FutureWarning)

# PLOT PARAMETERS
mpl.rcParams['lines.linewidth'] = 0.7
mpl.rcParams['lines.markersize'] = 1
mpl.rcParams['font.size'] = 7
mpl.rcParams['axes.spines.right'] = False
mpl.rcParams['axes.spines.top'] = False
label_kwargs = {'fontsize': 9, 'fontweight': 'roman'}


def cellex4A_opto_daily_report(trials_path, params_path, daily_report_path):
    """
    Creates a daily report with the performance and psychometric plots, if necessary.
    We use a dataframe with one row per trial: df.
    And a pandas series: params.
    At the end, we need to return params including the new values that we have calculated.
    """

    # READ THE CSVS
    # trials_path contains a csv with one row per trial, we read it as a dataframe
    # we need to set the index of the df using the column containing the number of trial
    # this is necessary if we want to unnest columns
    # when reading the df the columns that contain lists are read as string, transform them to lists

    try:
        df = pd.read_csv(trials_path, sep=';')
    except FileNotFoundError:
        logger.critical("Error: Reading trials CSV file. Exiting...")
        raise

    try:
        params = pd.read_csv(params_path, sep=';')
    except FileNotFoundError:
        logger.critical("Error: Reading params CSV file. Exiting...")
        raise

    # make index = trial and convert strings to lists
    df.index = df.trial
    df = Utils.convert_strings_to_lists(df, ['pre_stim_delay_start', 'pre_stim_delay_end',
                                             'l_poke_start', 'l_poke_end',
                                             'r_poke_start', 'r_poke_end',
                                             'c_poke_start', 'c_poke_end',
                                             'envelope_diff', 'early_withdrawal_pre',
                                             'stimulus_right', 'stimulus_left',
                                             'wait_cpoke_start', 'wait_cpoke_end'])

    params = Utils.convert_strings_to_lists(params, ['coherence_vector'])

    # transform params df that only contains one row in a Series object
    params = params.iloc[0]

    # LIGHT_OFF and LIGHT_ON DFs
    df_off = df[df.light_off == 1]
    df_on = df[df.light_on == 1]

    if df_off.size == 0:
        light_on_off = False
        df_off = df
        df_on = df
        df.light_off = 1
    else:
        light_on_off = True

    # NEW USEFUL COLUMNS
    df['reaction_time'] = df.play_target_end - df.play_target_start
    df['response_time'] = df.wait_apoke_end - df.wait_apoke_start
    df['sound_dur'] = df.play_target_end - df.play_target_start

    # EXPANDED DATAFRAMES
    fixations_df = Utils.unnesting(df, ['pre_stim_delay_start', 'pre_stim_delay_end'])
    pokes_df_off = Utils.unnesting(df_off, ['c_poke_start', 'c_poke_end'])
    pokes_df_on = Utils.unnesting(df_on, ['c_poke_start', 'c_poke_end'])

    # useful columns in expanded dataframs
    fixations_df['fixation_time'] = fixations_df.pre_stim_delay_end - fixations_df.pre_stim_delay_start
    pokes_df_off['poke_time'] = pokes_df_off.c_poke_end - pokes_df_off.c_poke_start
    pokes_df_on['poke_time'] = pokes_df_on.c_poke_end - pokes_df_on.c_poke_start

    df['previousResponse'] = df.response.shift(1)
    df['previousHit'] = df.hithistory.shift(1)
    df['error'] = 1 - df.hithistory
    df['reward_side2'] = df.reward_side - 1

    # SUB-DATAFRAMES
    left_trials_df = df[df.reward_side == 1]
    right_trials_df = df[df.reward_side == 2]
    valid_trials_df = df[df.hithistory != -3]
    valid_trials_df_off = valid_trials_df[valid_trials_df.light_off == 1]
    valid_trials_df_on = valid_trials_df[valid_trials_df.light_on == 1]

    left_valid_trials_df = valid_trials_df[valid_trials_df.reward_side == 1]
    right_valid_trials_df = valid_trials_df[valid_trials_df.reward_side == 2]

    # repeat_trials_df = valid_trials_df[(valid_trials_df.reward_side2 == valid_trials_df.previousResponse) & (valid_trials_df.previousHit == 1)]
    # alternate_trials_df = valid_trials_df[(valid_trials_df.reward_side2 != valid_trials_df.previousResponse) & (valid_trials_df.previousHit == 1)]

    # repeat_trials_df = valid_trials_df[(valid_trials_df.reward_side2 == valid_trials_df.previousResponse)]
    # alternate_trials_df = valid_trials_df[(valid_trials_df.reward_side2 != valid_trials_df.previousResponse)]

    repeat_trials_df = valid_trials_df[valid_trials_df.response == valid_trials_df.previousResponse]
    alternate_trials_df = valid_trials_df[
        (valid_trials_df.response != valid_trials_df.previousResponse) & ~(pd.isnull(valid_trials_df.previousResponse))]

    # USEFUL VALUES
    number_of_trials = df.shape[0]

    per_corrects = params.correct_trials / params.valid_trials
    per_invalids = params.invalid_trials / params.valid_trials

    no_outliers = valid_trials_df[(valid_trials_df.response_time > 0) & (valid_trials_df.response_time < 1)]
    try:
        response_time_mean = int(no_outliers.response_time.mean() * 1000)
    except:
        response_time_mean = np.nan
    try:
        reaction_time_mean = int(valid_trials_df.reaction_time.mean() * 1000)
    except:
        reaction_time_mean = np.nan

    corrects_left = left_trials_df[left_trials_df.hithistory == 1].shape[0]
    total_left = left_valid_trials_df.shape[0]
    per_left = corrects_left / total_left if total_left != 0 else 0

    corrects_right = right_trials_df[right_trials_df.hithistory == 1].shape[0]
    total_right = right_valid_trials_df.shape[0]
    per_right = corrects_right / total_right if total_right != 0 else 0

    micropokes = fixations_df[fixations_df.fixation_time < 0.299].shape[0]
    per_micropokes = micropokes / params.valid_trials

    silent_fail = df.sound_fail.sum()

    with PdfPages(daily_report_path) as pdf:
        plt.figure(figsize=(11.7, 11.7))

        # FIRST PLOT: ROLLING AVERAGES OF PERFORMANCES
        axes = plt.subplot2grid((140, 100), (0, 0), rowspan=40, colspan=100)

        ra_total = Utils.compute_window(valid_trials_df.hithistory, 20)
        ra_right = Utils.compute_window(right_valid_trials_df.hithistory, 20)
        ra_left = Utils.compute_window(left_valid_trials_df.hithistory, 20)

        ra_repeat = Utils.compute_window(repeat_trials_df.error, 20)
        ra_alternate = Utils.compute_window(alternate_trials_df.error, 20)

        axes.plot(valid_trials_df.trial, ra_total, marker='o', color='black', markersize=2, linewidth=1.1)
        axes.plot(left_valid_trials_df.trial, ra_left, marker='o', color='cyan', markersize=2, linewidth=1.1)
        axes.plot(right_valid_trials_df.trial, ra_right, marker='o', color='magenta', markersize=2, linewidth=1.1)

        axes.plot(repeat_trials_df.trial, ra_repeat, marker='o', color='red', markersize=2, linewidth=1.1)
        axes.plot(alternate_trials_df.trial, ra_alternate, marker='o', color='blue', markersize=2, linewidth=1.1)

        axes.set_ylim([0, 1.1])
        axes.set_yticks(list(np.arange(0, 1.1, 0.1)))
        axes.set_yticklabels(['0', '', '', '', '', '50', '', '', '', '', '100'])

        axes.set_xlim([1, number_of_trials + 1])
        axes.set_ylabel('Accuracy [%]')
        axes.set_xlabel('')
        axes.yaxis.set_tick_params()
        axes.xaxis.set_tick_params()

        legend_elements = [Line2D([0], [0], color='black', label='Total accuracy'),
                           Line2D([0], [0], color='cyan', label='Left accuracy'),
                           Line2D([0], [0], color='magenta', label='Right accuracy'),
                           Line2D([0], [0], color='red', label='Rep errors'),
                           Line2D([0], [0], color='blue', label='Alt errors'),
                           ]

        axes.legend(loc="lower right", bbox_to_anchor=(1.0, 1.0), handles=legend_elements, ncol=1, prop={'size': 8})

        # UPPER TEXT
        s1 = ('Date: ' + str(params.day) + ' ' + str(params.time) + '\n')
        s2 = ('Subject name: ' + str(params.subject_name) +
              ' / Box name: ' + str(params.box) +
              '\n')
        s3 = ('Valid trials: ' + str(params.valid_trials) +
              ' / Accuracy: ' + str(round(per_corrects * 100, 1)) + ' %'
              ' / Corrects on left: ' + str(corrects_left) + ' (' + str(round(per_left * 100, 1)) + ' %)'
              ' / Corrects on right: ' + str(corrects_right) + ' (' + str(round(per_right * 100, 1)) + ' %)'
              ' / Invalid trials: ' + str(params.invalid_trials) + ' (' + str(round(per_invalids * 100, 1)) + ' %)' +
              '\n')
        s4 = ('Water: ' + str(params.water) + ' mL' +
              ' / Mean reaction time: ' + str(reaction_time_mean) + ' ms' +
              ' / Mean response time: ' + str(response_time_mean) + ' ms' +
              ' / Micropokes: ' + str(micropokes) + ' (' + str(round(per_micropokes * 100, 1)) + ' %)' +
              '\n')

        try:
            lights = df.light_on.sum()
            s5 = ('Trials with light: ' + str(lights))
            plt.text(0.1, 0.90, s1 + s2 + s3 + s4 + s5, fontsize=8, transform=plt.gcf().transFigure)
        except:
            plt.text(0.1, 0.90, s1 + s2 + s3 + s4, fontsize=8, transform=plt.gcf().transFigure)

        # SECOND PLOT: SCATTER PLOT
        axes = plt.subplot2grid((140, 100), (43, 0), rowspan=9, colspan=100)

        conditions = [df.hithistory == 0, df.hithistory == 1, df.hithistory == -3]
        choices = ['red', 'green', 'grey']

        df['color'] = np.select(conditions, choices)

        num_of_colors = len(df.color.unique())

        if num_of_colors == 1:
            palette = ['grey']
        elif num_of_colors == 2:
            palette = ['red', 'green']
        else:
            palette = ['grey', 'red', 'green']

        sns.scatterplot(df.trial, df.reward_side, hue=df.hithistory, palette=palette, s=5, ax=axes)

        axes.set_xlim([1, number_of_trials + 1])
        axes.set_ylim(0.2, 2.8)
        axes.set_yticks([0, 1])
        axes.set_yticks([0, 1])
        axes.set_yticklabels(['Left', 'Right'])
        axes.set_xlabel('Trials')
        axes.set_ylabel('')
        axes.get_legend().remove()

        # THIRD PLOT: CPOKE HISTOGRAM
        axes = plt.subplot2grid((140, 100), (63, 0), rowspan=37, colspan=23)

        pokes = pokes_df_off[(pokes_df_off.poke_time < 1) & (pokes_df_off.c_poke_start < pokes_df_off.play_target_start)].poke_time
        pokes *= 1000

        axes.hist(pokes, bins=50)

        axes.set_xlim([0, 900])
        axes.axvline(x=300, color='r', linestyle=':')
        axes.set_title("Center poke time OFF")
        axes.set_xlabel("Time [ms]")
        axes.yaxis.set_tick_params()
        axes.xaxis.set_tick_params()

        # FOURTH PLOT: CUMULATIVE TIME
        axes = plt.subplot2grid((140, 100), (63, 26), rowspan=37, colspan=23)

        axes.plot(valid_trials_df_off.trial, valid_trials_df_off.trial_start, linewidth=2)
        axes.set_title("Engagement OFF")
        axes.set_xlabel("Trials")
        axes.set_ylabel("Time [s]")

        # FIFTH PLOT: PSYCHOMETRIC CURVE CORRECTS
        axes = plt.subplot2grid((140, 100), (63, 53), rowspan=37, colspan=23)

        coherences = valid_trials_df_off.coherence
        response = valid_trials_df_off.response
        previousResponse = valid_trials_df_off.previousResponse

        # we need coherences from -1 to 1, response from 0 to 1
        coherences = coherences * 2 - 1
        coherences[abs(coherences) < 0.1] = 0

        psych_curve = Utils.compute_psych_curve(coherences, response)

        axes.plot([0, 0], [0, 1], 'k-', lw=1, linestyle=':')
        axes.plot([-1, 1], [0.5, 0.5], 'k-', lw=1, linestyle=':')

        axes.errorbar(psych_curve.xdata, psych_curve.ydata,
                      yerr=psych_curve.fit_error, fmt='ro', elinewidth=1, markersize=3)
        axes.plot(np.linspace(-1, 1, 30), psych_curve.fit, color='black', linewidth=1)
        axes.set_yticks(np.arange(0, 1.1, step=0.1))
        axes.set_xlabel('Evidence')
        axes.set_title('Prob right OFF')
        axes.set_xlim([-1.05, 1.05])
        axes.yaxis.set_tick_params(labelsize=9)
        axes.xaxis.set_tick_params(labelsize=9)
        axes.set_ylim([-0.05, 1.05])
        axes.tick_params(labelsize=9)
        axes.annotate(str(round(psych_curve.ydata[0], 2)), xy=(psych_curve.xdata[0], psych_curve.ydata[0]),
                      xytext=(psych_curve.xdata[0] - 0.03, psych_curve.ydata[0] + 0.05), fontsize=8)
        axes.annotate(str(round(psych_curve.ydata[-1], 2)), xy=(psych_curve.xdata[-1], psych_curve.ydata[-1]),
                      xytext=(psych_curve.xdata[-1] - 0.1, psych_curve.ydata[-1] - 0.08), fontsize=8)
        # s, b, lr_r, lr_l = psych_curve.params
        #
        # axes.annotate("S = " + str(round(s, 2)) + "\n" + "B = " +
        #               str(round(b, 2)) + "\n" + "LR_L = " +
        #               str(round(lr_r, 2)) + "\n" + "LR_R = " +
        #               str(round(lr_l, 2)), xy=(0, 0), xytext=(-1, 0.85), fontsize=7)

        # SIXTH PLOT: PSYCHOMETRIC CURVE REPEAT
        axes = plt.subplot2grid((140, 100), (63, 79), rowspan=37, colspan=21)

        prob_repeat = valid_trials_df_off.env_prob_repeat
        previousHit = valid_trials_df_off.previousHit

        repeatresponse_rep = []
        repeatresponse_alt = []
        repeatcoherence_rep = []
        repeatcoherence_alt = []

        previousResponseSigned = np.array(previousResponse) * 2 - 1

        for i in range(response.size):
            if i == 0:
                if prob_repeat.iloc[0] >= 0.5:
                    repeatresponse_rep.append(0)
                    repeatcoherence_rep.append(coherences.iloc[i])
                else:
                    repeatresponse_alt.append(0)
                    repeatcoherence_alt.append(coherences.iloc[i])
            elif response.iloc[i] == previousResponse.iloc[i]:
                if previousHit.iloc[i] == 1:
                    if prob_repeat.iloc[i] >= 0.5:
                        repeatresponse_rep.append(1)
                        repeatcoherence_rep.append(coherences.iloc[i] * previousResponseSigned[i])
                    else:
                        repeatresponse_alt.append(1)
                        repeatcoherence_alt.append(coherences.iloc[i] * previousResponseSigned[i])
            else:
                if previousHit.iloc[i] == 1:
                    if prob_repeat.iloc[i] >= 0.5:
                        repeatresponse_rep.append(0)
                        repeatcoherence_rep.append(coherences.iloc[i] * previousResponseSigned[i])
                    else:
                        repeatresponse_alt.append(0)
                        repeatcoherence_alt.append(coherences.iloc[i] * previousResponseSigned[i])

        psych_curve2_rep = Utils.compute_psych_curve(repeatcoherence_rep, repeatresponse_rep)
        psych_curve2_alt = Utils.compute_psych_curve(repeatcoherence_alt, repeatresponse_alt)

        axes.plot([0, 0], [0, 1], 'k-', lw=1, linestyle=':')
        axes.plot([-1, 1], [0.5, 0.5], 'k-', lw=1, linestyle=':')

        axes.errorbar(psych_curve2_rep.xdata, psych_curve2_rep.ydata,
                      yerr=psych_curve2_rep.fit_error, fmt='ro', elinewidth=1, markersize=3)
        axes.errorbar(psych_curve2_alt.xdata, psych_curve2_alt.ydata,
                      yerr=psych_curve2_alt.fit_error, fmt='bo', elinewidth=1, markersize=3)
        axes.plot(np.linspace(-1, 1, 30), psych_curve2_rep.fit, color='red', linewidth=1)
        axes.plot(np.linspace(-1, 1, 30), psych_curve2_alt.fit, color='blue', linewidth=1)
        axes.set_yticks(np.arange(0, 1.1, step=0.1))
        axes.set_xlabel('Evidence repeat')
        axes.set_title('Prob repeat after correct OFF')
        axes.set_xlim([-1.05, 1.05])
        axes.yaxis.set_ticklabels([])
        axes.yaxis.set_tick_params(labelsize=0)
        axes.xaxis.set_tick_params(labelsize=9)
        axes.set_ylim([-0.05, 1.05])
        axes.tick_params(labelsize=9)


        axes.annotate(str(round(psych_curve2_rep.ydata[0], 2)),
                      xy=(psych_curve2_rep.xdata[0], psych_curve2_rep.ydata[0]),
                      xytext=(psych_curve2_rep.xdata[0] - 0.03, psych_curve2_rep.ydata[0] + 0.05), fontsize=8)
        axes.annotate(str(round(psych_curve2_rep.ydata[-1], 2)),
                      xy=(psych_curve2_rep.xdata[-1], psych_curve2_rep.ydata[-1]),
                      xytext=(psych_curve2_rep.xdata[-1] - 0.1, psych_curve2_rep.ydata[-1] - 0.08), fontsize=8)
        # s_rep, b_rep, lr_r_rep, lr_l_rep = psych_curve2_rep.params

        axes.annotate(str(round(psych_curve2_alt.ydata[0], 2)),
                      xy=(psych_curve2_alt.xdata[0], psych_curve2_alt.ydata[0]),
                      xytext=(psych_curve2_alt.xdata[0] - 0.03, psych_curve2_alt.ydata[0] + 0.05), fontsize=8)
        axes.annotate(str(round(psych_curve2_alt.ydata[-1], 2)),
                      xy=(psych_curve2_alt.xdata[-1], psych_curve2_alt.ydata[-1]),
                      xytext=(psych_curve2_alt.xdata[-1] - 0.1, psych_curve2_alt.ydata[-1] - 0.08), fontsize=8)

        if light_on_off:
            # SEVENTH PLOT: CPOKE HISTOGRAM
            axes = plt.subplot2grid((140, 100), (113, 0), rowspan=37, colspan=23)

            pokes = pokes_df_on[(pokes_df_on.poke_time < 1) & (pokes_df_on.c_poke_start < pokes_df_on.play_target_start)].poke_time
            pokes *= 1000

            axes.hist(pokes, bins=50)

            axes.set_xlim([0, 900])
            axes.axvline(x=300, color='r', linestyle=':')
            axes.set_title("Center poke time ON")
            axes.set_xlabel("Time [ms]")
            axes.yaxis.set_tick_params()
            axes.xaxis.set_tick_params()

            # EIGHTH PLOT: CUMULATIVE TIME
            axes = plt.subplot2grid((140, 100), (113, 26), rowspan=37, colspan=23)

            axes.plot(valid_trials_df_on.trial, valid_trials_df_on.trial_start, linewidth=2)
            axes.set_title("Engagement ON")
            axes.set_xlabel("Trials")
            axes.set_ylabel("Time [s]")

            # NINTH PLOT: PSYCHOMETRIC CURVE CORRECTS
            axes = plt.subplot2grid((140, 100), (113, 53), rowspan=37, colspan=23)

            coherences = valid_trials_df_on.coherence
            response = valid_trials_df_on.response
            previousResponse = valid_trials_df_on.previousResponse

            # we need coherences from -1 to 1, response from 0 to 1
            coherences = coherences * 2 - 1
            coherences[abs(coherences) < 0.1] = 0

            psych_curve = Utils.compute_psych_curve(coherences, response)

            axes.plot([0, 0], [0, 1], 'k-', lw=1, linestyle=':')
            axes.plot([-1, 1], [0.5, 0.5], 'k-', lw=1, linestyle=':')

            axes.errorbar(psych_curve.xdata, psych_curve.ydata,
                          yerr=psych_curve.fit_error, fmt='ro', elinewidth=1, markersize=3)
            axes.plot(np.linspace(-1, 1, 30), psych_curve.fit, color='black', linewidth=1)
            axes.set_yticks(np.arange(0, 1.1, step=0.1))
            axes.set_xlabel('Evidence')
            axes.set_title('Prob right ON')
            axes.set_xlim([-1.05, 1.05])
            axes.yaxis.set_tick_params(labelsize=9)
            axes.xaxis.set_tick_params(labelsize=9)
            axes.set_ylim([-0.05, 1.05])
            axes.tick_params(labelsize=9)
            axes.annotate(str(round(psych_curve.ydata[0], 2)), xy=(psych_curve.xdata[0], psych_curve.ydata[0]),
                          xytext=(psych_curve.xdata[0] - 0.03, psych_curve.ydata[0] + 0.05), fontsize=8)
            axes.annotate(str(round(psych_curve.ydata[-1], 2)), xy=(psych_curve.xdata[-1], psych_curve.ydata[-1]),
                          xytext=(psych_curve.xdata[-1] - 0.1, psych_curve.ydata[-1] - 0.08), fontsize=8)

            # TENTH PLOT: PSYCHOMETRIC CURVE REPEAT
            axes = plt.subplot2grid((140, 100), (113, 79), rowspan=37, colspan=21)

            prob_repeat = valid_trials_df_on.env_prob_repeat
            previousHit = valid_trials_df_on.previousHit

            repeatresponse_rep = []
            repeatresponse_alt = []
            repeatcoherence_rep = []
            repeatcoherence_alt = []

            previousResponseSigned = np.array(previousResponse) * 2 - 1

            for i in range(response.size):
                if i == 0:
                    if prob_repeat.iloc[0] >= 0.5:
                        repeatresponse_rep.append(0)
                        repeatcoherence_rep.append(coherences.iloc[i])
                    else:
                        repeatresponse_alt.append(0)
                        repeatcoherence_alt.append(coherences.iloc[i])
                elif response.iloc[i] == previousResponse.iloc[i]:
                    if previousHit.iloc[i] == 1:
                        if prob_repeat.iloc[i] >= 0.5:
                            repeatresponse_rep.append(1)
                            repeatcoherence_rep.append(coherences.iloc[i] * previousResponseSigned[i])
                        else:
                            repeatresponse_alt.append(1)
                            repeatcoherence_alt.append(coherences.iloc[i] * previousResponseSigned[i])
                else:
                    if previousHit.iloc[i] == 1:
                        if prob_repeat.iloc[i] >= 0.5:
                            repeatresponse_rep.append(0)
                            repeatcoherence_rep.append(coherences.iloc[i] * previousResponseSigned[i])
                        else:
                            repeatresponse_alt.append(0)
                            repeatcoherence_alt.append(coherences.iloc[i] * previousResponseSigned[i])

            psych_curve2_rep = Utils.compute_psych_curve(repeatcoherence_rep, repeatresponse_rep)
            psych_curve2_alt = Utils.compute_psych_curve(repeatcoherence_alt, repeatresponse_alt)

            axes.plot([0, 0], [0, 1], 'k-', lw=1, linestyle=':')
            axes.plot([-1, 1], [0.5, 0.5], 'k-', lw=1, linestyle=':')

            axes.errorbar(psych_curve2_rep.xdata, psych_curve2_rep.ydata,
                          yerr=psych_curve2_rep.fit_error, fmt='ro', elinewidth=1, markersize=3)
            axes.errorbar(psych_curve2_alt.xdata, psych_curve2_alt.ydata,
                          yerr=psych_curve2_alt.fit_error, fmt='bo', elinewidth=1, markersize=3)
            axes.plot(np.linspace(-1, 1, 30), psych_curve2_rep.fit, color='red', linewidth=1)
            axes.plot(np.linspace(-1, 1, 30), psych_curve2_alt.fit, color='blue', linewidth=1)
            axes.set_yticks(np.arange(0, 1.1, step=0.1))
            axes.set_xlabel('Evidence repeat')
            axes.set_title('Prob repeat after correct ON')
            axes.set_xlim([-1.05, 1.05])
            axes.yaxis.set_ticklabels([])
            axes.yaxis.set_tick_params(labelsize=0)
            axes.xaxis.set_tick_params(labelsize=9)
            axes.set_ylim([-0.05, 1.05])
            axes.tick_params(labelsize=9)

            axes.annotate(str(round(psych_curve2_rep.ydata[0], 2)),
                          xy=(psych_curve2_rep.xdata[0], psych_curve2_rep.ydata[0]),
                          xytext=(psych_curve2_rep.xdata[0] - 0.03, psych_curve2_rep.ydata[0] + 0.05), fontsize=8)
            axes.annotate(str(round(psych_curve2_rep.ydata[-1], 2)),
                          xy=(psych_curve2_rep.xdata[-1], psych_curve2_rep.ydata[-1]),
                          xytext=(psych_curve2_rep.xdata[-1] - 0.1, psych_curve2_rep.ydata[-1] - 0.08), fontsize=8)
            # s_rep, b_rep, lr_r_rep, lr_l_rep = psych_curve2_rep.params

            axes.annotate(str(round(psych_curve2_alt.ydata[0], 2)),
                          xy=(psych_curve2_alt.xdata[0], psych_curve2_alt.ydata[0]),
                          xytext=(psych_curve2_alt.xdata[0] - 0.03, psych_curve2_alt.ydata[0] + 0.05), fontsize=8)
            axes.annotate(str(round(psych_curve2_alt.ydata[-1], 2)),
                          xy=(psych_curve2_alt.xdata[-1], psych_curve2_alt.ydata[-1]),
                          xytext=(psych_curve2_alt.xdata[-1] - 0.1, psych_curve2_alt.ydata[-1] - 0.08), fontsize=8)

        pdf.savefig()
        plt.close()

    # RETURN NEW DATA
    params['accuracy'] = per_corrects
    params['per_invalids'] = per_invalids
    params['accuracy_left'] = per_left
    params['accuracy_right'] = per_right
    params['response_time_mean'] = response_time_mean
    params['reaction_time_mean'] = reaction_time_mean
    params['micropokes'] = micropokes
    params['silent_fail'] = silent_fail

    params['xdata'] = psych_curve.xdata
    params['ydata'] = psych_curve.ydata
    params['params'] = psych_curve.params
    params['fit'] = psych_curve.fit
    params['fit_error'] = psych_curve.fit_error
    params['xdata_rep'] = psych_curve2_rep.xdata
    params['ydata_rep'] = psych_curve2_rep.ydata
    params['params_rep'] = psych_curve2_rep.params
    params['fit_rep'] = psych_curve2_rep.fit
    params['fit_error_rep'] = psych_curve2_rep.fit_error

    # params['xdata'] = ','.join(str(e) for e in psych_curve.xdata)
    # params['ydata'] = ','.join(str(e) for e in psych_curve.ydata)
    # params['params'] = ','.join(str(e) for e in psych_curve.params)
    # params['fit'] = ','.join(str(e) for e in psych_curve.fit)
    # params['fit_error'] = ','.join(str(e) for e in psych_curve.fit_error)
    # params['xdata_rep'] = ','.join(str(e) for e in psych_curve2.xdata)
    # params['ydata_rep'] = ','.join(str(e) for e in psych_curve2.ydata)
    # params['params_rep'] = ','.join(str(e) for e in psych_curve2.params)
    # params['fit_rep'] = ','.join(str(e) for e in psych_curve2.fit)
    # params['fit_error_rep'] = ','.join(str(e) for e in psych_curve2.fit_error)

    return params
