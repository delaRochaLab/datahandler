"""
For each task, create a parser file, a daily report file, an intersession file and optionally a to_matlab file.
Follow the name conventions. Classes start with capital letters, functions and file names lowercase.
First, import the classes and the functions from the corresponding files.
Then, add the possible task names in the select_class function below.
"""

from tasks.cellex4A_parse import cellex4A_parse
from tasks.cellex4A_glue_sessions import cellex4A_glue_sessions
from tasks.cellex4A_daily_report import cellex4A_daily_report
from tasks.cellex4A_intersession_report import cellex4A_intersession_report
from tasks.cellex4A_to_matlab import cellex4A_to_matlab

from tasks.cellex4A_opto_parse import cellex4A_opto_parse
from tasks.cellex4A_opto_daily_report import cellex4A_opto_daily_report
from tasks.cellex4A_opto_intersession_report import cellex4A_opto_intersession_report
from tasks.cellex4A_opto_to_matlab import cellex4A_opto_to_matlab

from tasks.cellex4A_freq_parse import cellex4A_freq_parse
from tasks.cellex4A_freq_daily_report import cellex4A_freq_daily_report
from tasks.cellex4A_freq_intersession_report import cellex4A_freq_intersession_report

from tasks.wmfm_parse import wmfm_parse
from tasks.wmfm_glue_sessions import wmfm_glue_sessions
from tasks.wmfm_daily_report import wmfm_daily_report
from tasks.wmfm_intersession_report import wmfm_intersession_report

from tasks.wmfm_lickteaching_parse import wmfm_lickteaching_parse
from tasks.wmfm_lickteaching_daily_report import wmfm_lickteaching_daily_report
from tasks.wmfm_lickteaching_intersession import wmfm_lickteaching_intersession

from tasks.wmfm_touchteaching_daily_report import wmfm_touchteaching_daily_report
from tasks.wmfm_touchteaching_intersession import wmfm_touchteaching_intersession

from tasks.wm_parse import wm_parse
from tasks.wm_glue_sessions import wm_glue_sessions
from tasks.wm_daily_report import wm_daily_report
from tasks.wm_intersession_report import wm_intersession_report

taskNames = ['cellex4A', 'wmfm_old', 'wmfm', 'wm']  # add the generic name of your group of tasks

class Selection:

    def __init__(self, task, default_task):

        self.parse = None
        self.glue_sessions = None
        self.make_daily = None
        self.make_intersession = None
        self.make_matlab = None
        self.assign_task(task, default_task)

    def assign_task(self, task, default_task):

        if task in {'cellex4A', 'p3', 'pilot_s6_01', 'cellex4A_u', 'cellex4A_uncorrelated_new',
                    'cellex4A_repalt_silence_new', 'cellex4A_leftright_silence_new', 'cellex4A_repalt_delay',
                    'cellex4A_leftright_delay', 'cellex4A_uncorrelated_silence_new', 'cellex4A_repalt_silence_new_easy',
                    'p1_STRF_v2'}:

            self.parse = cellex4A_parse
            self.glue_sessions = cellex4A_glue_sessions
            self.make_daily = cellex4A_daily_report
            self.make_intersession = cellex4A_intersession_report
            self.make_matlab = cellex4A_to_matlab

        elif '_freq' in task:
            self.parse = cellex4A_freq_parse
            self.glue_sessions = cellex4A_glue_sessions
            self.make_daily = cellex4A_freq_daily_report
            self.make_intersession = cellex4A_freq_intersession_report
            self.make_matlab = cellex4A_to_matlab

        elif 'opto' in task:
            self.parse = cellex4A_opto_parse
            self.glue_sessions = cellex4A_glue_sessions
            self.make_daily = cellex4A_opto_daily_report
            self.make_intersession = cellex4A_opto_intersession_report
            self.make_matlab = cellex4A_opto_to_matlab

        # elif task in {'StageTraining_V3'}:
        #     self.parse = wmfm_parse_old
        #     self.glue_sessions = wmfm_glue_sessions
        #     self.make_daily = wmfm_daily_report_old
        #     self.make_intersession = wmfm_intersession_report
        #     self.make_matlab = None

        elif task in {'wmfm', 'StageTraining_V4'}:
            self.parse = wmfm_parse
            self.glue_sessions = wmfm_glue_sessions
            self.make_daily = wmfm_daily_report
            self.make_intersession = wmfm_intersession_report
            self.make_matlab = None

        elif task in {'Lickteaching'}:
            self.parse = wmfm_lickteaching_parse
            self.glue_sessions = wmfm_glue_sessions
            self.make_daily = wmfm_lickteaching_daily_report
            self.make_intersession = wmfm_lickteaching_intersession
            self.make_matlab = None

        elif task in {'Touchscreen_teaching'}:
            self.parse = wmfm_parse
            self.glue_sessions = wmfm_glue_sessions
            self.make_daily = wmfm_touchteaching_daily_report
            self.make_intersession =  wmfm_touchteaching_intersession
            self.make_matlab = None

        elif task in {'wm', 'tiff'}:
            self.parse = wm_parse
            self.glue_sessions = wm_glue_sessions
            self.make_daily = wm_daily_report
            self.make_intersession = wm_intersession_report
            self.make_matlab = None

        elif task in {'', 'water_calib', 'daily_check', 'Annarat_water_calibration_task', 'calibration_task',
                      'water_calibration_task', 'sub_water_calibration_task', 'Sound-AW-Test'}:
            # calibration tasks that we do not want to parse data from
            pass

        else:
            if default_task != '':
                self.assign_task(default_task, '')
