import logging
import warnings
import os
import datetime
import matplotlib as mpl
import matplotlib.dates as mdates
import matplotlib.pyplot as plt
import numpy as np
import seaborn as sns
import pandas as pd
from matplotlib.backends.backend_pdf import PdfPages
from matplotlib.lines import Line2D
from matplotlib.ticker import PercentFormatter
import matplotlib.patches as mpatches
from numpy import linalg as ln
import logging
if os.environ.get('DISPLAY', '') == '':
    mpl.use('Agg')

# LOGGING AND WARNINGS
logger = logging.getLogger(__name__)
warnings.simplefilter(action='ignore', category=UserWarning)
warnings.simplefilter(action='ignore', category=RuntimeWarning)
warnings.simplefilter(action='ignore', category=FutureWarning)

# PLOT PARAMETERS
mpl.rcParams['lines.linewidth'] = 0.7
mpl.rcParams['lines.markersize'] = 1
mpl.rcParams['font.size'] = 7
mpl.rcParams['axes.spines.right'] = False
mpl.rcParams['axes.spines.top'] = False
label_kwargs = {'fontsize': 9, 'fontweight': 'roman'}

# PLOT COLORS
first_poke_c = '#C97F74'
second_poke_c = '#96367C'
all_poke_c = '#5B1132'

first_correct_c = 'green'
correct_c = 'lightgreen'
incorrect_c = 'red'
water_c = 'teal'
other_c = 'black'

lines_c = 'gray'
lines2_c = 'silver'

def wmfm_touchteaching_intersession(intersession_df_path, intersession_report_path):

    try:
        df = pd.read_csv(intersession_df_path, sep=';')
    except FileNotFoundError:
        logger.critical("Error: Reading trials CSV file. Exiting...")
        raise

    # we set the index of the df as number of session, necessary if we want to unnest columns
    df.index = df.session

    # USEFUL VALUES
    total_sessions = df.shape[0]

    df['reward_drunk'] = 0
    df.loc[(df.reward_type == 0), 'reward_drunk'] = df.water_drunk.astype(float).values
    df.loc[(df.reward_type == 1), 'reward_drunk'] = df.milkshake_eat.astype(float).values
    df['reward_drunk'] = df['reward_drunk'].apply(lambda x: x * 1000)

    with PdfPages(intersession_report_path) as pdf:

        # PAGE 1:
        plt.figure(figsize=(8.3, 11.7))  # A4 vertical

        # NUMBER OF TRIALS
        axes = plt.subplot2grid((50, 50), (0, 0), rowspan=6, colspan=50)
        axes.plot(df.session, df.total_trials, color=all_poke_c, marker='o', markersize='3')
        axes.plot(df.session, df.total_valid_trials, color=first_poke_c, marker='o', markersize='3')

        axes.set_ylim([0, 110])
        axes.set_xticks(df.session)
        axes.set_xlabel("", label_kwargs)
        axes.set_ylabel("Number of trials", label_kwargs)

        colors = [all_poke_c, first_poke_c]
        labels = ['All trials', 'Valid']
        lines = [Line2D([0], [0], color=c, marker='o', markersize=3, markerfacecolor=c) for c in colors]
        axes.legend(lines, labels, fontsize=6)

        # SESSION LENGTH
        df['time_start'] = pd.to_datetime(df['time'])
        df['time_end'] = pd.to_datetime(df['time_end'])
        df['session_lenght'] = df.time_end - df.time_start
        df['session_lenght_min'] = df['session_lenght'].dt.total_seconds().div(60).astype(int)

        axes = plt.subplot2grid((50, 50), (8, 0), rowspan=6, colspan=50)
        axes.plot(df.session, df.session_lenght_min, color=other_c, marker='o', markersize=3)

        axes.set_xticks(df.session)
        axes.set_xlabel("Session number", label_kwargs)
        axes.set_ylabel("Session lenght (min)", label_kwargs)

        # NUMUBER OF TRIALS VS TIME
        axes = plt.subplot2grid((50, 50), (16, 0), rowspan=12, colspan=22)

        df['trials_per_min'] = df.total_valid_trials / df.session_lenght_min
        axes.plot(df.session, df.trials_per_min, c=other_c, marker='o', markersize=3)
        axes.hlines(y=[1, 2, 3], xmin=df.session.iloc[0], xmax=df.session.iloc[-1], color=lines_c, linestyle=':')

        axes.set_xlabel("Session number", label_kwargs)
        axes.set_ylabel("Valid trials per min", label_kwargs)
        axes.set_xticks(df.session)

        # LICKPORT LATENCY
        axes = plt.subplot2grid((50, 50), (16, 26), rowspan=12, colspan=26)
        # axes.plot(df.session, df.lickport_latency, c=other_c, marker='o', markersize=3)
        # axes.plot(df.session, df.lickport_latency, c=other_c, marker='o', markersize=3)

        axes.errorbar(df.session, df.licklat_miss_mean, yerr=df.licklat_miss_std.values / np.sqrt(total_sessions),
                      c=other_c, marker='o', markersize=2)
        axes.errorbar(df.session, df.licklat_correct_first_mean,
                      yerr=df.licklat_correct_first_std.values / np.sqrt(total_sessions),
                      c=first_correct_c, marker='o', markersize=2)

        axes.set_xlabel("Session number", label_kwargs)
        axes.set_ylabel("Lickport latency (sec)", label_kwargs)
        axes.set_xticks(df.session)

        colors = [first_correct_c, other_c]
        labels = ['Correct', 'Miss']
        lines = [Line2D([0], [0], color=c, marker='o', markersize=5, markerfacecolor=c) for c in colors]
        axes.legend(lines, labels, fontsize=6)

        # SAVING AND CLOSING PAGE 1
        pdf.savefig()
        plt.close()
