import numpy as np
import pandas as pd
import logging
from numpy import linalg as ln
import warnings


# LOGGING AND WARNINGS
logger = logging.getLogger(__name__)
warnings.simplefilter(action='ignore', category=UserWarning)
warnings.simplefilter(action='ignore', category=RuntimeWarning)
warnings.simplefilter(action='ignore', category=FutureWarning)

# VARIABLES NEEDED
screen_mmperpixel = 0.28
screen_height = 900
screen_height_mm = screen_height * screen_mmperpixel
stim_diameter = 20  # set always to 20 mm


# MAIN PARSE FUNCTION
def wmfm_parse(file):
    """
    File is an object containing the raw csv we want to read and some properties that have been calculated in the main
    routine:

    file.path = the path of the file
    file.session_name = the name of the session extracted from the filename
    file.task = the name of the task extracted from the filename
    file.subject_name = the name of the subject extracted from the filename
    file.date = the date extracted from the filename
    file.day = the day extracted from the date
    file.time = the time extracted from the date

    First we read the csv into a df and the we create 2 dictionaries.
    Params dictionary has one value (or one list or one numpy array) for each key.
    Trials dictionary contains n values (or n lists or n numpy arrays) for each key, where n is the number of trials.
    From this dictionaries, 2 csv files containing one row and n rows respectively, will be automatically created in
    the main routine.
    """

    # READ THE CSV INTO A DATAFRAME
    try:
        df = pd.read_csv(file.path, skiprows=6, sep=';')
    except FileNotFoundError:
        logger.critical("Error: Reading CSV file. Exiting...")
        raise

    # VALUES WE HAVE ALREADY EXTRACTED FROM THE FILENAME
    session_name = file.session_name
    task = file.task
    subject_name = file.subject_name
    date = file.date
    day = file.day
    time = file.time

    # METADATA AND VARIABLES
    try:
        box = df[df.MSG == 'BOARD-NAME']['+INFO'].values[0]
    except IndexError:
        box = "Unknown box"
        logger.warning("Box name not found.")

    try:
        stage_number = df[df.MSG == 'VAR_STAGE']['+INFO'].values[0]
    except IndexError:
        stage_number = np.nan
        logger.warning('Stage number not found')

    try:
        timeup = df[df.MSG == 'VAR_TIMEUP']['+INFO'].astype(float).values[0]
    except IndexError:
        timeup = np.nan
        logger.warning('timeup not found')

    try:
        fixation_time = df[df.MSG == 'VAR_FIXATION_TIME']['+INFO'].astype(float).values[0]
    except IndexError:
        fixation_time = np.nan
        logger.warning('fixation_time not found')

    try:
        vg_th_diameter = df[df.MSG == 'VAR_VG_THRESHOLD']['+INFO'].astype(float).values[0]
    except IndexError:
        vg_th_diameter = np.nan
        logger.warning('VG_threshold not found')

    try:
        wm_th_diameter = df[df.MSG == 'VAR_WM_THRESHOLD']['+INFO'].astype(float).values[0]
    except IndexError:
        wm_th_diameter = np.nan
        logger.warning('WM_threshold not found')

    try:
        reward_type = df[df.MSG == 'VAR_MILK_RW']['+INFO'].values[0]
    except IndexError:
        reward_type = np.nan
        logger.warning('Reward type not found')

    try:
        basal_weight = df[df.MSG == 'VAR_BASAL_WEIGHT']['+INFO'].astype(float).values[0]
    except:
        if subject_name == 'A1':
            basal_weight = 25.7
        elif subject_name == 'A2':
            basal_weight = 22.4
        elif subject_name == 'A3':
            basal_weight = 25.3
        elif subject_name == 'A4':
            basal_weight = 23.8
        elif subject_name == 'C1':
            basal_weight = 32.0
        elif subject_name == 'C2':
            basal_weight = 32.4
        elif subject_name == 'C3':
            basal_weight = 29.7
        elif subject_name == 'C4':
            basal_weight = 28.8
        elif subject_name == 'M1':
            basal_weight = 32.0
        elif subject_name == 'M2':
            basal_weight = 31.0
        elif subject_name == 'M3':
            basal_weight = 28.8
        elif subject_name == 'M4':
            basal_weight = 31.6
        else:
            logger.warning('Basal weight not found')

    weight = df[df.MSG == 'VAR_WEIGHT']['+INFO'].astype(float).values
    weight = weight[0] if weight.size else np.nan

    height = df[df.MSG == 'VAR_HEIGHT']['+INFO'].astype(float).values
    height = height[0] if height.size else 14

    easy_trials = df[df.MSG == 'VAR_EASY_TRIALS']['+INFO'].astype(float).values
    easy_trials = easy_trials[0] if easy_trials.size else 0

    stim_type = df[df.MSG =='STIMULUS_TYPE']['+INFO'].values
    stim_type = stim_type[0] if stim_type.size else 'circle'

    try:
        stim_time_wmi = df[df.MSG =='VAR_STIM_TIME_WMI']['+INFO'].astype(float).values[0]
    except:
        stim_time_wmi = df[df.MSG == 'VAR_DELAY_INTRO']['+INFO'].astype(float).values
        stim_time_wmi = stim_time_wmi[0] if stim_time_wmi.size else np.nan

    delay_short = df[df.MSG == 'VAR_DELAY_SHORT']['+INFO'].astype(float).values
    delay_short = delay_short[0] if delay_short.size else np.nan

    delay_med = df[df.MSG == 'VAR_DELAY_MED']['+INFO'].astype(float).values
    delay_med = delay_med[0] if delay_med.size else np.nan

    delay_long = df[df.MSG == 'VAR_DELAY_LONG']['+INFO'].astype(float).values
    delay_long = delay_long[0] if delay_long.size else np.nan

    water_drunk = df[df.MSG =='WATER_DRUNK']['+INFO'].astype(float).values
    water_drunk = water_drunk[-1] * 0.001 if water_drunk.size else 0  # last value of water_drunk converted to mL

    milkshake_eat = df[df.MSG == 'MILKSHAKE_EAT']['+INFO'].astype(float).values
    milkshake_eat = milkshake_eat[-1] * 0.001 if milkshake_eat.size else 0

    logger.info(str(subject_name) + str(day) + '-' + str(time))
    logger.info("Metadata loaded.")

    # CREATES CHANGING VARIABLES
    trial_type = []
    th_diameter = []
    delay = []
    delay_type = []
    start_trial = []
    stop_respwin = []
    fixation_breaks = []
    correct = []
    incorrects = []
    incorrect_attempts = []
    all_responses_time = []
    trial_result = []
    lick_time = []
    stimulus_onsets = []
    stimulus_on = []
    stimulus_off = []
    absolute_time = []
    x_positions = []
    y_positions = []
    x_responses = []
    y_responses = []
    x_errors = []
    y_errors = []
    errors = []
    wm_bool = []
    time_end_list = []

    # NUMBER OF TRIALS
    trial_start = np.flatnonzero(df['MSG'] == 'X_COORDINATE_PSYCHOPY')
    trial_end = np.flatnonzero(df['TYPE'] == 'END-TRIAL')
    trials = int(len(trial_end))  # only ended trials

    # TOTAL DURATION
    durations = df[df.MSG == 'TRIAL-BPOD-TIME']['BPOD-FINAL-TIME'].values[:trials].astype(float)
    try:
        total_duration = max(durations)
    except:
        total_duration = 0

    firstWMG = False  # it is false until the first WMG

    # ITERATE FOR EVERY TRIAL
    for elem in range(trials):

        # GET TRIAL
        trial = df[trial_start[elem]:trial_end[elem] + 1]

        # ABSOLUTE TIME
        absolute_time_trial = trial.query(
            "TYPE=='TRANSITION' and MSG=='Exit'")['BPOD-INITIAL-TIME'].astype(float).values
        if absolute_time_trial.size:
            absolute_time_trial = absolute_time_trial[0]
        else:
            trials = elem
            break  # this is the last trial and it's empty, exit the loop
        absolute_time.append(absolute_time_trial)

        # GET START TIME & RESPONSE WINDOW ONSET
        start_respwin_trial = trial.query(
            "TYPE=='TRANSITION' and MSG=='Response_window'")['BPOD-INITIAL-TIME'].astype(float).values
        if start_respwin_trial.size:
            start_respwin_trial = start_respwin_trial[0]
        start_trial.append(-start_respwin_trial)

        # GET TRIAL TYPE
        trial_type_trial = trial.query("TYPE=='VAL' and MSG=='TRIAL_TYPE'")['+INFO'].values[0]
        trial_type.append(trial_type_trial)

        # GET THRESHOLD
        if trial_type_trial == 'VG':
            th_diameter_trial = vg_th_diameter
            if day < '20200211' and firstWMG:
                th_diameter_trial = wm_th_diameter
        else:
            firstWMG = True
            th_diameter_trial = wm_th_diameter
        th_diameter.append(th_diameter_trial)

        # GET DELAY
        if trial_type_trial == 'VG' or trial_type_trial == 'WM_I':
            delay_trial = 0
            delay_type_trial = np.nan
        elif trial_type_trial == 'WM_D':
            delay_trial = trial.query("TYPE=='VAL' and MSG == 'DELAY'")['+INFO'].astype(float).values[0]
            if delay_trial == delay_short:
                delay_type_trial = 'short'
            elif delay_trial == delay_long:
                delay_type_trial = 'long'
            elif  delay_trial == delay_med:
                delay_type_trial = 'med'
            else:
                delay_type_trial = 'delay not found'
        delay.append(delay_trial)
        delay_type.append(delay_type_trial)

        # GET RESPONSE WINDOW OFFSET
        stop_respwin_trial = trial.query(
            "TYPE=='TRANSITION' and MSG=='Wait_for_reward'")['BPOD-INITIAL-TIME'].astype(float).values
        if stop_respwin_trial.size:
            stop_respwin_trial = stop_respwin_trial[0] - start_respwin_trial
        else:
            stop_respwin_trial = trial.query(
                "TYPE=='TRANSITION' and MSG=='Wait_for_reward_non_first'")['BPOD-INITIAL-TIME'].astype(float).values
            if stop_respwin_trial.size:
                stop_respwin_trial = stop_respwin_trial[0] - start_respwin_trial
            else:
                stop_respwin_trial = trial.query(
                    "TYPE=='TRANSITION' and MSG=='Wait_for_miss_poke'")['BPOD-INITIAL-TIME'].astype(float).values
                if stop_respwin_trial.size:
                    stop_respwin_trial = stop_respwin_trial[0] - start_respwin_trial
        stop_respwin.append(stop_respwin_trial)

        # CORRECT
        correct_trial = trial.query(
            "TYPE=='TRANSITION' and MSG=='Correct_first'")['BPOD-INITIAL-TIME'].astype(float).values
        if correct_trial.size:
            correct_trial = correct_trial[0] - start_respwin_trial
        else:
            correct_trial = trial.query(
                "TYPE=='TRANSITION' and MSG=='Correct_non_first'")['BPOD-INITIAL-TIME'].astype(float).values
            if correct_trial.size:
                correct_trial = correct_trial[0] - start_respwin_trial
            else:
                correct_trial = np.nan
        correct.append(correct_trial)

        # WATER REWARD & ABSOLUTE TIME
        lick_time_trial = trial.query(
            "TYPE=='TRANSITION' and MSG=='Reward'")['BPOD-INITIAL-TIME'].astype(float).values # Correct first
        if lick_time_trial.size:
            lick_time_trial = lick_time_trial[0] - start_respwin_trial
        else:
            lick_time_trial = trial.query(
                "TYPE=='TRANSITION' and MSG=='Reward_non_first'")['BPOD-INITIAL-TIME'].astype(
                float).values  # Correct second
            if lick_time_trial.size:
                lick_time_trial = lick_time_trial[0] - start_respwin_trial
            else:
                lick_time_trial = trial.query(
                    "TYPE=='TRANSITION' and MSG=='Miss_poke'")['BPOD-INITIAL-TIME'].astype(float).values  # Wrong or missed trial
                if lick_time_trial.size:
                    lick_time_trial = lick_time_trial[0] - start_respwin_trial
                else:
                    lick_time_trial = np.nan
        lick_time.append(lick_time_trial)


        # INCORRECT POKES & ALL RESPONSES
        incorrects_trial = trial.query(
            "TYPE=='TRANSITION' and MSG=='Incorrect_first'")['BPOD-INITIAL-TIME'].astype(float).values

        second_incorrect_trial = trial.query(
            "TYPE=='TRANSITION' and MSG=='Incorrect_non_first'")['BPOD-INITIAL-TIME'].astype(float).values

        incorrects_trial = np.concatenate([incorrects_trial, second_incorrect_trial])
        incorrects_trial -= start_respwin_trial

        if np.isnan(correct_trial):
            all_responses_time_trial = incorrects_trial
            trial_result_trial = -len(incorrects_trial)
            incorrect_attempts_trial = list(range(1, abs(trial_result_trial) + 1))
        else:
            all_responses_time_trial = np.append(incorrects_trial, [correct_trial])
            incorrects_trial = np.append(incorrects_trial, np.nan)
            trial_result_trial = len(incorrects_trial)
            incorrect_attempts_trial = list(range(1, abs(trial_result_trial) + 1))
            incorrect_attempts_trial[-1] = 0

        incorrects.append(incorrects_trial)
        trial_result.append(trial_result_trial)
        incorrect_attempts.append(incorrect_attempts_trial)
        all_responses_time.append(all_responses_time_trial)

        # STIMULUS ONSETS
        stimulus_onset_trial = trial.query(
            "TYPE=='TRANSITION' and MSG=='Fixation'")['BPOD-INITIAL-TIME'].astype(float).values
        if stimulus_onset_trial.size:
            stimulus_onset_trial = stimulus_onset_trial[:-1]  # last onset is in fact stimulus_on
            stimulus_onset_trial -= start_respwin_trial
        else:
            stimulus_onset_trial = []
        stimulus_onsets.append(stimulus_onset_trial)

        # FIXATION BREAKS
        fixation_breaks_trial = trial.query(
            "TYPE=='TRANSITION' and MSG=='Fixation_break'")['BPOD-INITIAL-TIME'].astype(float).values
        if len(fixation_breaks_trial) > len(stimulus_onset_trial):  # first break is not a break is the beggining ¿?
            fixation_breaks_trial = fixation_breaks_trial[1:]
            fixation_breaks_trial -= start_respwin_trial
        elif fixation_breaks_trial.size:
            fixation_breaks_trial -= start_respwin_trial
        else:
            fixation_breaks_trial = []
        fixation_breaks.append(fixation_breaks_trial)

        # STIMULUS ON
        stimulus_on_trial = -(fixation_time + delay_trial)
        stimulus_on.append(stimulus_on_trial)

        # STIMULUS OFF
        if trial_type_trial == 'VG':
            stimulus_off_trial = stop_respwin_trial
        elif trial_type_trial == 'WM_I':
            stimulus_off_trial = min(stim_time_wmi - fixation_time, correct_trial)
        elif trial_type_trial == 'WM_D':
            stimulus_off_trial = - delay_trial
        else:
            stimulus_off_trial = np.nan
        stimulus_off.append(stimulus_off_trial)

        # WORKING MEMORY TRIALS
        wm_bool_trial = (stimulus_off_trial < all_responses_time_trial)
        wm_bool.append(wm_bool_trial)

        # PSYCHOPY STIMULUS COORDINATES
        try:
            x_positions_trial = trial.query(
                "TYPE=='VAL' and MSG=='X_COORDINATE_PSYCHOPY'")['+INFO'].astype(int).values[0] * screen_mmperpixel
        except:
            x_positions_trial = np.nan
        y_positions_trial = height
        x_positions.append(x_positions_trial)
        y_positions.append(y_positions_trial)

        raw_responses_trial = trial.query("TYPE=='VAL' and MSG=='ANSW_COORDINATES'")['+INFO'].values
        x_responses_trial = []
        y_responses_trial = []
        x_errors_trial = []
        y_errors_trial = []
        errors_trial = []

        # ANALYSE ALL COORD
        try:
            for d in range(len(raw_responses_trial)):
                raw = raw_responses_trial[d]
                if type(raw) == float:
                    del raw
                    continue
                to_replace = ",[]"
                for ch in to_replace:
                    raw = raw.replace(ch, "")
                str_answer = raw.split()
                float_answer = [float(e) * screen_mmperpixel for e in str_answer]  # converted to mm

                if len(float_answer) < 3:  # necessary, if len = 3 the coords are not transformed, we don't use them
                    x = float_answer[0]
                    y = screen_height_mm - float_answer[1]
                    x_responses_trial.append(x)
                    y_responses_trial.append(y)
                    x_errors_trial.append(x - x_positions_trial)
                    y_errors_trial.append(y - y_positions_trial)
                    err = ln.norm(np.array([x, y]) - np.array([x_positions_trial, y_positions_trial]))
                    errors_trial.append(err)
        except ValueError:
            continue

        # in the last trial it is possible to have an extra coordinate response that we need to delete
        if len(x_responses_trial) > len(all_responses_time_trial):
            x_responses_trial.pop()
            y_responses_trial.pop()
            x_errors_trial.pop()
            y_errors_trial.pop()
            errors_trial.pop()

        x_responses.append(np.asarray(x_responses_trial))
        y_responses.append(np.asarray(y_responses_trial))
        x_errors.append(np.asarray(x_errors_trial))
        y_errors.append(np.asarray(y_errors_trial))
        errors.append(np.asarray(errors_trial))

        # TRIAL END TIME
        time_end_trial = trial.query(
            "TYPE=='END-TRIAL' and MSG=='The trial ended'")['PC-TIME'].values
        if time_end_trial.size:
            time_end_trial = time_end_trial[0]
        else:
            time_end_trial = ['0000-00-00 00:00:00']
            logger.warning("Trial end time not found.")
        time_end_list.append(time_end_trial)

    # print(time_end_list)
    time_end = time_end_list[-1]  # last time end is in fact when session ends
    time_end = time_end[11:19]

    # RESPONSE WINDOW IS TIME ZERO
    start_respwin = [0] * trials

    # TRIAL NUMBER
    trial = list(range(1, trials + 1))

    session_params = {'session_name': session_name,
                      'stage_number': stage_number,
                      'subject_name': subject_name,
                      'weight': weight,
                      'basal_weight': basal_weight,
                      'task': task,
                      'date': date,
                      'day': day,
                      'time': time,
                      'time_end': time_end,
                      'box': box,
                      'reward_type':reward_type,
                      'easy_trials': easy_trials,
                      'stim_type': stim_type,
                      'height': height,
                      'fixation_time': fixation_time,
                      'stim_diameter': stim_diameter,
                      'vg_th_diameter': vg_th_diameter,
                      'wm_th_diameter': wm_th_diameter,
                      'timeup': timeup,
                      'stim_time_wmi': stim_time_wmi,
                      'delay_short': delay_short,
                      'delay_med': delay_med,
                      'delay_long': delay_long,
                      'total_trials': trials,
                      'total_duration': total_duration,
                      'water_drunk': water_drunk,
                      'milkshake_eat': milkshake_eat,
                      }

    session_trials = {'trial': trial,
                      'trial_type': trial_type,
                      'delay': delay,
                      'delay_type': delay_type,
                      'th_diameter': th_diameter,
                      'stimulus_onsets': stimulus_onsets,
                      'fixation_breaks': fixation_breaks,
                      'start_trial': start_trial,
                      'start_respwin': start_respwin,
                      'stop_respwin': stop_respwin,
                      'stimulus_on': stimulus_on,
                      'stimulus_off': stimulus_off,
                      'lick_time': lick_time,
                      'absolute_time': absolute_time,
                      'trial_result': trial_result,
                      'correct': correct,
                      'x_positions': x_positions,
                      'y_positions': y_positions,
                      'all_responses_time': all_responses_time,
                      'incorrects': incorrects,
                      'wm_bool': wm_bool,
                      'x_responses': x_responses,
                      'y_responses': y_responses,
                      'x_errors': x_errors,
                      'y_errors': y_errors,
                      'errors': errors,
                      'incorrect_attempts': incorrect_attempts
                      }

    return [session_params, session_trials]
