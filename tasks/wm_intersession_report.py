# -*- coding: utf-8 -*-
"""
Created on Tue Dec 10 17:57:59 2019

@author: user
"""

import logging
import warnings
import os
import re
import datetime
import matplotlib as mpl
import matplotlib.dates as mdates
import matplotlib.pyplot as plt
import numpy as np
import seaborn as sns
import pandas as pd
from matplotlib.backends.backend_pdf import PdfPages
from matplotlib.lines import Line2D
from matplotlib.ticker import PercentFormatter
import matplotlib.patches as mpatches
from numpy import linalg as ln
import logging

# LOGGING AND WARNINGS
logger = logging.getLogger(__name__)
warnings.simplefilter(action='ignore', category=UserWarning)
warnings.simplefilter(action='ignore', category=RuntimeWarning)
warnings.simplefilter(action='ignore', category=FutureWarning)

# PLOT PARAMETERS
mpl.rcParams['lines.linewidth'] = 0.7
mpl.rcParams['lines.markersize'] = 1
mpl.rcParams['font.size'] = 7
mpl.rcParams['axes.spines.right'] = False
mpl.rcParams['axes.spines.top'] = False
label_kwargs = {'fontsize': 9, 'fontweight': 'roman'}

# PLOT COLORS AND MARKS
stage_marks = ['o', '^', 's', 'D']

# Plot parameters:
COLORLEFT = '#31A2AC'
COLORRIGHT = '#FF8D3F'
DELAY_H = 'black'
DELAY_M = 'purple'
DELAY_L = 'crimson'
BINSIZE = 0.1
LENSESSIONS = 30

intersession_df_path='C:/Users/user/Google Drive (tiffany.ona@gmail.com)/WORKING_MEMORY/EXPERIMENTS/4B/clean/N10/N10_intersession.csv'
intersession_report_path= 'C:/Users/user/Google Drive (tiffany.ona@gmail.com)/WORKING_MEMORY/EXPERIMENTS/4B/'

#intersession_df_path='C:/Users/Tiffany/Google Drive/WORKING_MEMORY/EXPERIMENTS/4B/clean/N15/N15_intersession.csv'
#intersession_report_path= 'C:/Users/Tiffany/Google Drive/WORKING_MEMORY/EXPERIMENTS/4B/'

def wm_intersession_report(intersession_df_path, intersession_report_path):

    try:
        df = pd.read_csv(intersession_df_path, sep=';')
    except FileNotFoundError:
        logger.critical("Error: Reading trials CSV file. Exiting...")
        raise

    # we set the index of the df as number of session, necessary if we want to unnest columns
    df.index = df.session

    with PdfPages(intersession_report_path) as pdf:
        #Re-indexar el dataframe
        df = df.sort_values(by='day')
        
        df = df.drop_duplicates() #Remove duplicates
        
        df.index = np.arange(len(df))
        df['session']=df.index+1
        
        #Cut information of sessions to plot only last ones.
        df=df[-LENSESSIONS:]
 
        conditions = [df.stage_number == num for num in range(4)]
        conditions.append(np.isnan(df.stage_number))

        conditions.append(True)
        colors = ['', 'blue', 'green', 'black', 'purple', 'orange']
        df['color'] = np.select(conditions, colors)

        boxes = ['BPOD01', 'BPOD02', 'BPOD03', 'BPOD04', 'BPOD05', 'BPOD06']
        conditions = [df.box == box for box in boxes]
        conditions.append(True)

        markers = ['o', 'o','o','o','o','o','o']
        df['marker'] = np.select(conditions, markers)
        
#______________________________Plot accuracy accross sessions._________________________________________      
        fig = plt.figure(figsize=(11.7, 8.3))  
        axes = plt.subplot2grid((14,2), (0,0), colspan=2, rowspan=4)

        grouped = df.groupby('marker')
        for key, group in grouped:
            axes.scatter(group.session, group.accuracy_right, c=group.color, marker=key, s=8, zorder=2)
            axes.scatter(group.session, group.accuracy_left, c=group.color, marker=key, s=8, zorder=2)
            axes.scatter(group.session, group.accuracy, c=group.color, marker=key, s=8, zorder=2)

        axes.plot(df.session, df.accuracy_right, color=COLORRIGHT)
        axes.plot(df.session, df.accuracy_left, color=COLORLEFT)
        axes.plot(df.session, df.accuracy, color='black')

        axes.set_ylabel('Accuracy (%)')

        axes.get_xaxis().set_visible(False)    
           
        axes.set_ylim([0,1.05])
        axes.set_xlim([df.index[0],df.index[-1]])
        axes.hlines(y=0.5, xmin=df.session.iloc[0], xmax=df.session.iloc[-1], color='gray',linestyle = ':')
        axes.hlines(y=0.75, xmin=df.session.iloc[0], xmax=df.session.iloc[-1], color='blue',linestyle = ':')
                
        # Custom legend:
        legend_elements = [Line2D([0], [0],color='black', label='Total'),
        Line2D([0], [0], color=COLORLEFT, label='Left'),
        Line2D([0], [0], color=COLORRIGHT, label='Right'),
        Line2D([0], [0], color='blue', label='Stage 1',marker='s'),
        Line2D([0], [0], color='green', label='Stage 2',marker='s'),
        Line2D([0], [0], color='black', label='Stage 3',marker='s')]
        leg = plt.legend(loc="lower right", handles=legend_elements, ncol=1, prop={'size': 4})
        leg.get_frame().set_alpha(0.1)
        
#______________________________Plot misses accross sessions._________________________________________  
        axes = plt.subplot2grid((14,2), (4,0), colspan=2, rowspan=2)

        axes.plot(df.session, df.miss_right, marker = 'o', color=COLORRIGHT)
        axes.plot(df.session, df.miss_left, marker = 'o', color=COLORLEFT)
        axes.plot(df.session, df.miss, marker = 'o', color='black')
        axes.set_xticks(df.session)

        axes.set_ylim([0,1.05])
        axes.set_xlim([df.index[0],df.index[-1]])
        axes.set_ylabel('Miss (%)')
        axes.get_xaxis().set_visible(False)  
        axes.hlines(y=0.5, xmin=df.session.iloc[0], xmax=df.session.iloc[-1], linewidth=0.2, color='gray',linestyle = ':')

#___________________________Plot number of  valid trials __________________________________________________
        axes = plt.subplot2grid((14,2), (6,0), colspan=2, rowspan=2)
        axes.plot(df.session, df.valid_trials,marker = 'o', color='black')
        axes.set_xlim([df.index[0],df.index[-1]])
        axes.set_ylabel('Valid trials')
        axes.set_xlabel('Session')
        
        axes.set_xticks(np.arange(df.session.min(), df.session.max()+1, step=1))

#___________________________Plot accuracy per delay type across sessions __________________________________________________
        # axes = plt.subplot2grid((14,2), (8,0), colspan=2, rowspan=2)
        # axes.plot(df.session, df.accuracy_low, marker = 'o', color='grey')
        # axes.plot(df.session, df.accuracy_medium, marker = 'o', color='pink')
        # axes.plot(df.session, df.accuracy_high, marker = 'o', color='purple')
        
        # axes.set_xlim([0,20])
        # axes.set_ylabel('Accuracy (%)')
        # axes.set_xlabel('Session')
        
        # axes.set_xticks(np.arange(df.session.min(), df.session.max()+1, step=1))
     
#___________________________Plot accuracy per delay type accumulated __________________________________________________
       
        # SAVING AND CLOSING PAGE
        if df.motor.any() == 6:
            axes = plt.subplot2grid((14,2), (8,0), colspan=2, rowspan=2)
            axes.plot(df.session, df.delay_h,marker = 'o', color=DELAY_H)
            axes.plot(df.session, df.delay_m,marker = 'o', color=DELAY_M)
            axes.plot(df.session, df.delay_l,marker = 'o', color=DELAY_L)
            
            axes.set_xlim([df.index[0],df.index[-1]])
            axes.set_ylabel('Delay Accuracy')
            axes.set_xlabel('Session')
            
            axes.set_xticks(np.arange(df.session.min(), df.session.max()+1, step=1))
            
        plt.tight_layout()
        pdf.savefig()
        plt.close()
        
#___________________________Plot win-stay/lose switch accumulated __________________________________________________

        
# #___________________________Plot table with overview of settings. ___________________________________________        
        fig = plt.figure(figsize=(11.7, 8.3))  
    
        axes = plt.subplot2grid((14,2), (0,0), colspan=2, rowspan=14)
    
        fig.patch.set_visible(False)
    
        axes.axis('off')
        axes.axis('tight')
    
        new = df.filter(['session','day','time','code','box','stage_number','fixation','timeout','switch','motor', 'delay_l', 'delay_m', 'delay_h',], axis=1)
    
        plt.table(cellText=new.values, colLabels=new.columns, loc='center', colWidths=[0.1,0.1,0.1,0.1,0.1,0.1,0.1,0.1,0.1,0.1,0.1,0.1,0.1])

        pdf.savefig(plt.gcf(), transparent=True)  #Saves the current figure into a pdf page  
        plt.close()    

#     with PdfPages(intersession_report_path) as pdf:
        
#         # Function that adds the path to all the csv contained in the current folder all the children ones to the variable file_list. 
#         def collectfiles():
#             #input: none, output: list
#             file_list = []
#             for root, _, files in os.walk(os.getcwd()):
#                 for file in files:
#                     if file.endswith('intersession.csv'):
#                         file_list.append(os.path.join(root, file))
#             assert file_list, "I couldn't find the CSV files. Exiting ..."  
#             return file_list
