import os
import matplotlib as mpl
import matplotlib.pyplot as plt
from matplotlib.ticker import MultipleLocator
import numpy as np
import pandas as pd
from matplotlib.backends.backend_pdf import PdfPages
from matplotlib.lines import Line2D
import datetime as dt
from datahandler import Utils
import logging
import warnings
if os.environ.get('DISPLAY', '') == '':
    mpl.use('Agg')


# LOGGING AND WARNINGS
logger = logging.getLogger(__name__)
warnings.simplefilter(action='ignore', category=UserWarning)
warnings.simplefilter(action='ignore', category=RuntimeWarning)
warnings.simplefilter(action='ignore', category=FutureWarning)

# PLOT PARAMETERS
mpl.rcParams['lines.linewidth'] = 0.7
mpl.rcParams['lines.markersize'] = 1
mpl.rcParams['font.size'] = 7
mpl.rcParams['axes.spines.right'] = False
mpl.rcParams['axes.spines.top'] = False
label_kwargs = {'fontsize': 9, 'fontweight': 'roman'}
# register_matplotlib_converters()


# # TRANSFORM DATE STRING TO DATE
# def transform_date(datestring):
#     return dt.datetime.strptime(datestring, '%Y-%m-%d').date()


# TRANSFORM DATE STRING TO DATE NUMERIC
def transform_datenum(datestring):
    date = dt.datetime.strptime(datestring, '%Y-%m-%d').date()
    datetime = dt.datetime.combine(date, dt.datetime.max.time())
    datetime = dt.datetime.timestamp(datetime)
    return datetime // 86400


#  TRANSFORM DATE NUMERIC TO DATE STRING
def transform_datestring(value, tick_number):
    value *= 86400
    date = dt.datetime.fromtimestamp(value)
    return date.strftime('%Y-%m-%d')


def cellex4A_freq_intersession_report(intersession_df_path, intersession_report_path):

    try:
        df = pd.read_csv(intersession_df_path, sep=';')
    except FileNotFoundError:
        logger.critical("Error: Reading trials CSV file. Exiting...")
        raise

    # we set the index of the df as number of session, necessary if we want to unnest columns
    df.index = df.session
    # when reading the df the columns that contain lists are read as string, transform them to lists
    Utils.convert_strings_to_lists(df, ['xdata', 'fit', 'fit_error', 'params', 'ydata',
                                        'xdata_rep', 'fit_rep', 'fit_error_rep', 'params_rep', 'ydata_rep'])

    # USEFUL VALUES
    subject_name = df.subject_name.iloc[0]
    number_of_sessions = df.shape[0]
    coh_neg = [1 - df.ydata.iloc[i][0] for i in range(number_of_sessions)]
    coh_pos = [df.ydata.iloc[i][-1] for i in range(number_of_sessions)]
    s = [df.params.iloc[i][0] for i in range(number_of_sessions)]
    b = [df.params.iloc[i][1] for i in range(number_of_sessions)]
    lr_l = [df.params.iloc[i][2] for i in range(number_of_sessions)]
    lr_r = [df.params.iloc[i][3] for i in range(number_of_sessions)]
    s_rep = [df.params_rep.iloc[i][0] for i in range(number_of_sessions)]
    b_rep = [df.params_rep.iloc[i][1] for i in range(number_of_sessions)]
    lr_l_rep = [df.params_rep.iloc[i][2] for i in range(number_of_sessions)]
    lr_r_rep = [df.params_rep.iloc[i][3] for i in range(number_of_sessions)]

    df['coh_neg'] = coh_neg
    df['coh_pos'] = coh_pos
    df['s'] = s
    df['b'] = b
    df['lr_l'] = lr_l
    df['lr_r'] = lr_r
    df['s_rep'] = s_rep
    df['b_rep'] = b_rep
    df['lr_l_rep'] = lr_l_rep
    df['lr_r_rep'] = lr_r_rep
    df['date'] = df.day.apply(transform_datenum)
    # df['datenum'] = df.day.apply(transform_datenum)

    xy_df = Utils.unnesting(df, ['xdata', 'fit_error', 'ydata'])
    fit_df = Utils.unnesting(df, ['fit'])

    xy_df_rep = Utils.unnesting(df, ['xdata_rep', 'fit_error_rep', 'ydata_rep'])
    fit_df_rep = Utils.unnesting(df, ['fit_rep'])


    with PdfPages(intersession_report_path) as pdf:

        conditions = [df.stage_number == num for num in range(6)]
        conditions.append(np.isnan(df.stage_number))
        conditions.append(True)
        colors = ['blue', 'green', 'red', 'purple', 'orange', 'dodgerblue', 'black', 'gold']
        df['color'] = np.select(conditions, colors)

        boxes = ['BPOD01', 'BPOD02', 'BPOD03', 'BPOD04', 'BPOD05', 'BPOD06']
        conditions = [df.box == box for box in boxes]
        conditions.append(True)
        markers = ['o', 's', '^', 'p', 'D', 'X', 'x']
        df['marker'] = np.select(conditions, markers)

        # START OF FIRST PAGE
        plt.figure(figsize=(11.7, 8.3))  # A4

        # --------------------- Valid and silent trials plot: ---------------------------
        axes = plt.subplot2grid((100, 100), (0, 0), rowspan=20, colspan=100)

        grouped = df.groupby('marker')
        for key, group in grouped:
            axes.scatter(group.date, group.valid_trials, c=group.color, marker=key, s=3, zorder=2)
            axes.scatter(group.date, group.silent_fail, c=group.color, marker=key, s=3, zorder=2)

        axes.plot(df.date, df.valid_trials, color='black', linewidth=0.7, zorder=1)
        axes.plot(df.date, df.silent_fail, color='green', linewidth=0.7, zorder=1)

        axes.set_ylabel('Valid trials', label_kwargs)
        axes.xaxis.set_major_formatter(plt.FuncFormatter(transform_datestring))
        axes.xaxis.set_minor_locator(MultipleLocator(1))

        stage_numbers_in_plot = df.stage_number.unique()
        colors_in_plot = df.color.unique()
        order = stage_numbers_in_plot.argsort()
        stage_numbers_in_plot = stage_numbers_in_plot[order]
        colors_in_plot = colors_in_plot[order]
        boxes_in_plot = df.box.unique()
        markers_in_plot = df.marker.unique()
        order = boxes_in_plot.argsort()
        boxes_in_plot = boxes_in_plot[order]
        markers_in_plot = markers_in_plot[order]

        legend_elements = [Line2D([0], [0], color='black', label='Valid trials'),
                           Line2D([0], [0], color='green', label='Silent trials')]

        legend_elements1 = [Line2D([0], [0], color=colors_in_plot[i], marker='s', markersize=5,
                                   markerfacecolor=colors_in_plot[i], linewidth=0,
                                   label='stage_number ' + str(stage_numbers_in_plot[i]))
                            for i in range(len(stage_numbers_in_plot))]

        legend_elements2 = [Line2D([0], [0], color='black', marker=markers_in_plot[i], markersize=5,
                                   markerfacecolor='black', linewidth=0,
                                   label=str(boxes_in_plot[i])) for i in range(len(boxes_in_plot))]

        legend_elements += legend_elements1 + legend_elements2

        axes.legend(handles=legend_elements, fontsize=6, bbox_to_anchor=(1.01, 0.5),
                    loc="center left", borderaxespad=0)

        # --------------------- Accuracy plot: -------------------------------------
        axes = plt.subplot2grid((100, 100), (27, 0), rowspan=20, colspan=100)

        for key, group in grouped:
            axes.scatter(group.date, group.accuracy, c=group.color, marker=key, s=3, zorder=2)
            axes.scatter(group.date, group.accuracy_right, c=group.color, marker=key, s=3, zorder=2)
            axes.scatter(group.date, group.accuracy_left, c=group.color, marker=key, s=3, zorder=2)
            axes.scatter(group.date, group.per_invalids, c=group.color, marker=key, s=3, zorder=2)

        axes.plot(df.date, df.accuracy, color='black', zorder=1)
        axes.plot(df.date, df.accuracy_right, color='magenta', zorder=1)
        axes.plot(df.date, df.accuracy_left, color='cyan', zorder=1)
        axes.plot(df.date, df.per_invalids, color='gray', zorder=1, linestyle='dashed')

        axes.set_ylim([0, 1.1])
        axes.set_yticks(list(np.arange(0, 1.1, 0.1)))
        axes.set_yticklabels(['0', '', '', '', '', '50', '', '', '', '', '100'], fontsize=8)
        axes.set_ylabel("Accuracy [%]", label_kwargs)
        axes.xaxis.set_major_formatter(plt.FuncFormatter(transform_datestring))
        axes.xaxis.set_minor_locator(MultipleLocator(1))

        plt.text(0.1, 0.95, "Subject name: " + subject_name, fontsize=8,
                 transform=plt.gcf().transFigure)

        legend_elements = [Line2D([0], [0], color='black', label='Total'),
                           Line2D([0], [0], color='cyan', label='Left'),
                           Line2D([0], [0], color='magenta', label='Right'),
                           Line2D([0], [0], color='gray', label='Inv %', linestyle='dashed')]
        axes.legend(handles=legend_elements, fontsize=6, bbox_to_anchor=(1.01, 0.5),
                    loc="center left", borderaxespad=0)

        # --------------------- Coherences 1 and -1 plot: -------------------------
        axes = plt.subplot2grid((100, 100), (53, 0), rowspan=20, colspan=100)
        axes.set_ylim([0.5, 1.1])
        axes.set_yticks(list(np.arange(0.5, 1.1, 0.1)))
        axes.set_yticklabels(['50', '', '', '', '', '100'], fontsize=8)

        for key, group in grouped:
            axes.scatter(group.date, group.coh_neg, c=group.color, marker=key, s=3, zorder=2)
            axes.scatter(group.date, group.coh_pos, c=group.color, marker=key, s=3, zorder=2)

        axes.plot(df.date, coh_neg, color='cyan', linewidth=0.7, zorder=1)
        axes.plot(df.date, coh_pos, color='magenta', linewidth=0.7, zorder=1)

        axes.set_ylabel('Acc. Coh = 1, -1 [%]', label_kwargs)
        axes.xaxis.set_major_formatter(plt.FuncFormatter(transform_datestring))
        axes.xaxis.set_minor_locator(MultipleLocator(1))

        # --------------------- Micropokes plot: --------------------------------
        axes = plt.subplot2grid((100, 100), (80, 0), rowspan=20, colspan=100)

        for key, group in grouped:
            axes.scatter(group.date, group.micropokes, c=group.color, marker=key, s=3, zorder=2)

        axes.plot(df.date, df.micropokes, color='black', linewidth=0.7, zorder=1)

        axes.set_ylabel('Number of micropokes', label_kwargs)
        axes.set_xlabel('Session', label_kwargs)
        plt.yticks(fontsize=8)
        axes.xaxis.set_major_formatter(plt.FuncFormatter(transform_datestring))
        axes.xaxis.set_minor_locator(MultipleLocator(1))

        pdf.savefig()
        plt.close()

        # START OF SECOND PAGE
        plt.figure(figsize=(11.7, 8.3))

        # ------------------ Response and reaction times plot: -----------------------
        axes = plt.subplot2grid((100, 100), (0, 0), rowspan=27, colspan=100)

        for key, group in grouped:
            axes.scatter(group.date, group.response_time_mean, c=group.color, marker=key, s=3, zorder=2)
            axes.scatter(group.date, group.reaction_time_mean, c=group.color, marker=key, s=3, zorder=2)

        axes.plot(df.date, df.response_time_mean, color='black', zorder=1)
        axes.plot(df.date, df.reaction_time_mean, color='green', zorder=1)

        axes.set_ylabel('Response & reaction times [ms]', label_kwargs)
        plt.yticks(fontsize=8)
        axes.xaxis.set_major_formatter(plt.FuncFormatter(transform_datestring))
        axes.xaxis.set_minor_locator(MultipleLocator(1))

        legend_elements = [Line2D([0], [0], color='black', label='Response time'),
                           Line2D([0], [0], color='green', label='Reaction time')]
        axes.legend(handles=legend_elements, fontsize=6, bbox_to_anchor=(1.01, 0.5),
                    loc="center left", borderaxespad=0)

        # ----- Bias plot ----------------
        axes = plt.subplot2grid((100, 100), (35, 0), rowspan=27, colspan=100)

        for key, group in grouped:
            axes.scatter(group.date, group.b, c=group.color, marker=key, s=3, zorder=2)
            axes.scatter(group.date, group.b_rep, c=group.color, marker=key, s=3, zorder=2)

        axes.plot(df.date, df.b, color='black', zorder=1)
        axes.plot(df.date, df.b_rep, color='green', zorder=1)

        axes.set_ylabel('Bias', label_kwargs)
        axes.set_ylim([-0.55, 0.55])
        plt.yticks(fontsize=8)
        axes.xaxis.set_major_formatter(plt.FuncFormatter(transform_datestring))
        axes.xaxis.set_minor_locator(MultipleLocator(1))

        axes.axhline(y=0, color='gray', linestyle=':')

        legend_elements = [Line2D([0], [0], color='black', label='Left-right bias'),
                           Line2D([0], [0], color='green', label='Repeat bias')]
        axes.legend(handles=legend_elements, fontsize=6, bbox_to_anchor=(1.01, 0.5),
                    loc="center left", borderaxespad=0)

        # ----- Sensitivity plot ----------------
        axes = plt.subplot2grid((100, 100), (73, 0), rowspan=27, colspan=100)

        for key, group in grouped:
            axes.scatter(group.date, group.s, c=group.color, marker=key, s=3, zorder=2)
            axes.scatter(group.date, group.s_rep, c=group.color, marker=key, s=3, zorder=2)

        axes.plot(df.date, df.s, color='black', zorder=1)
        axes.plot(df.date, df.s_rep, color='green', zorder=1)

        axes.set_ylabel('Sensitivity', label_kwargs)
        axes.set_xlabel('Session', label_kwargs)
        axes.set_ylim([0, 25])
        plt.yticks(fontsize=8)
        axes.xaxis.set_major_formatter(plt.FuncFormatter(transform_datestring))
        axes.xaxis.set_minor_locator(MultipleLocator(1))

        legend_elements = [Line2D([0], [0], color='black', label='Left-right sens'),
                           Line2D([0], [0], color='green', label='Repeat sens')]
        axes.legend(handles=legend_elements, fontsize=6, bbox_to_anchor=(1.01, 0.5),
                    loc="center left", borderaxespad=0)

        pdf.savefig()
        plt.close()

        # START OF THIRD PAGE
        plt.figure(figsize=(11.7, 8.3))

        # --------------------- Psychometric curves: --------------------------------
        number = min(12, df.shape[0])
        first = df.shape[0] - number

        for i in range(number):
            num = first + i
            j = i // 4 * 37
            k = i % 4 * 27

            axes = plt.subplot2grid((100, 100), (j, k), rowspan=26, colspan=19)

            session_df = xy_df[xy_df.session == num + 1]
            session_fit_df = fit_df[fit_df.session == num + 1]

            s = session_df.s.iloc[0]
            b = session_df.b.iloc[0]
            lr_l = session_df.lr_l.iloc[0]
            lr_r = session_df.lr_r.iloc[0]

            axes.annotate('S = ' + str(round(s, 2)) + "\n" +
                          'B = ' + str(round(b, 2)) + "\n" +
                          'LR_L = ' + str(round(lr_l, 2)) + "\n" +
                          'LR_R = ' + str(round(lr_r, 2)),
                          xy=(0, 0), xytext=(-1, 0.81), fontsize=6)

            axes.plot(np.linspace(-1, 1, 30), session_fit_df.fit, linewidth=0.8, c='black')
            axes.errorbar(session_df.xdata, session_df.ydata, yerr=session_df.fit_error,
                          fmt='ro', markersize=2, elinewidth=0.7)
            axes.plot([0, 0], [0, 1], 'k-', lw=1, linestyle=':')
            axes.plot([-1, 1], [0.5, 0.5], 'k-', lw=1, linestyle=':')

            axes.tick_params(axis='both', labelsize=7.5)
            axes.set_xlim([-1.05, 1.05])
            axes.set_ylim([-0.05, 1.05])
            if i // 4 == 2:
                axes.set_xlabel('Evidence', fontsize=8)
            if i % 4 == 0:
                axes.set_ylabel('Probability of right', fontsize=8)
            axes.set_title(df.day.iloc[num] + '/' + df.time.iloc[num] + ' Session: ' + str(df.session.iloc[num]),
                           fontsize=8)

        pdf.savefig()
        plt.close()

        # START OF FOURTH PAGE
        plt.figure(figsize=(11.7, 8.3))

        # --------------------- Psychometric curves 2: --------------------------------
        number = min(12, df.shape[0])
        first = df.shape[0] - number

        for i in range(number):
            num = first + i
            j = i // 4 * 37
            k = i % 4 * 27

            axes = plt.subplot2grid((100, 100), (j, k), rowspan=26, colspan=19)

            session_df = xy_df_rep[xy_df_rep.session == num + 1]
            session_fit_df = fit_df_rep[fit_df_rep.session == num + 1]

            s_rep = session_df.s_rep.iloc[0]
            b_rep = session_df.b_rep.iloc[0]
            lr_l_rep = session_df.lr_l_rep.iloc[0]
            lr_r_rep = session_df.lr_r_rep.iloc[0]

            axes.annotate('S = ' + str(round(s_rep, 2)) + "\n" +
                          'B = ' + str(round(b_rep, 2)) + "\n" +
                          'LR_L = ' + str(round(lr_l_rep, 2)) + "\n" +
                          'LR_R = ' + str(round(lr_r_rep, 2)),
                          xy=(0, 0), xytext=(-1, 0.81), fontsize=6)

            axes.plot(np.linspace(-1, 1, 30), session_fit_df.fit_rep, linewidth=0.8, c='black')
            axes.errorbar(session_df.xdata_rep, session_df.ydata_rep, yerr=session_df.fit_error_rep,
                          fmt='ro', markersize=2, elinewidth=0.7)
            axes.plot([0, 0], [0, 1], 'k-', lw=1, linestyle=':')
            axes.plot([-1, 1], [0.5, 0.5], 'k-', lw=1, linestyle=':')

            axes.tick_params(axis='both', labelsize=7.5)
            axes.set_xlim([-1.05, 1.05])
            axes.set_ylim([-0.05, 1.05])
            if i // 4 == 2:
                axes.set_xlabel('Evidence', fontsize=8)
            if i % 4 == 0:
                axes.set_ylabel('Probability of repeat', fontsize=8)
            axes.set_title(df.day.iloc[num] + '/' + df.time.iloc[num] + ' Session: ' + str(df.session.iloc[num]),
                           fontsize=8)

        pdf.savefig()
        plt.close()
