import os
import matplotlib as mpl
import matplotlib.pyplot as plt
import matplotlib.patches as mpatches
import seaborn as sns
import numpy as np
import pandas as pd
from matplotlib.backends.backend_pdf import PdfPages
from matplotlib.lines import Line2D
from datahandler import Utils
import logging
import warnings
if os.environ.get('DISPLAY', '') == '':
    mpl.use('Agg')


# LOGGING AND WARNINGS
logger = logging.getLogger(__name__)
warnings.simplefilter(action='ignore', category=UserWarning)
warnings.simplefilter(action='ignore', category=RuntimeWarning)
warnings.simplefilter(action='ignore', category=FutureWarning)
pd.options.mode.chained_assignment = None  # suppress warning about chaining assignment

# PLOT PARAMETERS
mpl.rcParams['lines.linewidth'] = 0.7
mpl.rcParams['lines.markersize'] = 1
mpl.rcParams['font.size'] = 7
mpl.rcParams['axes.spines.right'] = False
mpl.rcParams['axes.spines.top'] = False
label_kwargs = {'fontsize': 9, 'fontweight': 'roman'}

# PLOT COLORS
first_poke_c = '#C97F74'
second_poke_c = '#96367C'
all_poke_c = '#5B1132'

first_correct_c = 'green'
correct_c = 'lightgreen'
incorrect_c = 'red'
water_c = 'teal'
other_c = 'black'

lines_c = 'gray'
lines2_c = 'silver'

def wmfm_touchteaching_daily_report(trials_path, params_path, daily_report_path):
    """
    Creates a daily report with the performance and psychometric plots, if necessary.
    We use a dataframe with one row per trial: df.
    And a pandas series: params.
    At the end, we need to return params including the new values that we have calculated.
    """

    # READ THE CSVS
    # trials_path contains a csv with one row per trial, we read it as a dataframe
    # we need to set the index of the df using the column containing the number of trial
    # this is necessary if we want to unnest columns
    # when reading the df the columns that contain lists are read as string, transform them to lists

    try:
        df = pd.read_csv(trials_path, sep=';')
    except FileNotFoundError:
        logger.critical("Error: Reading trials CSV file. Exiting...")
        raise

    try:
        params = pd.read_csv(params_path, sep=';')
    except FileNotFoundError:
        logger.critical("Error: Reading params CSV file. Exiting...")
        raise

    # SET GLOBAL INDEX AS TRIAL
    df.index = df.trial

    # NEW USEFUL COLUMNS
    df['correct_first_bool'] = df.trial_result == 1
    df['valid_bool'] = df.trial_result != 0
    df['lickport_latency'] = df['lick_time'] - df['stop_respwin']


    # EXPANDED DATAFRAMES
    # convert strings to lists, be careful df has lists in some cells now!
    df = Utils.convert_strings_to_lists(df, ['all_responses_time', 'incorrects', 'x_responses',
                                             'y_responses', 'x_errors', 'y_errors', 'errors', 'incorrect_attempts',
                                             'wm_bool', 'stimulus_onsets', 'fixation_breaks'])
    # transform params df that only contains one row in a Series object
    params = params.iloc[0]

    # unnest the lists
    resp_df = Utils.unnesting(df,
                              ['all_responses_time', 'incorrects', 'x_responses', 'y_responses', 'x_errors', 'y_errors',
                               'errors', 'incorrect_attempts', 'wm_bool'])
    onsets_df = Utils.unnesting(df, ['stimulus_onsets', 'fixation_breaks'])

    # set the response number index name
    resp_df.rename(columns={'all_responses_time_index': 'resp_index'}, inplace=True)

    # correct correct responses (no duplicates)
    resp_df['compare'] = resp_df.correct.eq(resp_df.correct.shift(-1))
    dup_index = resp_df[resp_df['compare'] == True].index
    resp_df.loc[dup_index, 'correct'] = np.NaN
    resp_df.drop('compare', axis=1)

    # SUB-DATAFRAMES
    valid_trial_df = df[df.trial_result != 0]

    #NEW COLUMNS
    resp_df['correct_bool'] = resp_df.correct > 0
    resp_df['resp_bool'] = resp_df.all_responses_time > 0

    # USEFUL VALUES
    total_trials = df.shape[0]
    total_valid_trials = valid_trial_df.shape[0]
    total_miss_trials = df[df.trial_result == 0].shape[0]

    # BINNING
    """
    Touchscreen active area: 1440*900 pixels --> 403.2*252 mm
    Stimulus radius: 35pix (9.8mm)
    x_positions: 35-1405 pix --> 9.8-393.4mm
    """
    l_edge = 9.8
    r_edge = 393.4
    bins_err = np.arange(-r_edge, r_edge, 5)
    bins_pos = np.linspace(l_edge, r_edge, 6)

    # PLOTING
    with PdfPages(daily_report_path) as pdf:

        # PAGE 1:
        plt.figure(figsize=(8.3, 11.7))  # A4 vertical

        # HEADER
        axes = plt.subplot2grid((50, 50), (0, 0), rowspan=5, colspan=50)


        s1 = ('Date: ' + str(params.day) + '-' + str(params.time) +
              '  /  Box: ' + str(params.box) +
              '  /  Subject name: ' + str(params.subject_name) +
              '  /  Weight: ' + str(params.weight) + " g" +
              '  /  Water: ' + str(round(params.water_drunk, 3)) + "mL" +
              '  /  Milkshake: ' + str(round(params.milkshake_eat, 3)) + "mL" + '\n')
        s2 = ('Total trials: ' + str(total_trials) +
              '  /  Valid trials: ' + str(total_valid_trials) +
              '  /  Missed trials: ' + str(total_miss_trials) + '\n')

        axes.text(0.1, 0.9, s1 + s2, fontsize=8, transform=plt.gcf().transFigure)

        # Y AXES TO 100 %
        def pcn_y_ax():
            axes.set_ylim(0, 1.10)
            axes.set_yticks(list(np.arange(0, 1.1, 0.1)))
            axes.set_yticklabels(['0', '', '', '', '', '50', '', '', '', '', '100'])

        # RESPONSE LATENCIES
        axes.plot(df.trial, df.correct, marker='o', markersize=3, color=all_poke_c)


        axes.set_xlabel("", label_kwargs)
        axes.set_ylabel("Response\nlatency (sec)", label_kwargs)

        # LICKPORT LATENCIES
        axes = plt.subplot2grid((50, 50), (6, 0), rowspan=5, colspan=50)
        axes.plot(df.trial, df.lickport_latency, marker='o', markersize=3, color=water_c)

        axes.set_xlabel("Trials", label_kwargs)
        axes.set_ylabel("Lickport\n latency (sec)", label_kwargs)

        # RASTER-LIKE PLOT
        axes = plt.subplot2grid((50, 50), (14, 0), rowspan=30, colspan=30)
        x_min = -2
        x_max = 75
        size_bins = 2

        correct_first_df = df[df.trial_result == 1]
        trial_list = list(df.trial)  # we need to create a list because putting directly in barh causes a small error
        axes.barh(trial_list, x_max, color=lines2_c, left=x_min, alpha=0.2)  # light bar signals each trial

        axes.scatter(resp_df.lick_time, resp_df.trial, s=2, c=water_c)
        axes.scatter(resp_df.incorrects, resp_df.trial, s=2, c=incorrect_c)
        axes.scatter(resp_df.correct, resp_df.trial, s=2, c=correct_c)
        axes.scatter(correct_first_df.correct, correct_first_df.trial, s=2, c= first_correct_c)
        axes.vlines(x=0, ymin=0, ymax=total_trials + 1, linewidth=1, color=lines_c)
        axes.vlines(x=params.timeup, ymin=0, ymax=total_trials + 1, color=lines_c, linestyle=':')

        axes.set_xlim(x_min, (x_max+x_min))
        axes.set_ylim(0, total_trials+1)
        axes.set_xticklabels([])
        axes.set_ylabel('Trial number', label_kwargs)
        yticks = (axes.get_yticks())
        axes.set_yticks(yticks[yticks != 0])  # delete tick y=0 to avoid overlaping with next plot

        # HISTOGRAM OF LATENCIES
        axes = plt.subplot2grid((50, 50), (44, 0), rowspan=6, colspan=30)
        bins = np.arange(0, x_max, size_bins)
        axes.hist(df.lick_time, histtype='step', bins=bins, alpha=0.7, color=water_c) #lickport pokes
        axes.hist(resp_df.incorrects.dropna(), histtype='step', bins=bins,
                  alpha=0.7, color=incorrect_c)
        axes.hist(df.correct, histtype='step', bins=bins, alpha=0.7, color=first_correct_c)

        axes.set_xlim(x_min, (x_max+x_min))
        axes.set_xlabel('Latency (sec)', label_kwargs)
        axes.set_ylabel('Nº of pokes', label_kwargs)

        #legend
        colors = [first_correct_c, water_c]
        labels = ['Correct', 'Water']
        lines = [Line2D([0], [0], color=colors[i], marker='o', markersize=6,
                        markerfacecolor=colors[i], linewidth=0) for i in range(len(colors))]
        axes.legend(lines, labels, fontsize=6)


        # LICKPORT LATENCIES
        axes = plt.subplot2grid((50, 50), (14, 34), rowspan=17, colspan=17)

        df['trial_result_filter'] = 0
        df.loc[(df.trial_result == 0), 'trial_result_filter'] = 'M'
        df.loc[(df.trial_result == 1), 'trial_result_filter'] = 'Cf'

        resplat_correct_first_mean = df[df.trial_result_filter == 'Cf'].correct.mean()
        resplat_correct_first_std = df[df.trial_result_filter == 'Cf'].correct.std()

        licklat_correct_first_mean = df[df.trial_result_filter == 'Cf'].lickport_latency.mean()
        licklat_miss_mean = df[df.trial_result_filter == 'M'].lickport_latency.mean()
        licklat_correct_first_std = df[df.trial_result_filter == 'Cf'].lickport_latency.std()
        licklat_miss_std = df[df.trial_result_filter == 'M'].lickport_latency.std()

        sns.boxplot(y='lickport_latency', x='trial_result_filter', data=df, order=['Cf', 'M'],
                            width=0.8, linewidth = 0.7, color = 'white', fliersize=0.1)
        sns.stripplot(y='lickport_latency', x='trial_result_filter',  data=df, order=['Cf', 'M'], color = water_c,
                      jitter=True, size=2, dodge=True)

        axes.set_ylim(0, 50)
        axes.set_xticklabels(['Correct', 'Miss'], rotation=45)
        axes.set_ylabel("Lickport latency (sec)", label_kwargs)
        axes.set_xlabel("", label_kwargs)


        # RESPONSES HISTOGRAM
        axes = plt.subplot2grid((50, 50), (38, 34), rowspan=12, colspan=17)

        bins_3 = np.linspace(l_edge, r_edge, 11)
        axes.hist(resp_df.x_responses, bins=bins_3, histtype='bar', color=all_poke_c)

        axes.set_xlim(0, 400)
        axes.set_xlabel('Screen position (mm)', label_kwargs)
        axes.set_ylabel('Nº of touches', label_kwargs)

        # SAVING AND CLOSING PAGE 1
        pdf.savefig()
        plt.close()


        # X,Y ERROR HISTOGRAM
        # axes = plt.subplot2grid((50, 50), (4, 0), rowspan=7, colspan=50)
        # plot = axes.hist2d(resp_df.x_responses, resp_df.y_responses, bins=bins_3)
        #
        # # axes.set_ylim(-25, 120)
        # axes.set_xlim(l_edge, r_edge)
        # axes.set_ylabel('Resp y (mm)',label_kwargs)
        # axes.set_xlabel('Resp x (mm)', label_kwargs)
        #
        # # horizontal colorbar
        # axes = plt.subplot2grid((50, 50), (11, 42), rowspan=2, colspan=10)
        # axes.spines['bottom'].set_visible(False)
        # axes.spines['left'].set_visible(False)
        # axes.set_xticks([])
        # axes.set_yticks([])
        # plt.colorbar(plot[3], orientation='horizontal')



    # RETURN NEW DATA FOR GENERAL PARAMS
    params['total_trials'] = total_trials
    params['total_valid_trials'] = total_valid_trials
    params['total_miss_trials'] = total_miss_trials
    params['licklat_correct_first_mean'] = licklat_correct_first_mean
    params['licklat_miss_mean'] = licklat_miss_mean
    params['licklat_correct_first_std'] = licklat_correct_first_std
    params['licklat_miss_std'] = licklat_miss_std
    params['resplat_correct_first_mean']= resplat_correct_first_mean
    params['resplat_correct_first_std'] = resplat_correct_first_std

    return params

   # RETURN NEW DATA FOR GENERAL TRIALS
    # trials[''] =
    # return trials