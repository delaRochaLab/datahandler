def wm_glue_sessions():

    # we only glue the sessions if the values of these variables are the same
    variables_to_check = {'substage','motor','switch','delay_h','delay_m','delay_l'}

    # by default params is calculated from the first session, we can select the ones we want to be the sum of all sessions
    params_sum = {'total_trials'
                  }

    # by default columns in trials_df are concatenated directly, here, columns in trials_df to which we add total_duration
    trials_plus_total_duration = { }

    # columns in trials_df to which we add total_trials
    trials_plus_total_trials = {'trials'}

    return variables_to_check, params_sum, trials_plus_total_duration, trials_plus_total_trials
