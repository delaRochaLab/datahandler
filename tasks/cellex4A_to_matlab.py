import numpy as np
import pandas as pd
import logging
import warnings
from datahandler import Utils
import scipy.io as sio


# LOGGING AND WARNINGS
logger = logging.getLogger(__name__)
warnings.simplefilter(action='ignore', category=UserWarning)
warnings.simplefilter(action='ignore', category=RuntimeWarning)
warnings.simplefilter(action='ignore', category=FutureWarning)


def transpose(df, column):
    mylist = []
    for element in df[column]:
        mylist.append(np.array(element))
    return np.transpose(np.array(mylist))

def transposedoble(df, column1, column2, idx):
    if column1 == '':
        a = np.nan
    else:
        a = np.array(df[column1].iloc[idx])
    if column2 == '':
        b = np.nan
    else:
        b = np.array(df[column2].iloc[idx])

    if isinstance(a, np.ndarray) and isinstance(b, np.ndarray):
        if a.size > 1 and b.size > 1:
            return np.transpose(np.array([a, b])[np.newaxis])
        else:
            return np.array([np.array([a, b])])
    else:
        return np.array([np.array([a, b])])

def cellex4A_to_matlab(global_params_path, global_trials_path, subject_name, matlab_path):

    global_params = pd.read_csv(global_params_path, sep = ';')
    global_trials = pd.read_csv(global_trials_path, sep=';')

    global_params.index = global_params.session
    global_trials.index = global_trials.session

    global_params = Utils.convert_strings_to_lists(global_params, ['coherence_vector'])

    global_trials = Utils.convert_strings_to_lists(global_trials, ['pre_stim_delay_start', 'pre_stim_delay_end',
                                                                     'l_poke_start', 'l_poke_end',
                                                                     'r_poke_start', 'r_poke_end',
                                                                     'c_poke_start', 'c_poke_end',
                                                                     'envelope_diff', 'early_withdrawal_pre',
                                                                     'stimulus_right', 'stimulus_left',
                                                                     'wait_cpoke_start', 'wait_cpoke_end'])

    sessions = global_params.session.unique()

    all_params = []
    session_names = []

    for session in sessions:

        params = global_params[global_params.session == session].iloc[0]
        trials = global_trials[global_trials.session == session]
        session_name = params.session_name

        StimulusBlock = transpose(trials, 'envelope_diff')
        StimulusR = transpose(trials, 'stimulus_right')
        StimulusL = transpose(trials, 'stimulus_left')

        session_params = {'CoherenceVec': np.array(params['coherence_vector']),
                          'RewardSideList': np.array(trials['reward_side']),
                          'HitHistoryList': np.array(trials['hithistory']),
                          'CoherenceList': np.array(trials['coherence_number']),
                          'SilenceList': np.array(trials['silence']),  # trials on purpose with no sound
                          'DelayList': np.array(trials['delay']),
                          'StimulusList': np.array(trials['stimulus_ind']),
                          'EnviroProbRepeat': np.array(trials['env_prob_repeat']),
                          'StageNumber': np.array(params['stage_number']),
                          'StimulusBlock': StimulusBlock,
                          'StimulusR': StimulusR,
                          'StimulusL': StimulusL,
                          'TaskVariation': np.array(params['variation']),
                          'PharmaPostOp': np.array(params['postop']),
                          'NoSound': np.array(trials['sound_fail']),  # trials when the sound failed to play
                          'SoundOnset': np.array(trials['sound_onset']),  # first message back sound is playing
                          'SoundOffset': np.array(trials['sound_offset']),  # last message back sound is playing
                          }

        if params.task == 'p1_STRF_v2.0':
            sound_names = {'SoundNames': np.array(params['sound_names'])}
            session_params.update(sound_names)
        else:
            ParsedEvents = []
            for i in range(trials.shape[0]):
                states = {'starting_state': trials['state_0'].iloc[i],
                          'ending_state': trials['final_state'].iloc[i],
                          'state_0': transposedoble(trials, '',
                                                    'trial_end', i),
                          'wait_for_apoke': transposedoble(trials, 'wait_apoke_start',
                                                           'wait_apoke_end', i),
                          'reward': transposedoble(trials, 'reward_start',
                                                   'reward_end', i),
                          'check_next_trial_ready': transposedoble(trials, 'intertrial_interval_end',
                                                                   'intertrial_interval_end', i),
                          'play_target': transposedoble(trials, 'play_target_start',
                                                        'play_target_end', i),
                          'intertrial_interval': transposedoble(trials, 'intertrial_interval_start',
                                                                'intertrial_interval_end', i),
                          'final_state': transposedoble(trials, 'intertrial_interval_end',
                                                        'intertrial_interval_end', i),
                          'wait_for_cpoke': transposedoble(trials, 'wait_cpoke_start',
                                                           'wait_cpoke_end', i),
                          'send_trial_info': np.transpose(transposedoble(trials, 'wait_cpoke_start',
                                                            'wait_cpoke_start', i)[0]),
                          'pre_stim_delay': transposedoble(trials, 'pre_stim_delay_start',
                                                           'pre_stim_delay_end', i),
                          'early_withdrawal_pre': transposedoble(trials, 'early_withdrawal_pre',
                                                                 'early_withdrawal_pre', i),
                          }

                pokes = {'L': transposedoble(trials, 'l_poke_start', 'l_poke_end', i),
                         'R': transposedoble(trials, 'r_poke_start', 'r_poke_end', i),
                         'c': transposedoble(trials, 'c_poke_start', 'c_poke_end', i),
                         }

                mylist = {'states': states,
                          'pokes': pokes}
                ParsedEvents.append([mylist])

            ParsedEvents = np.array(ParsedEvents)

            parsed_events = {'ParsedEvents': ParsedEvents}
            session_params.update(parsed_events)

        all_params.append(session_params)
        session_names.append(session_name)

    session_names = [item + '.csv' for item in session_names]
    session_names = np.array(session_names, dtype=object)

    sio.savemat(matlab_path,
                {'RatName': subject_name,
                 'ExperimentSession': session_names,
                 'BehaviorData': all_params,
                 'Experimenter': 'experimenter'},
                oned_as='column')
