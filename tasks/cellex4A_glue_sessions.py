def cellex4A_glue_sessions():
    # check this variables are the same as a condition to glue the sessions
    variables_to_check = { }

    # when having several sessions in the same day, for some parameters we simply select the ones of the first session

    # other parameters are the sum of all the sessions
    params_sum = {'total_trials',
                  'valid_trials',
                  'invalid_trials',
                  'correct_trials',
                  'water',
                  'total_duration'
                  }

    # columns in trials df that are concatenate directly

    # columns in trials df to which we add total_duration
    trials_plus_total_duration = {'c_poke_start',
                                  'c_poke_end',
                                  'l_poke_start',
                                  'l_poke_end',
                                  'r_poke_start',
                                  'r_poke_end'
                                  'intertrial_interval_start',
                                  'intertrial_interval_end',
                                  'play_target_start',
                                  'play_target_end',
                                  'pre_stim_delay_start',
                                  'pre_stim_delay_end',
                                  'reward_start',
                                  'reward_end',
                                  'trial_start',
                                  'trial_end',
                                  'wait_apoke_start',
                                  'wait_apoke_end',
                                  'wait_cpoke_start',
                                  'wait_cpoke_end',
                                  'sound_onset',
                                  'sound_offset'
                                  }

    # columns in trials df to which we add total_trials
    trials_plus_total_trials = {'trial',
                                'stimulus_ind'
                                }

    return variables_to_check, params_sum, trials_plus_total_duration, trials_plus_total_trials