import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()

setuptools.setup(
    name="datahandler",
    version="0.0.1",
    author="Rafael Marín",
    author_email="marinraf@gmail.com",
    description="Package to parse data and create reports",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://delaRochaLab@bitbucket.org/delaRochaLab/datahandler.git",
    install_requires=['statsmodels'],
    include_package_data=True,
    packages=setuptools.find_packages(),
    data_files=[
        ('', ['settings.ini']),
    ],
    zip_safe=False,
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
    python_requires='>=3.6',
    entry_points={
        'console_scripts': ['datahandler=datahandler.__main__:main']
    }
)
